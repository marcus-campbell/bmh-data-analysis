--##########################################################################################################################
-- Copyright (C) Blue Mesa Health, Inc - All Rights Reserved
-- Unauthorized reproduction of this file, via any medium, is strictly prohibited
-- All code contained in this file is both proprietary and confidential
-- Written by Evan Willms (evan.willms@bluemesahealth.com) and Marcus Campbell (marcus.campbell@bluemesahealth.com), Jan 2019
--##########################################################################################################################

-- ##############################
-- Create and populate cdc_status
-- ##############################

-- Drop existing marcus_cdc_status table
DROP TABLE IF EXISTS scratch_marcuscampbell.marcus_cdc_status;

-- Create new table from transformuser table; we filter out any PPTs that have ever had status code 6 (NAR).
CREATE TABLE scratch_marcuscampbell.marcus_cdc_status
AS
SELECT
    tu.id                                                          AS transformuserid
  , tu.status
  , tu.cohortid                                                    AS cohortid
  , c.name                                                         AS cohortname
  , c.startdate
  , EXTRACT(YEAR FROM c.startdate :: DATE)                         AS startyear
  , concat('m-' :: TEXT, EXTRACT(YEAR FROM c.startdate :: DATE) :: TEXT, '-' :: TEXT,
           to_char(EXTRACT(MONTH FROM c.startdate :: DATE), '00')) AS startmonth
  , concat('w-' :: TEXT, EXTRACT(YEAR FROM c.startdate :: DATE) :: TEXT, '-' :: TEXT,
           to_char(EXTRACT(WEEK FROM c.startdate :: DATE), '00'))  AS startweek
  , CASE
    WHEN c.version IN (2, 3)
      THEN 'en'
    WHEN c.version IN (4)
      THEN 'es'
    END                                                            AS cohort_lang
FROM bmh_prod_na.transformuser tu
  LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid
WHERE
  tu.cohortid NOT IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 16, 222, 223, 353, 354, 355, 500, 518, 632, 9998)
  AND
  tu.id NOT IN (
    SELECT DISTINCT
      transformuserid
    FROM bmh_prod_na.participantstatus
    WHERE
      statusid = 6
  );
-- Demo cohorts are in https://docs.google.com/spreadsheets/d/1Tbtw_kJ_pu0FD0TddPoPM2ARCTFi8B8oWI3ZUOsvG9Q/edit#gid=921791468
-- For now we are choosing to include the Ohio Health Test cohorts, 646 and 657, as per verbal communication from Evan.

-- Add a primary key
ALTER TABLE scratch_marcuscampbell.marcus_cdc_status
ADD PRIMARY KEY (transformuserid);

-- #########################################
-- Create and populate cdc_sessions table
-- #########################################

-- Drop previous version of table
DROP TABLE IF EXISTS    scratch_marcuscampbell.marcus_cdc_sessions;

CREATE TABLE scratch_marcuscampbell.marcus_cdc_sessions
AS
WITH
    lesson_weeks AS (
      SELECT
        transformuserid
        , contentdoneat :: DATE                         AS date
        , extract(WEEK FROM contentdoneat :: TIMESTAMP) AS yearweek
        , extract(YEAR FROM contentdoneat :: TIMESTAMP) AS year
      FROM bmh_prod_na.lessonprogress lp
      WHERE
        lp.contentdoneat NOTNULL
  ),
    checkin_weeks AS (
      SELECT
        transformuserid
        , min(date) :: DATE                    AS date
        , extract(WEEK FROM date :: TIMESTAMP) AS yearweek
        , extract(YEAR FROM date :: TIMESTAMP) AS year
      FROM bmh_prod_na.contact
      WHERE
        response = TRUE
      GROUP BY transformuserid, yearweek, year
  )

SELECT
  row_number()
  OVER () AS id
  , *

FROM (
       -- lesson_sessions
       SELECT
         transformuserid
         , 'lesson' AS source
         , date
         , yearweek
         , year
       FROM
         lesson_weeks
       GROUP BY transformuserid, date, yearweek, year

       UNION


       -- checkin_sessions
       SELECT
         checkin_weeks.transformuserid
         , 'checkin'               AS source
         , min(checkin_weeks.date) AS date
         , checkin_weeks.yearweek
         , checkin_weeks.year
       FROM
         (
           -- checkin_only_weeks: Weeks with a checkin but not a lesson
           SELECT
             transformuserid
             , yearweek
             , year
           FROM
             checkin_weeks

           EXCEPT

           SELECT
             transformuserid
             , yearweek
             , year
           FROM
             lesson_weeks
         ) checkin_only_weeks
         LEFT JOIN
         checkin_weeks ON
                         checkin_weeks.transformuserid = checkin_only_weeks.transformuserid
                         AND checkin_weeks.yearweek = checkin_only_weeks.yearweek
                         AND checkin_weeks.year = checkin_only_weeks.year
       GROUP BY checkin_weeks.transformuserid, checkin_weeks.yearweek, checkin_weeks.year

       ORDER BY date ASC, transformuserid ASC
     ) sessions;

-- ############################################
-- Create and populate cdc_weight_blended table
-- ############################################

-- First we need to copy bmh_prod_na.weight into our own schema.
-- Drop the previous version of this table
DROP TABLE IF EXISTS    scratch_marcuscampbell.marcus_cdc_weight;

-- Create a new version of this table. Note that we are selection from bmh_prod_na only.
CREATE TABLE    scratch_marcuscampbell.marcus_cdc_weight
AS
SELECT *
FROM   bmh_prod_na.weight;

-- Add primary key
ALTER TABLE     scratch_marcuscampbell.marcus_cdc_weight
ADD PRIMARY KEY (id);

-- Using our newly created marcus_cdc_weight table, we filter out any weights
-- that are too small, using Evan's old filtering script. In this case, we
-- are removing any weights below 85 lbs.

UPDATE    scratch_marcuscampbell.marcus_cdc_weight
SET       deletedat = NOW()
         ,deletedreason = 'cdc_prep_filter'
WHERE    valuelbs :: NUMERIC < 85.0 AND
         deletedat ISNULL;


-- ############################################
-- Create and populate cdc_weight_blended table
-- ############################################

-- This is a subset of our above weight table; looks like we could possibly remove the
-- previous step and just work directly with this:

-- Drop previous version
DROP TABLE IF EXISTS    scratch_marcuscampbell.marcus_cdc_weight_blended;

CREATE TABLE    scratch_marcuscampbell.marcus_cdc_weight_blended
AS
SELECT *
FROM
  (
    SELECT
        w.valuelbs :: NUMERIC  AS value
      , w.timestamp :: DATE AS date
      , w.transformuserid
      , source
    FROM
      scratch_marcuscampbell.marcus_cdc_weight w
    WHERE   1=1
            AND W.deletedat ISNULL
            AND W.transformuserid NOTNULL

  ) blended
;

-- ############################################
-- Create and populate cdc_sessions_weights
-- ############################################

-- Here we link the weight measurements from weight_blended to session data:

-- Drop previous version of table
DROP TABLE IF EXISTS    scratch_marcuscampbell.marcus_cdc_sessions_weights      CASCADE;

-- Create new version
CREATE TABLE    scratch_marcuscampbell.marcus_cdc_sessions_weights
AS
WITH all_weights AS (SELECT cs.id
                          , cs.transformuserid
                          , cs.source AS source
                          , cs.date
                          , cs.yearweek
                          , cs.year
                          , cw.value
                          , cw.source AS weight_source
                          , cw.date   AS weight_date
                     FROM scratch_marcuscampbell.marcus_cdc_sessions cs
                              LEFT JOIN scratch_marcuscampbell.marcus_cdc_weight_blended cw
                                        ON cw.transformuserid = cs.transformuserid
                                            AND cw.date <= cs.date + INTERVAL '5 days'
                                            AND cw.date >= cs.date - INTERVAL '5 days'
                     ),
     first_weight AS (SELECT  DISTINCT ON (transformuserid) -- Make sure first weight is highest that is censused within search interval around first session with weight.
                              id
                              ,transformuserid
                              ,source
                              ,date
                              ,yearweek
                              ,year
                              ,value
                              ,source AS weight_source
                              ,date   AS weight_date
                      FROM    all_weights
                      WHERE   value NOTNULL
                      ORDER BY transformuserid ASC, id ASC, value DESC
                      ),
     remaining_weights AS (SELECT DISTINCT ON (id)
                                    id
                                    ,transformuserid
                                    ,source
                                    ,date
                                    ,yearweek
                                    ,year
                                    ,value
                                    ,source AS weight_source
                                    ,date   AS weight_date
                          FROM  all_weights
                          WHERE id NOT IN (SELECT id FROM first_weight) -- Make sure not to include sessions associated with first weight recordings.
ORDER BY id, value ASC NULLS LAST
     )
SELECT  *
FROM    first_weight
UNION
SELECT  *
FROM    remaining_weights
ORDER BY transformuserid, id
;

-- ####################################
-- Create and populate cdc_activeweek
-- ####################################

-- Now we track weekly activity. IT IS VERY IMPORTANT THAT WE
-- PULL ACTIVITY INFO FROM bmh_prod_na.activityday

-- !!!!! DO NOT USE bmh_prod_na.activity !!!!

-- Drop previous version
DROP TABLE IF EXISTS    scratch_marcuscampbell.marcus_cdc_activeweek;

-- Create new table
CREATE TABLE    scratch_marcuscampbell.marcus_cdc_activeweek
AS
SELECT *
FROM
  (
    SELECT
      transformuserid
      , sum(active_minutes)   AS minutes
      , count(active_minutes) AS days
      , yearweek
      , year
    FROM (
           SELECT
             transformuserid
            -- The purpose of this code addition is to increase our reported physical activity minutes (within reason)
            -- On days where no PA was recorded, but where >= 5000 steps were recorded, the number of steps are converted
            -- to some positive number of physical activity minutes.
             , CASE WHEN activeminutes = 0 AND steps >= 5000 THEN ROUND(steps / 100)
                    ELSE activeminutes
                    END AS active_minutes
             , extract(WEEK FROM date :: DATE) AS yearweek
             , extract(YEAR FROM date :: DATE) AS year

           FROM bmh_prod_na.activityday
         ) activity

    GROUP BY transformuserid, yearweek, year
  ) activeweek;

-- #########################################
-- Create and populate cdc_sessions_activity
-- #########################################

-- Now we link CDC sessions with activity logs

-- Drop previous version
DROP TABLE IF EXISTS    scratch_marcuscampbell.marcus_cdc_sessions_activity;

-- Create new version
CREATE TABLE    scratch_marcuscampbell.marcus_cdc_sessions_activity
AS
SELECT
  cs.id
  , cs.transformuserid
  , cs.source AS source
  , cs.date
  , cs.yearweek
  , cs.year
  , ca.minutes
FROM
  scratch_marcuscampbell.marcus_cdc_sessions cs
  LEFT JOIN scratch_marcuscampbell.marcus_cdc_activeweek ca
    ON ca.transformuserid = cs.transformuserid AND ca.yearweek = cs.yearweek AND ca.year = cs.year
;