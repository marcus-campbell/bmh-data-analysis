--##########################################################################################################################
-- Copyright (C) Blue Mesa Health, Inc - All Rights Reserved
-- Unauthorized reproduction of this file, via any medium, is strictly prohibited
-- All code contained in this file is both proprietary and confidential
-- Written by Evan Willms (evan.willms@bluemesahealth.com) and Marcus Campbell (marcus.campbell@bluemesahealth.com), Jan 2019
--##########################################################################################################################

-- ################################
-- Create view_ppt_for_cdc_analysis
-- ################################

-- This view shows personal and cohort info for each ppt that is eligible for cdc analysis.

CREATE OR REPLACE VIEW scratch_marcuscampbell.view_ppt_for_cdc_analysis AS
  SELECT
      tu.id          AS transformuserid
    , tu.fullname
    , tu.status
    , tu.cohortid
    , c.name         AS cohortname
    , c.startdate
    , CASE
      WHEN c.version IN (2, 3, 11)
        THEN 'en' :: TEXT
      WHEN c.version = 4
        THEN 'es' :: TEXT
      ELSE NULL
      END            AS cohort_lang
    , coach.fullname AS coachname
  FROM bmh_prod_na.transformuser tu
    LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid
    LEFT JOIN (
                SELECT
                  ccm_1.transformuserid
                  , ccm_1.cohortid
                FROM (
                       SELECT
                         min(cohortcoachmapping.id) AS id
                         , cohortcoachmapping.cohortid
                       FROM bmh_prod_na.cohortcoachmapping
                       WHERE
                         cohortcoachmapping.deletedat IS NULL
                       GROUP BY cohortcoachmapping.cohortid) primary_coachmapping
                  LEFT JOIN bmh_prod_na.cohortcoachmapping ccm_1 ON ccm_1.id = primary_coachmapping.id) ccm
      ON ccm.cohortid = c.id
    LEFT JOIN bmh_prod_na.transformuser coach ON coach.id = ccm.transformuserid
  WHERE
    tu.cohortid NOT IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 16, 222, 223, 353, 354, 355, 500, 518, 632, 9998) -- Demo cohorts
    AND
    tu.id NOT IN (
      SELECT DISTINCT
        transformuserid
      FROM bmh_prod_na.participantstatus
      WHERE
        statusid = 6
    )
    AND
    c.version IN (2, 3, 4, 11)

    AND tu.id NOT IN (6090, 6137, 6089, 6084)
;

-- Four transform users have been filtered out from historical analyses because of pre-existing conditions or injuries incurred during their program period
-- which should make them ineligible for CDC analysis.

-- A listing of demo cohorts can be found at: https://docs.google.com/spreadsheets/d/1Tbtw_kJ_pu0FD0TddPoPM2ARCTFi8B8oWI3ZUOsvG9Q/edit#gid=921791468
-- For now we are choosing to include the Ohio Health Test cohorts, 646 and 657, as per verbal communication with Evan

-- #####################################################################
-- Create CDC Session Views (multiple views created in this subsection)
-- #####################################################################


-- Make-up sessions can be provided in any delivery mode,
-- but only one make-up session can be held on the same date as a regularly scheduled session,
-- and only one make-up session per participant per week can be held.

-- Timeframes for conducting make-up sessions are as follows:
--  1) missed core sessions must be made up within months 1-6,
--  2) missed core maintenance sessions must be made up in months 7-12.


CREATE OR REPLACE VIEW
  scratch_marcuscampbell.view_lesson_weeks AS
  SELECT
    lp.transformuserid
    , lp.contentdoneat :: DATE                                                   AS date
    , date_part('week' :: TEXT, lp.contentdoneat :: TIMESTAMP WITHOUT TIME ZONE) AS yearweek
    , date_part('year' :: TEXT, lp.contentdoneat :: TIMESTAMP WITHOUT TIME ZONE) AS year
    , lp.weeknum                                                                 AS lesson_number
  FROM bmh_prod_na.lessonprogress lp
  WHERE
    lp.contentdoneat IS NOT NULL
;

CREATE OR REPLACE VIEW
  scratch_marcuscampbell.view_checkin_weeks AS
  SELECT
    contact.transformuserid
    , min(contact.date) :: DATE                                              AS date
    , date_part('week' :: TEXT, contact.date :: TIMESTAMP WITHOUT TIME ZONE) AS yearweek
    , date_part('year' :: TEXT, contact.date :: TIMESTAMP WITHOUT TIME ZONE) AS year
  FROM bmh_prod_na.contact
  WHERE
    contact.response = TRUE
  GROUP BY contact.transformuserid, (date_part('week' :: TEXT, contact.date :: TIMESTAMP WITHOUT TIME ZONE)),
    (date_part('year' :: TEXT, contact.date :: TIMESTAMP WITHOUT TIME ZONE))
;


CREATE OR REPLACE VIEW
  scratch_marcuscampbell.view_first_session AS
  SELECT
    all_weeks.transformuserid
    , min(all_weeks.date) AS date
  FROM (SELECT
          view_lesson_weeks.transformuserid
          , view_lesson_weeks.date
        FROM scratch_marcuscampbell.view_lesson_weeks
        UNION
        SELECT
          view_checkin_weeks.transformuserid
          , view_checkin_weeks.date
        FROM scratch_marcuscampbell.view_checkin_weeks) all_weeks
    LEFT JOIN scratch_marcuscampbell.view_ppt_for_cdc_analysis cstat ON cstat.transformuserid = all_weeks.transformuserid
-- Removed WHERE clause that was causing difficulties
--WHERE
--all_weeks.date > cstat.startdate
  GROUP BY all_weeks.transformuserid
;

-- #######################
-- Create view_cdc_status
-- #######################

 CREATE OR REPLACE VIEW scratch_marcuscampbell.view_cdc_status AS
SELECT
  ppt.*
    , CASE
        WHEN fs.date ISNULL
          THEN NULL
        ELSE EXTRACT(YEAR FROM fs.date :: DATE) END                                               AS startyear
    , CASE
        WHEN fs.date ISNULL
          THEN NULL
        ELSE concat('m-' :: TEXT, EXTRACT(YEAR FROM fs.date :: DATE) :: TEXT, '-' :: TEXT,
                    to_char(EXTRACT(MONTH FROM fs.date :: DATE), '00')) END                       AS startmonth
    , CASE
        WHEN fs.date ISNULL
          THEN NULL
        ELSE concat('w-' :: TEXT, EXTRACT(YEAR FROM fs.date :: DATE) :: TEXT, '-' :: TEXT,
                    to_char(EXTRACT(WEEK FROM fs.date :: DATE), '00')) END                        AS startweek
    , CASE
        WHEN fs.date ISNULL
          THEN NULL
        ELSE concat('h-' :: TEXT, EXTRACT(YEAR FROM fs.date :: DATE) :: TEXT, '-' :: TEXT,
                    to_char((FLOOR(EXTRACT(MONTH FROM fs.date :: DATE) / 7) + 1), '00')) END AS starthalf
FROM scratch_marcuscampbell.view_ppt_for_cdc_analysis ppt
       LEFT JOIN scratch_marcuscampbell.view_first_session fs ON fs.transformuserid = ppt.transformuserid
;


-- ###########################
-- Create view_lesson_sessions
-- ###########################

CREATE OR REPLACE VIEW
  scratch_marcuscampbell.view_lesson_sessions AS
  -- Constrain core lesson (1-16) sessions to the first 6 months
  SELECT
    lw.transformuserid
    , lw.date
    , lw.yearweek
    , lw.year
    , lw.lesson_number
    , 'core' :: TEXT AS type
  FROM scratch_marcuscampbell.view_lesson_weeks lw
    LEFT JOIN scratch_marcuscampbell.view_first_session fs ON fs.transformuserid = lw.transformuserid
  WHERE
    --lw.lesson_number <= 20 AND
    lw.date < (fs.date + '6 mons' :: INTERVAL)
  UNION
  -- Maintenance lessons are valid anytime after they're available
  SELECT
    lw.transformuserid
    , lw.date
    , lw.yearweek
    , lw.year
    , lw.lesson_number
    , 'maintenance' :: TEXT AS type
  FROM scratch_marcuscampbell.view_lesson_weeks lw
    LEFT JOIN scratch_marcuscampbell.view_first_session fs ON fs.transformuserid = lw.transformuserid
  WHERE
    --lw.lesson_number > 20
    lw.date >= (fs.date + '6 mons' :: INTERVAL)
;

-- #############################
-- Create view_cdc_2018_sessions
-- #############################

-- Here we list all of our CDC sessions here:


DROP TABLE IF EXISTS scratch_marcuscampbell.view_cdc_2018_sessions CASCADE;

CREATE TABLE scratch_marcuscampbell.view_cdc_2018_sessions AS

  SELECT
    row_number()
    OVER () AS id
    , transformuserid
    , source
    , date
    , yearweek
    , year
    , type
    , lesson_number
    , cdc_type
    , cdc_session_id
    , period
    , week_count

  FROM (
         SELECT
           *
           , CASE
             WHEN week_count > 1 AND type IN ('core', 'maintenance') -- changed to allow makeup sessions for PPTs in maintenance
               THEN 'MU'
             WHEN type = 'core'
               THEN 'C'
             ELSE 'CM'
             END               AS cdc_type
           , CASE
             WHEN type = 'core'
               THEN session_count
             ELSE 99 END       AS cdc_session_id
           , row_number()
             OVER (
               PARTITION BY transformuserid, period
               ORDER BY date ) AS period_number

         FROM
           (
             SELECT
               s.*
               , row_number()
                 OVER (
                   PARTITION BY s.transformuserid, year, yearweek
                   ORDER BY s.transformuserid, s.date ASC ) AS week_count
               , row_number()
                 OVER (
                   PARTITION BY s.transformuserid
                   ORDER BY s.transformuserid, s.date )      AS session_count
               , CASE WHEN s.date < (fs.date + INTERVAL '6 months')
               THEN 'lte_6mo'
                 WHEN s.date BETWEEN (fs.date + INTERVAL '6 months') AND (fs.date + INTERVAL '9 months')
                   THEN 'btw_7mo_9mo'
                 WHEN s.date BETWEEN (fs.date + INTERVAL '9 months') AND (fs.date + INTERVAL '12 months')
                   THEN 'btw_10mo_12mo'
                 WHEN s.date > (fs.date + INTERVAL '12 months')
                   THEN 'gte_12mo'
                END                                         AS period
             FROM
               (

                 SELECT
                     ls.transformuserid :: INTEGER AS transformuserid
                   , ls.source :: TEXT             AS source
                   , ls.date
                   , ls.yearweek :: INTEGER        AS yearweek
                   , ls.year :: INTEGER            AS year
                   , ls.type
                   , ls.lesson_number
                 FROM (
                        SELECT
                          scratch_marcuscampbell.view_lesson_sessions.transformuserid
                          , 'lesson'                                                                                                        AS source
                          , view_lesson_sessions.date
                          , view_lesson_sessions.yearweek
                          , view_lesson_sessions.year
                          , view_lesson_sessions.type
                          , view_lesson_sessions.lesson_number
                          , -- Only 2 lessons per week count, but they can be the same day
                            row_number()
                            OVER (
                              PARTITION BY view_lesson_sessions.transformuserid, view_lesson_sessions.yearweek, view_lesson_sessions.year ) AS in_week
                        FROM scratch_marcuscampbell.view_lesson_sessions) ls
                 WHERE
                   ls.in_week <= 2
                 UNION
                 -- checkin_sessions
                 SELECT
                     checkin_weeks.transformuserid :: INTEGER AS transformuserid
                   , 'checkin' :: TEXT                        AS source
                   , min(checkin_weeks.date)                  AS date
                   , checkin_weeks.yearweek :: INTEGER        AS yearweek
                   , checkin_weeks.year :: INTEGER            AS year
                   , CASE WHEN min(checkin_weeks.date) >= (fs.date + INTERVAL '6 months')
                   THEN 'maintenance'
                     ELSE 'core' END                          AS type
                   , NULL                                     AS lesson_number
                 FROM (
                        -- checkin_only_weeks: Weeks with a checkin but not a lesson
                        SELECT
                          checkin_weeks_1.transformuserid
                          , checkin_weeks_1.yearweek
                          , checkin_weeks_1.year
                        FROM scratch_marcuscampbell.view_checkin_weeks checkin_weeks_1
                        EXCEPT
                        SELECT
                          view_lesson_sessions.transformuserid
                          , view_lesson_sessions.yearweek
                          , view_lesson_sessions.year
                        FROM scratch_marcuscampbell.view_lesson_sessions) checkin_only_weeks
                   LEFT JOIN scratch_marcuscampbell.view_checkin_weeks checkin_weeks ON checkin_weeks.transformuserid =
                                                                     checkin_only_weeks.transformuserid AND
                                                                     checkin_weeks.yearweek =
                                                                     checkin_only_weeks.yearweek AND
                                                                     checkin_weeks.year = checkin_only_weeks.year
                   LEFT JOIN scratch_marcuscampbell.view_first_session fs ON fs.transformuserid = checkin_only_weeks.transformuserid

                 GROUP BY checkin_weeks.transformuserid, checkin_weeks.yearweek, checkin_weeks.year, fs.date
               ) s
               LEFT JOIN scratch_marcuscampbell.view_first_session fs ON fs.transformuserid = s.transformuserid
             ORDER BY transformuserid, s.date
           ) os


       ) oss
  WHERE
    (period = 'lte_6mo' AND period_number <= 19)
    OR (period = 'btw_7mo_9mo' AND period_number <= 3)
    OR (period = 'btw_10mo_12mo' AND period_number <= 3)
;

-- #########################################################
-- Create view_last_session and view_cdc_2018_sessions_count
-- #########################################################

-- The view "view_cdc_2018_sessions_count" essentially shows the aggregated counts
-- for each session type (here, 'type' refers to the different time intervals within which
-- CDC qualified sessions are considered for each eligible PPT.

-- First we create a view for the last sessions:
-- For dropping this view we need to have a CASCADE statement, as
-- scratch_marcuscampbell.view_cdc_2018_sessions_count depends on it:

-- Drop existing view and cascade drop
DROP VIEW IF EXISTS scratch_marcuscampbell.view_last_session CASCADE;

-- Create new view
CREATE OR REPLACE VIEW
  scratch_marcuscampbell.view_last_session AS
  SELECT DISTINCT ON (cdc_2018_sessions.transformuserid)
    cdc_2018_sessions.transformuserid
    , cdc_2018_sessions.date
  FROM scratch_marcuscampbell.view_cdc_2018_sessions cdc_2018_sessions
  ORDER BY cdc_2018_sessions.transformuserid, cdc_2018_sessions.date DESC
;

DROP VIEW IF EXISTS scratch_marcuscampbell.view_cdc_2018_sessions_count CASCADE;

CREATE OR REPLACE VIEW scratch_marcuscampbell.view_cdc_2018_sessions_count AS
SELECT
  cstat.transformuserid :: INTEGER AS transformuserid
    , cstat.startyear              AS year
    , cstat.startweek
    , cstat.starthalf
    , fs.date                      AS first_date
    , ls.date                      AS last_date
    , sessions.count_sessions_lte_6_mo
    , sessions.count_sessions_gte_7_mo
    , sessions.count_sessions_months_7_9
    , sessions.count_sessions_months_10_12
    , CASE
        WHEN sessions.count_sessions_lte_6_mo >= 3
          THEN 1
        ELSE 0
  END                              AS gte_3_sessions_6_mo
    , CASE
        WHEN ls.date >= (fs.date + '9 mons' :: INTERVAL)
          THEN 1
        ELSE 0
  END                              AS last_session_gte_9_mo
FROM scratch_marcuscampbell.view_cdc_status cstat
       LEFT JOIN scratch_marcuscampbell.view_first_session fs ON fs.transformuserid = cstat.transformuserid
       LEFT JOIN scratch_marcuscampbell.view_last_session ls ON ls.transformuserid = cstat.transformuserid
       LEFT JOIN (
    SELECT
      cs.transformuserid
        , count(*)                                                           AS count_total_sessions
        , count(*)
                FILTER (WHERE cs.date < (fs_1.date + '6 mons' :: INTERVAL)) AS count_sessions_lte_6_mo
        , count(*)
                FILTER (WHERE cs.date >= (fs_1.date + '6 mons' :: INTERVAL) AND
                              cs.date <= (fs_1.date + '1 year' :: INTERVAL)) AS count_sessions_gte_7_mo
        , count(*)
                FILTER (WHERE cs.date > (fs_1.date + '6 mons' :: INTERVAL) AND
                              cs.date <= (fs_1.date + '9 mons' :: INTERVAL)) AS count_sessions_months_7_9
        , count(*)
                FILTER (WHERE cs.date > (fs_1.date + '9 mons' :: INTERVAL) AND
                              cs.date <= (fs_1.date + '1 year' :: INTERVAL)) AS count_sessions_months_10_12
    FROM scratch_marcuscampbell.view_cdc_2018_sessions cs
           LEFT JOIN scratch_marcuscampbell.view_first_session fs_1 ON fs_1.transformuserid = cs.transformuserid
    GROUP BY cs.transformuserid
  ) sessions
                 ON sessions.transformuserid = cstat.transformuserid
;
