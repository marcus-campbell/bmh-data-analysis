--##########################################################################################################################
-- Copyright (C) Blue Mesa Health, Inc - All Rights Reserved
-- Unauthorized reproduction of this file, via any medium, is strictly prohibited
-- All code contained in this file is both proprietary and confidential
-- Written by Evan Willms (evan.willms@bluemesahealth.com) and Marcus Campbell (marcus.campbell@bluemesahealth.com)
--##########################################################################################################################

-- In this query, we first generate two tables of weights and session activity data.
-- We then generate a view that is a summary of all the participants, listing
-- their program stage and session completion counts. Finally, we generate the DPRP report to be submitted.

-- ######################
-- Session Weight Data
-- ######################

DROP TABLE IF EXISTS    scratch_marcuscampbell.cdc_2018_sessions_weights        CASCADE;

CREATE TABLE    scratch_marcuscampbell.cdc_2018_sessions_weights
AS
SELECT DISTINCT ON (cs.id)
  cs.id
  , cs.transformuserid
  , cs.source AS source
  , cs.date
  , cs.yearweek
  , cs.year
  , cw.value
  , cw.source AS weight_source
  , cw.date   AS weight_date
FROM
  scratch_marcuscampbell.view_cdc_2018_sessions cs
  LEFT JOIN scratch_marcuscampbell.marcus_cdc_weight_blended cw ON cw.transformuserid = cs.transformuserid
                                         AND cw.date <= cs.date + INTERVAL '5 days' AND
                                         cw.date >= cs.date - INTERVAL '5 days'
ORDER BY cs.id, cw.value ASC NULLS LAST
;

-- #########################
--    Session Activity Data
-- #########################

DROP TABLE IF EXISTS scratch_marcuscampbell.cdc_2018_sessions_activity CASCADE
;

CREATE TABLE scratch_marcuscampbell.cdc_2018_sessions_activity
AS
SELECT
  cs.id
  , cs.transformuserid
  , cs.source AS source
  , cs.date
  , cs.yearweek
  , cs.year
  , ca.minutes
FROM
  scratch_marcuscampbell.view_cdc_2018_sessions cs
  LEFT JOIN scratch_marcuscampbell.marcus_cdc_activeweek ca
    ON ca.transformuserid = cs.transformuserid
       AND ca.yearweek = cs.yearweek
       AND ca.year = cs.year
;

-- ##############################
-- Create view_dprp_2018_by_ppt
-- ##############################

DROP VIEW IF EXISTS scratch_marcuscampbell.view_dprp_2018_by_ppt
;

CREATE OR REPLACE VIEW scratch_marcuscampbell.view_dprp_2018_by_ppt
AS
  SELECT
    cstat.transformuserid
    , cstat.fullname
    , cstat.status
    , cstat.cohortid
    , cstat.cohortname
    , cstat.coachname
    , to_char(cstat.startdate :: DATE, 'YYYY-MM-DD')                      AS startdate
    , to_char(fs.date :: DATE, 'YYYY-MM-DD')                              AS first_cdc_session
    , 9                                                                   AS months_1_6_sessions_needed
    , coalesce(csc.count_sessions_lte_6_mo, 0)                            AS sessions_completed_mo_1_6
    , CASE
      WHEN csc.count_sessions_lte_6_mo >= 9
        THEN 0
      ELSE 9 - coalesce(csc.count_sessions_lte_6_mo, 0)
      END                                                                 AS sessions_needed_mo_1_6
    , (coalesce(fs.date, cstat.startdate) + INTERVAL '6 months') :: DATE  AS month_6_ends
    , 2                                                                   AS months_7_9_sessions_needed
    , coalesce(csc.count_sessions_months_7_9, 0)                          AS sessions_completed_mo_7_9
    , CASE
      WHEN csc.count_sessions_months_7_9 >= 2
        THEN 0
      ELSE 2 - coalesce(csc.count_sessions_months_7_9, 0)
      END                                                                 AS sessions_needed_mo_7_9
    , (coalesce(fs.date, cstat.startdate) + INTERVAL '9 months') :: DATE  AS month_9_ends
    , 1                                                                   AS months_10_12_sessions_needed
    , coalesce(csc.count_sessions_months_10_12, 0)                        AS sessions_completed_mo_10_12
    , CASE
      WHEN csc.count_sessions_months_10_12 >= 1
        THEN 0
      ELSE 2 - coalesce(csc.count_sessions_months_10_12, 0)
      END                                                                 AS sessions_needed_mo_10_12
    , (coalesce(fs.date, cstat.startdate) + INTERVAL '12 months') :: DATE AS month_12_ends
  FROM
    scratch_marcuscampbell.view_cdc_status cstat
    LEFT JOIN scratch_marcuscampbell.view_cdc_2018_sessions_count csc ON csc.transformuserid = cstat.transformuserid
    LEFT JOIN scratch_marcuscampbell.view_first_session fs ON fs.transformuserid = cstat.transformuserid

  ORDER BY cstat.startdate, cstat.cohortname, cstat.fullname
;

-- ########################
-- Generate the DPRP Report
-- ########################

-- NOTE: Due to changes in our calculations for PPT start dates, you may have to manually shift the session IDs
-- for PPT 8898 forward by 1 session. Please refer to previous DPRP submissions in order to ensure agreement across
-- reporting periods.

-- DROP TABLE IF EXISTS    scratch_marcuscampbell.DPRP_submission_2019_H2;

CREATE TABLE    scratch_marcuscampbell.DPRP_submission_2020_h2_v3  AS

SELECT
  demo.*
  , sess.cdc_session_id AS SESSID
  , sess.cdc_type       AS SESSTYPE
  , TO_CHAR(sess.date,   'mm/dd/yyyy')         AS DATE
  , CASE WHEN sweight.value < 997
  THEN ROUND(sweight.value, 0)
    ELSE 999 END        AS WEIGHT
  , CASE
    WHEN act.minutes < 997
      THEN ROUND(act.minutes,0)
    ELSE 999 END        AS PA

FROM (

       SELECT
           '3202115'::INTEGER                                 AS ORGCODE
         , tu.id                                              AS PARTICIP
         , '9'::SMALLINT                                      AS ENROLL
         , CASE WHEN sp.selfpay IS TRUE
         THEN '4'
           ELSE '3' END                                       AS PAYER
         , ccs.state                                          AS STATE
         , CASE WHEN cqb.any = 1
         THEN 1
           ELSE 2 END                                         AS GLUCTEST
         , CASE WHEN cqb.gdm = 1
         THEN 1
           ELSE 2 END                                         AS GDM
         , CASE
           WHEN ccr.risk_qual = 1
             THEN 1
           WHEN (cqb.any <> 1 OR cqb.any ISNULL) AND (cqb.gdm <> 1 OR cqb.gdm ISNULL)
             THEN 1
           ELSE 2 END                                         AS RISKTEST
         , DATE_PART('year', AGE(c.startdate::TIMESTAMP, tu.birthdate::TIMESTAMP)) AS AGE
         , coalesce(cceth.ethnic, 2)                          AS ETHNIC
         , coalesce(cceth.aian, 2)                            AS AIAN
         , coalesce(cceth.asian, 2)                           AS ASIAN
         , coalesce(cceth.black, 2)                           AS BLACK
         , coalesce(cceth.nhopi, 2)                           AS NHOPI
         , coalesce(cceth.white, 2)                           AS WHITE
         , CASE WHEN ccx.biologicalsex = 'M' OR ccx.biologicalsex = 'Male'
         THEN 1
           WHEN ccx.biologicalsex = 'F' OR ccx.biologicalsex = 'Female'
             THEN 2
           ELSE 9 END                                         AS SEX
         , coalesce(FLOOR(tu.height), FLOOR(cch.height), 999)               AS HEIGHT
         , coalesce(ccedu.edu, 9)                             AS EDU
         , '2'::SMALLINT                                      AS DMODE


       FROM
         bmh_prod_na.transformuser tu
         LEFT JOIN (
                     SELECT
                         "Participant ID" :: INTEGER AS transformuserid
                       , TRUE                        AS selfpay
                     FROM zoho.contacts
                     WHERE
                       "Lead Source" = 'Direct'
                   ) sp ON sp.transformuserid = tu.id
         LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid
         LEFT JOIN scratch_marcuscampbell.marcus_cdc_state ccs ON ccs.transformuserid = tu.id
         LEFT JOIN scratch_marcuscampbell.marcus_cdc_qual_blood cqb ON cqb.transformuserid = tu.id
         LEFT JOIN scratch_marcuscampbell.marcus_cdc_risk ccr ON ccr.transformuserid = tu.id
         LEFT JOIN scratch_marcuscampbell.marcus_cdc_ethnographic cceth ON cceth.transformuserid = tu.id
         LEFT JOIN scratch_marcuscampbell.marcus_cdc_sex ccx ON ccx.transformuserid = tu.id
         LEFT JOIN scratch_marcuscampbell.marcus_cdc_education ccedu ON ccedu.transformuserid = tu.id
         LEFT JOIN scratch_marcuscampbell.marcus_cdc_height cch ON cch.transformuserid = tu.id

       WHERE
         tu.cohortid NOT IN
         (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 16, 19, 222, 223, 353, 354, 355, 500, 518, 632, 646, 657, 907, 918, 950, 1079, 1132, 9988, 1536, 1441, 1503, 1535, 1598)
         -- We are excluding these demo cohorts, which can be found at:
         -- https://docs.google.com/spreadsheets/d/1Tbtw_kJ_pu0FD0TddPoPM2ARCTFi8B8oWI3ZUOsvG9Q/edit#gid=921791468
         -- Based on verbal confirmation from Evan, we are including the Ohio Health Test Cohorts: 646 and 657
         AND
         tu.id NOT IN (
           SELECT DISTINCT
             transformuserid
           FROM bmh_prod_na.participantstatus
           WHERE
             statusid = 6
         )

         -- https://docs.google.com/spreadsheets/d/1Tbtw_kJ_pu0FD0TddPoPM2ARCTFi8B8oWI3ZUOsvG9Q/edit#gid=921791468
         AND tu.id NOT IN (6090,
                           6137,
                           6089,
                           6084,
                           9133, -- CRC ppt in Solera cohort
                           10825, -- CRC ppt in Solera cohort
                           15687, -- ppt missing info in contacts and leads
                           15688, -- BMH employee
                           14937, -- program repeat
                           15113, -- program repeat
                           16443, -- program repeat
                           17430, -- demo
                           23353, -- missing height info
                           26001, -- internal member
                           26002, -- internal member
                           26027, -- internal member
                           26053, -- internal member
                           26054, -- internal member
                           26114, -- internal member
                           26115, -- internal member
                           26118, -- internal member
                           26659, -- internal member
                           26713, -- internal member
                           26716, -- internal member
                           26717, -- internal member
                           26722, -- internal member
                           26723, -- internal member
                           27639 -- internal member
                          ,26269 -- missing height info
                          ,26287 -- missing height info
                          ,26292 -- missing height info
                          ,26295 -- missing height info
                          ,26306 -- missing height info
                          ,26818 -- missing height info
                          )
     ) demo

  RIGHT JOIN scratch_marcuscampbell.view_cdc_2018_sessions sess ON sess.transformuserid = demo.PARTICIP
  LEFT JOIN scratch_marcuscampbell.cdc_2018_sessions_weights sweight ON sweight.id = sess.id
  LEFT JOIN scratch_marcuscampbell.cdc_2018_sessions_activity act ON act.id = sess.id

WHERE 1=1
      AND ORGCODE = '3202115'
      AND sess.date >= '2020-07-01'::DATE AND sess.date <= '2020-12-31'::DATE

ORDER BY PARTICIP, sess.date
;
