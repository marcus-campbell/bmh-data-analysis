--###########################################################################################################################
-- Copyright (C) Blue Mesa Health, Inc - All Rights Reserved
-- Unauthorized reproduction of this file, via any medium, is strictly prohibited
-- All code contained in this file is both proprietary and confidential
-- Written by Evan Willms (evan.willms@bluemesahealth.com) and Marcus Campbell (marcus.campbell@bluemesahealth.com), Jan 2019
--###########################################################################################################################

-- ######################
-- Metric 5: Attendance
-- ######################

CREATE OR REPLACE VIEW  scratch_marcuscampbell.DPRP_2018_attendance_criteria    AS

WITH counts AS (
    SELECT
      cstat.cohort_lang
      , cstat.starthalf
      , COUNT(*) AS all_ppt
      , COUNT(*)
          FILTER (WHERE
            csc.gte_3_sessions_6_mo = 1
            AND csc.last_session_gte_9_mo = 1
          )      AS qual_ppt
      , COUNT(*)
          FILTER (WHERE
            csc.gte_3_sessions_6_mo = 1
            AND csc.last_session_gte_9_mo = 1
            AND csc.count_sessions_lte_6_mo >= 9
          )      AS qual_ppt_gte_9_sessions_core
      , COUNT(*)
          FILTER (WHERE
            csc.gte_3_sessions_6_mo = 1
            AND csc.last_session_gte_9_mo = 1
            AND csc.count_sessions_gte_7_mo >= 3
          )      AS qual_ppt_gte_3_sessions_maint
    FROM
      scratch_marcuscampbell.view_cdc_status cstat
      LEFT JOIN scratch_marcuscampbell.view_cdc_2018_sessions_count csc ON csc.transformuserid = cstat.transformuserid
    WHERE
      cstat.cohort_lang NOTNULL AND cstat.starthalf NOTNULL
    GROUP BY cstat.cohort_lang, cstat.starthalf
    ORDER BY cstat.cohort_lang, cstat.starthalf ASC
)
SELECT
    concat(cohort_lang, '-', starthalf) as key
    ,*
    ,CASE WHEN qual_ppt > 0
        THEN (qual_ppt_gte_9_sessions_core * 100 :: NUMERIC / qual_ppt)
        ELSE NULL
     END    AS pct_first_6_attendance
    ,CASE WHEN qual_ppt > 0
     THEN
            CASE WHEN (qual_ppt_gte_9_sessions_core * 100 :: NUMERIC / qual_ppt) > 60.0
            THEN 1
            ELSE 0 END
     ELSE 0
     END        AS qual_5a_first_6_attendence
    , CASE WHEN qual_ppt > 0
        THEN (qual_ppt_gte_3_sessions_maint * 100 :: NUMERIC / qual_ppt)
        ELSE NULL
      END   AS pct_last_6_attendance
    , CASE WHEN qual_ppt > 0
  THEN
    CASE WHEN (qual_ppt_gte_3_sessions_maint * 100 :: NUMERIC / qual_ppt) > 60.0
      THEN 1
    ELSE 0 END
    ELSE 0
    END        AS qual_5b_last_6_attendence
  , CASE WHEN qual_ppt >= 5
  THEN 1
    ELSE 0 END AS qual_5c_at_least_5_qual
FROM counts
ORDER BY starthalf, cohort_lang
;

-- #############################################
--    Attendance Trends (Not actually criteria)
-- #############################################

CREATE OR REPLACE VIEW  scratch_marcuscampbell.DPRP_2018_attendance_trend       AS

WITH sd AS (
    SELECT
      transformuserid
      , year
      , csc.first_date
      , CASE
        WHEN EXTRACT(EPOCH FROM age(csc.first_date)) <= 0
          THEN NULL
        WHEN EXTRACT(YEAR FROM age(csc.first_date)) >= 1
          THEN 6
        WHEN EXTRACT(MONTH FROM age(csc.first_date)) >= 6
          THEN 6
        ELSE EXTRACT(MONTH FROM age(csc.first_date))
             + (EXTRACT(DAY FROM age(csc.first_date)) / 31)
        END
      AS months_in_core
      , CASE
        WHEN EXTRACT(EPOCH FROM age(csc.first_date + INTERVAL '6 months')) <= 0
          THEN NULL
        WHEN EXTRACT(YEAR FROM age(csc.first_date + INTERVAL '6 months')) >= 1
          THEN 6
        WHEN EXTRACT(MONTH FROM age(csc.first_date + INTERVAL '6 months')) >= 6
          THEN 6
        ELSE EXTRACT(MONTH FROM age(csc.first_date + INTERVAL '6 months'))
             + (EXTRACT(DAY FROM age(csc.first_date + INTERVAL '6 months')) / 31)
        END
      AS months_in_maint
    FROM scratch_marcuscampbell.view_cdc_2018_sessions_count csc
),
    metrics AS (

      SELECT
        cstat.starthalf
        , count(DISTINCT cstat.transformuserid)        AS count_ppt
        , avg(
              csc.count_sessions_lte_6_mo / sd.months_in_core
          )
            FILTER (WHERE csc.gte_3_sessions_6_mo = 1) AS avg_sessions_per_qual_ppt_core_month
        , 9.0 / 6.0                                    AS req_avg_sessions_core_month
        , count(*)
            FILTER (WHERE
              csc.gte_3_sessions_6_mo = 1
              AND (csc.count_sessions_lte_6_mo / sd.months_in_core) > (9.0 / 6.0)
            ) :: NUMERIC                               AS count_qual_ppt_on_trend_in_core
        , count(*)
            FILTER (WHERE
              csc.gte_3_sessions_6_mo = 1
            ) :: NUMERIC                               AS count_qual_ppt
        , avg(
              csc.count_sessions_gte_7_mo / sd.months_in_maint
          )
            FILTER (WHERE csc.gte_3_sessions_6_mo = 1) AS avg_sessions_per_qual_ppt_maint_month
        , 3.0 / 6.0                                    AS req_avg_sessions_maint_month
        , count(*)
            FILTER (WHERE
              csc.gte_3_sessions_6_mo = 1
              AND (csc.count_sessions_gte_7_mo / sd.months_in_maint) > 0.5
            ) :: NUMERIC                               AS count_qual_ppt_on_trend_in_maint
      FROM
        scratch_marcuscampbell.view_cdc_status cstat
        LEFT JOIN scratch_marcuscampbell.view_cdc_2018_sessions_count csc ON csc.transformuserid = cstat.transformuserid
        LEFT JOIN sd ON sd.transformuserid = cstat.transformuserid
      WHERE
        cstat.starthalf NOTNULL
      GROUP BY cstat.starthalf
  ),

    summary AS (
      SELECT
        starthalf
        , count_ppt
        , count_qual_ppt
        , CASE WHEN count_ppt > 0 AND count_qual_ppt > 0
        THEN to_char(count_qual_ppt * 100 :: NUMERIC / count_ppt, '900.0')
          ELSE NULL END                                             AS pct_qual_ppt
        , round(avg_sessions_per_qual_ppt_core_month :: NUMERIC, 2) AS avg_sessions_per_qual_ppt_core_month
        , req_avg_sessions_core_month
        , CASE WHEN count_qual_ppt > 0 AND count_qual_ppt_on_trend_in_core > 0
        THEN to_char(count_qual_ppt_on_trend_in_core * 100 :: NUMERIC / count_qual_ppt, '900.0')
          ELSE NULL
          END                                                       AS pct_qual_ppt_on_trend_in_core
        , 60.0                                                         req_pct_qual_ppt_on_trend_in_core
        , CASE WHEN count_qual_ppt > 0 AND count_qual_ppt_on_trend_in_core > 0
        THEN to_char(count_qual_ppt_on_trend_in_core * 100 :: NUMERIC / count_qual_ppt, '900.0')
          ELSE NULL
          END                                                       AS pct_qual_ppt_on_trend_in_maint
        , 60.0                                                      AS req_pct_qual_ppt_on_trend_in_maint
      FROM metrics
      ORDER BY starthalf
  )
SELECT
  *
  , CASE
    WHEN pct_qual_ppt_on_trend_in_core ISNULL
      THEN NULL
    WHEN pct_qual_ppt_on_trend_in_core :: NUMERIC > req_pct_qual_ppt_on_trend_in_core :: NUMERIC
      THEN 1
    ELSE 0 END AS qual_5a_attenance_core
  , CASE
    WHEN pct_qual_ppt_on_trend_in_maint ISNULL
      THEN NULL
    WHEN pct_qual_ppt_on_trend_in_maint :: NUMERIC > req_pct_qual_ppt_on_trend_in_maint :: NUMERIC
      THEN 1
    ELSE 0 END AS qual_5b_attenance_maint

FROM summary
;


-- ############################################################################
--    6: Sessions with Bodyweight data
-- ############################################################################

CREATE OR REPLACE VIEW  scratch_marcuscampbell.DPRP_2018_weight_criteria        AS

WITH metrics AS (

    SELECT
      cstat.cohort_lang
      , cstat.starthalf
      , count(*)                                                            AS count_all_sessions_init_qual_ppt
      , count(*)
          FILTER (WHERE cw.value NOTNULL)                                   AS count_sessions_w_weight_init_qual_ppt
      --       , count(*)
      --           FILTER (WHERE cw.value NOTNULL) * 100 :: NUMERIC / count(*)       AS pct_sessions_w_weight_init
      , 80.0                                                                AS req_pct_sessions_w_weight
      --       , CASE WHEN (count(*)
      --                      FILTER (WHERE cw.value NOTNULL) * 100 :: NUMERIC / count(*)) > 80.0
      --       THEN 1
      --         ELSE 0 END                                                          AS qual_6_sessions_w_weight_init

      , count(*)
          FILTER (WHERE csc.last_session_gte_9_mo = 1)                      AS count_sessions_qual_ppt
      , count(*)
          FILTER (WHERE cw.value NOTNULL AND csc.last_session_gte_9_mo = 1) AS count_sessions_w_weight_qual_ppt
    --       , count(*)
    --           FILTER (WHERE cw.value NOTNULL AND csc.last_session_gte_9_mo = 1) * 100 :: NUMERIC /
    --         count(*)
    --           FILTER (WHERE csc.last_session_gte_9_mo = 1)                      AS pct_sessions_w_weight
    --       , CASE WHEN (count(*)
    --                      FILTER (WHERE cw.value NOTNULL AND csc.last_session_gte_9_mo = 1) * 100 :: NUMERIC
    --                    / count(*)
    --                      FILTER (WHERE csc.last_session_gte_9_mo = 1)
    --                   ) > 80.0
    --       THEN 1
    --         ELSE 0 END                                                          AS qual_6_sessions_w_weight

    FROM
      scratch_marcuscampbell.view_cdc_status cstat
      LEFT JOIN scratch_marcuscampbell.view_cdc_2018_sessions cs ON cs.transformuserid = cstat.transformuserid
      LEFT JOIN scratch_marcuscampbell.view_cdc_2018_sessions_count csc ON csc.transformuserid = cs.transformuserid
      LEFT JOIN scratch_marcuscampbell.cdc_2018_sessions_weights cw ON cw.id = cs.id
    WHERE
      csc.gte_3_sessions_6_mo = 1
      AND cstat.cohort_lang NOTNULL
--       AND cs.source = 'lesson'
    GROUP BY cstat.cohort_lang, cstat.starthalf
    ORDER BY cstat.cohort_lang, cstat.starthalf ASC
)
SELECT
  *
  , CASE WHEN count_all_sessions_init_qual_ppt > 0
  THEN count_sessions_w_weight_init_qual_ppt * 100 :: NUMERIC / count_all_sessions_init_qual_ppt
    ELSE NULL END AS pct_init_qual_ppt_sessions_w_weight
  , CASE WHEN count_all_sessions_init_qual_ppt > 0
  THEN
    CASE WHEN (count_sessions_w_weight_init_qual_ppt * 100 :: NUMERIC / count_all_sessions_init_qual_ppt) >
              req_pct_sessions_w_weight
      THEN 1
    ELSE 0 END
    ELSE NULL END AS qual_6_init_ppt
  , CASE WHEN count_sessions_qual_ppt > 0
  THEN
    count_sessions_w_weight_qual_ppt * 100 :: NUMERIC / count_sessions_qual_ppt
    ELSE NULL END AS pct_qual_ppt_sessions_w_weight
  , CASE WHEN count_sessions_qual_ppt > 0
  THEN
    CASE WHEN (count_sessions_w_weight_qual_ppt * 100 :: NUMERIC / count_sessions_qual_ppt) > req_pct_sessions_w_weight
      THEN 1
    ELSE 0 END
    ELSE NULL END AS qual_ppt_sessions_w_weight
FROM
  metrics
;


-- ############################################################################
--   7: Physical Activity
-- ############################################################################

CREATE OR REPLACE VIEW  scratch_marcuscampbell.DPRP_2018_activity_criteria      AS

SELECT
  cstat.starthalf
  , count(*)                                                                       AS count_all_sessions
  , count(*)
      FILTER (WHERE csc.gte_3_sessions_6_mo = 1)                                   AS count_sessions_w_init_qual_ppt
  , count(*)
      FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csa.minutes >
                                                    0)                             AS count_init_qual_sessions_w_activity
  , CASE WHEN (count(*)
  FILTER (WHERE csc.gte_3_sessions_6_mo = 1)) > 0
  THEN

    count(*)
      FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csa.minutes > 0) * 100 :: NUMERIC /
    count(*)
      FILTER (WHERE csc.gte_3_sessions_6_mo = 1) :: NUMERIC
    ELSE 0 END                                                                     AS pct_init_qual_sessions_w_activity
  ,
    60.0                                                                           AS req_pct_init_qual_sessions_w_activity
  , CASE WHEN CASE WHEN (count(*)
  FILTER (WHERE csc.gte_3_sessions_6_mo = 1)) > 0
  THEN

    count(*)
      FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csa.minutes > 0) * 100 :: NUMERIC /
    count(*)
      FILTER (WHERE csc.gte_3_sessions_6_mo = 1) :: NUMERIC
              ELSE 0 END > 60
  THEN 1
    ELSE 0 END                                                                     AS qual_7_sessions_w_pa_init_only

  , CASE WHEN (count(*)
  FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csc.last_session_gte_9_mo = 1)) > 0
  THEN

    count(*)
      FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csc.last_session_gte_9_mo = 1 AND csa.minutes > 0) * 100 :: NUMERIC /
    count(*)
      FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csc.last_session_gte_9_mo = 1) :: NUMERIC
    ELSE 0 END                                                                     AS pct_qual_sessions_w_activity
  , 60.0                                                                           AS req_pct_qual_sessions_w_activity
  , COUNT(*) FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csc.last_session_gte_9_mo = 1) AS count_sessions_w_qual_ppt
  , CASE WHEN CASE WHEN (count(*)
  FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csc.last_session_gte_9_mo = 1)) > 0
  THEN

    count(*)
      FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csc.last_session_gte_9_mo = 1 AND csa.minutes > 0) * 100 :: NUMERIC
    /
    count(*)
      FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csc.last_session_gte_9_mo = 1) :: NUMERIC
              ELSE 0 END > 60
  THEN 1
    ELSE 0 END                                                                     AS qual_7_sessions_w_pa
FROM
  scratch_marcuscampbell.view_cdc_2018_sessions cs
  LEFT JOIN scratch_marcuscampbell.view_cdc_2018_sessions_count csc ON csc.transformuserid = cs.transformuserid
  LEFT JOIN scratch_marcuscampbell.cdc_2018_sessions_activity csa ON csa.id = cs.id
  LEFT JOIN scratch_marcuscampbell.view_cdc_status cstat ON cstat.transformuserid = cs.transformuserid
WHERE
  cstat.startdate NOTNULL
--   AND cs.source = 'lesson'
GROUP BY cstat.starthalf
ORDER BY cstat.starthalf ASC
;


-- ############################################################################
--   Actual Weight Loss
-- ############################################################################

CREATE OR REPLACE VIEW  scratch_marcuscampbell.DPRP_2018_weight_loss_criteria   AS

WITH
    first_weight AS
  (
      SELECT DISTINCT ON (csw.transformuserid)
        *
      FROM
        scratch_marcuscampbell.cdc_2018_sessions_weights csw
      WHERE
        csw.value NOTNULL
      ORDER BY transformuserid, date
  ),
    last_session_weight_12mo AS (
      SELECT DISTINCT ON (csw.transformuserid)
        csw.id
        , csw.transformuserid
        , csw.date
        , csw.yearweek
        , csw.year
        , csw.value
        , csc.first_date
        , csw.date - csc.first_date AS days_in
      FROM
        scratch_marcuscampbell.cdc_2018_sessions_weights csw
        LEFT JOIN scratch_marcuscampbell.view_cdc_2018_sessions_count csc ON csc.transformuserid = csw.transformuserid
      WHERE
        csw.date <= csc.first_date + INTERVAL '12 months'
        AND csw.value NOTNULL
      ORDER BY csw.transformuserid, csw.date DESC
  )

SELECT
  csc.starthalf
  , count(*)                                                                       AS count_all_ppt
  , count(*)
      FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csc.last_session_gte_9_mo = 1) AS count_qual_ppt
  , avg((fw.value - lw.value) / fw.value)
      FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csc.last_session_gte_9_mo = 1) AS avg_pct_lost_qual_ppt
  , 0.05                                                                           AS req_avg_pct_lost_qual_ppt
FROM
  scratch_marcuscampbell.view_cdc_2018_sessions_count csc
  LEFT JOIN first_weight fw ON fw.transformuserid = csc.transformuserid
  LEFT JOIN last_session_weight_12mo lw ON lw.transformuserid = csc.transformuserid
GROUP BY csc.starthalf
ORDER BY csc.starthalf ASC
;


-- ############################################################################
--   Rate of Weight Loss
-- ############################################################################


--  Criteria 8 is  avg (weight loss of cohort)
--  Therefore I can normalize for time by avg(weight loss of cohort) / months in
CREATE OR REPLACE VIEW  scratch_marcuscampbell.DPRP_2018_weight_loss_trend      AS

WITH
    first_weight AS
  (
      SELECT DISTINCT ON (csw.transformuserid)
        csw.*
      FROM
        scratch_marcuscampbell.cdc_2018_sessions_weights csw
        LEFT JOIN scratch_marcuscampbell.view_cdc_2018_sessions cs ON cs.id = csw.id
      WHERE
        csw.value NOTNULL
--         AND cs.source = 'lesson'
      ORDER BY csw.transformuserid, csw.date ASC
  ),
    last_session_weight_12mo AS (
      SELECT DISTINCT ON (csw.transformuserid)
        csw.id
        , csw.transformuserid
        , csw.date
        , csw.yearweek
        , csw.year
        , csw.value
        , csc.first_date
        , csw.date - csc.first_date AS days_in
      FROM
        scratch_marcuscampbell.cdc_2018_sessions_weights csw
        LEFT JOIN scratch_marcuscampbell.view_cdc_2018_sessions cs ON cs.id = csw.id
        LEFT JOIN scratch_marcuscampbell.view_cdc_2018_sessions_count csc ON csc.transformuserid = csw.transformuserid
      WHERE
        csw.date <= csc.first_date + INTERVAL '12 months'
        AND csw.value NOTNULL
--         AND cs.source = 'lesson'
      ORDER BY csw.transformuserid, csw.date DESC
  ),
    loss AS (
      SELECT
        csc.transformuserid
        , csc.starthalf
        , cstat.cohort_lang
        , csc.gte_3_sessions_6_mo
        , fw.value                         AS first_weight_value
        , fw.date                          AS first_weight_date
        , lw.value                         AS last_weight_value
        , lw.date                          AS last_weight_date
        , (fw.value - lw.value) / fw.value AS pct_lost
        , CASE
          WHEN EXTRACT(YEAR FROM age(lw.date, fw.date)) > 1.0
            THEN 12
          ELSE EXTRACT(MONTH FROM age(lw.date, fw.date)) + (EXTRACT(DAY FROM age(lw.date, fw.date)) / 31)
          END                              AS pgrm_months
        , csc.last_session_gte_9_mo
      FROM
        scratch_marcuscampbell.view_cdc_status cstat
        LEFT JOIN scratch_marcuscampbell.view_cdc_2018_sessions_count csc ON csc.transformuserid = cstat.transformuserid
        LEFT JOIN first_weight fw ON fw.transformuserid = csc.transformuserid
        LEFT JOIN last_session_weight_12mo lw ON lw.transformuserid = csc.transformuserid
      WHERE
        csc.gte_3_sessions_6_mo = 1
  )
SELECT
  concat(cohort_lang, '-', starthalf) as key
  , count(DISTINCT transformuserid)            AS count_ppt_init_cdc_elig
  , avg(
        CASE WHEN pgrm_months > 0
          THEN pct_lost * 100 :: NUMERIC / pgrm_months
        ELSE NULL END
    )                                          AS avg_pct_loss_trend
  , 5.0 / 12                                   AS req_avg_pct_loss_trend
  , 5.0 / 10                                   AS req_avg_pct_loss_trend_w_safetyfactor
  , CASE WHEN avg(
                  CASE WHEN pgrm_months > 0
                    THEN pct_lost * 100 :: NUMERIC / pgrm_months
                  ELSE NULL END
              ) > (5.0 / 12)
  THEN 1
    ELSE 0 END                                 AS qual_8_rate
  , CASE WHEN avg(
                  CASE WHEN pgrm_months > 0
                    THEN pct_lost * 100 :: NUMERIC / pgrm_months
                  ELSE NULL END
              ) > (5.0 / 10)
  THEN 1
    ELSE 0 END                                 AS qual_8_rate_safe
  , count(DISTINCT transformuserid)
      FILTER (WHERE last_session_gte_9_mo = 1) AS count_ppt_cdc_elig
  , avg(pct_lost * 100.0)
      FILTER (WHERE last_session_gte_9_mo = 1) AS avg_pct_lost
  , avg(pgrm_months)
      FILTER (WHERE last_session_gte_9_mo = 1) AS avg_pgrm_months
  , CASE WHEN avg(pct_lost * 100.0)
                FILTER (WHERE last_session_gte_9_mo = 1) > 5.0
  THEN 1
    ELSE 0 END                                 AS qual_8
FROM loss
GROUP BY starthalf, cohort_lang
ORDER BY cohort_lang, starthalf ASC
;

-- ############################################################################
--   9: Blood Test Eligibile
-- ############################################################################

CREATE OR REPLACE VIEW scratch_marcuscampbell.DPRP_2018_bloodtest_criteria      AS

SELECT
  starthalf
  , all_ppt
  , all_ppt_blood_elig
  , CASE WHEN all_ppt_blood_elig > 0
  THEN
    all_ppt_blood_elig * 100 :: NUMERIC / all_ppt :: NUMERIC
    ELSE 0 END AS pct_all_ppt_blood_qual
  , 35.0       AS req_pct_all_ppt_blood_qual
  , qual_ppt
  , qual_ppt_blood_elig
  , CASE WHEN qual_ppt > 0
  THEN
    qual_ppt_blood_elig * 100 :: NUMERIC / qual_ppt :: NUMERIC
    ELSE 0 END AS pct_qual_ppt_blood_elig
  , 35.0       AS req_pct_qual_ppt_blood_elig
  , CASE WHEN CASE WHEN qual_ppt > 0
  THEN
    qual_ppt_blood_elig * 100 :: NUMERIC / qual_ppt :: NUMERIC
              ELSE 0 END > 35.0
  THEN 1
    ELSE 0 END AS qual_9_blood_eleg
FROM (
       SELECT
         cs.starthalf
         , count(*)                                                    AS all_ppt
         , count(*)
             FILTER (WHERE qb.any =
                           1)                                          AS all_ppt_blood_elig
         , count(*)
             FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csc.last_session_gte_9_mo = 1)                AS qual_ppt
         , count(*)
             FILTER (WHERE csc.gte_3_sessions_6_mo = 1 AND csc.last_session_gte_9_mo = 1 AND qb.any = 1) AS qual_ppt_blood_elig
       FROM
         scratch_marcuscampbell.view_cdc_status cs
         LEFT JOIN scratch_marcuscampbell.view_cdc_2018_sessions_count csc ON csc.transformuserid = cs.transformuserid
         LEFT JOIN scratch_marcuscampbell.marcus_cdc_qual_blood qb ON qb.transformuserid = csc.transformuserid
       WHERE
         cs.starthalf NOTNULL
       GROUP BY cs.starthalf
     ) q
ORDER BY starthalf ASC
;
