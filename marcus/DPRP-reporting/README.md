# DPRP Reporting Workflow


This folder contains all of the code necessary to create the DPRP report which we submit to the CDC on a semiannual basis.
If you are using this code to generate a DPRP report, it is critical to familiarize yourself with the reporting process.

### The approach can be broken down into four major steps:

#### A. Generation of CDC Qualification Tables 
We generate the "qualification tables". In a nutshell, the CDC requires us to report certain vital statistics for 
each participant (e.g. height, education, ethnographic info, etc.). We currently generate the following tables:

1.    cdc_height
2.    cdc_education
3.    cdc_ethnographic
4.    cdc_risk
5.    cdc_sex
6.    cdc_state
7.    cdc_qual_blood

To generate these tables, run the queries found in `generate-cdc-qual-tables.sql`.

*Please note that in the past, tables called "cdc_age" and "cdc_dob" were also generated, but these are no longer used.*

#### B. Intermediate Views and Tables

After the qualification tables are created, we need to find out which PPTs are eligible for CDC analysis, associate 
activity readings and weight measurements with PPTs, and then group all of this information together to determine which 
sessions should be submitted to the CDC. We do this using a combination of tables and views.
Run the queries found in **this order:** (1) `generate-cdc-status-tables.sql` and then (2) `generate-cdc-session-and-ppt-views.sql`.

#### C. Evaluation of CDC Criteria

Before we submit information to the CDC, it is important to check our data to ensure that we are actually meeting 
the CDC's criteria. We produce summary statistics to ensure we are meeting the DPRP requirements for: frequency of weight logging, 
% total weight loss, frequency of activity logging, minimum session attendance, and for proportion of PPTs that 
qualified via blood test. These summary tables are also useful for forecasting for future periods; some of the 
tables are coded with this specific purpose in mind, e.g. `scratch_marcuscampbell.DPRP_2018_attendance_trend`.
To generate the required tables, run the queries found in `generate-cdc-criteria-views.sql`.

#### D. Generation of the DPRP Report to be Submitted

After we're satisfied that we're meeting the CDC criteria, run the queries found in `generate-DPRP-report.sql`. This generates 
the DPRP report that will be submitted to the CDC. You should refer to the current edition of the 
DPRP reporting guidelines and ensure that any field listed as "Required" is reported. It is also critical to review the weight patterns,
and to investigate the weight data for any PPTs that experienced a weight loss of greater than 10% over the course of the program.
