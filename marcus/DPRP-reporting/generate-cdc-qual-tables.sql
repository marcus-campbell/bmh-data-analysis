--##########################################################################################################################
-- Copyright (C) Blue Mesa Health, Inc - All Rights Reserved
-- Unauthorized reproduction of this file, via any medium, is strictly prohibited
-- All code contained in this file is both proprietary and confidential
-- Written by Evan Willms (evan.willms@bluemesahealth.com) and Marcus Campbell (marcus.campbell@bluemesahealth.com)
--##########################################################################################################################

-- ####################################
-- Create and populate cdc_height table
-- ####################################

-- Drop previous version of marcus_cdc_height
DROP TABLE IF EXISTS    scratch_marcuscampbell.marcus_cdc_height;

-- Create new table using two columns in bmh_prod_na.transformuser
CREATE  TABLE   scratch_marcuscampbell.marcus_cdc_height
        AS
        SELECT  id::INTEGER   AS      transformuserid
                ,height::INTEGER
        FROM    bmh_prod_na.transformuser
        WHERE   1=1
                AND transformuser.id NOTNULL
                -- Exclude demo cohorts. Check https://docs.google.com/spreadsheets/d/1Tbtw_kJ_pu0FD0TddPoPM2ARCTFi8B8oWI3ZUOsvG9Q/edit?usp=sharing
                AND transformuser.cohortid NOT IN (7,8,9,10,15,16,19,222,223,353,354,355,500,518,632,646,657,1079,9988)
                -- Exclude CRC cohorts.
                AND transformuser.cohortid NOT IN (297, 489)
;

-- Set primary key
ALTER TABLE scratch_marcuscampbell.marcus_cdc_height
ADD PRIMARY KEY (transformuserid)
;

-- #######################################
-- Create and populate cdc_education table
-- #######################################

-- Drop previous version of marcus_cdc_education
DROP TABLE IF EXISTS    scratch_marcuscampbell.marcus_cdc_education;

CREATE TABLE scratch_marcuscampbell.marcus_cdc_education
       AS
       WITH old_survey_education AS (SELECT survey.leadid
                                     ,transformuser.id AS transformuserid
                                     ,survey.response ::JSONB ->> 'educationLevel' AS survey_education

                                     ,survey.updatedat
                              FROM  bmh_prod_na.survey
                                    INNER JOIN bmh_prod_na.lead
                                    ON survey.leadid = lead.id
                                    INNER JOIN bmh_prod_na.transformuser
                                    ON lead.uuid = transformuser.uuid
    )
    ,genericsurvey_education AS (SELECT gs.leadid
                                      , gltm.transformuserid
                                      , gs.response ::JSONB ->> 'educationLevel' AS survey_education
                                      ,gs.updatedat
                                 FROM   bmh_prod_na.genericsurvey gs
                                        INNER JOIN bmh_prod_na.genericleadtransformusermap gltm
                                        ON gs.leadid = gltm.leadid
    )
    ,union_education AS (SELECT *
                         FROM   old_survey_education
                         UNION ALL
                         SELECT *
                         FROM   genericsurvey_education)
SELECT DISTINCT ON(transformuserid)
        transformuserid
        ,CASE
          WHEN survey_education = 'lessThanGed'
            THEN 1
          WHEN survey_education = 'ged'
            THEN 2
          WHEN survey_education = 'someCollege'
            THEN 3
          WHEN survey_education = 'graduate'
            THEN 4
          ELSE 9
          END AS edu
FROM    union_education
ORDER BY transformuserid, updatedat DESC
;


-- ############################################
-- Create and populate cdc_ethnographic table
-- ############################################

-- Drop old version of cdc_ethnographic
DROP TABLE IF EXISTS    scratch_marcuscampbell.marcus_cdc_ethnographic;

-- Mostly Evan's code from here on, but I modified it somewhat to handle cases where people did not report their ethnicity.

CREATE TABLE scratch_marcuscampbell.marcus_cdc_ethnographic
       AS
       WITH old_survey_ethnographic AS (SELECT  transformuser.id   AS transformuserid
                                                ,lead.ethnicity    AS lead_ethnicity
                                        FROM    bmh_prod_na.transformuser
                                                INNER JOIN bmh_prod_na.lead
                                                ON lead.uuid = transformuser.uuid
           )
           ,genericsurvey_ethnographic AS (SELECT  gltm.transformuserid
                                                  ,gl.ethnicity AS lead_ethnicity
                                           FROM bmh_prod_na.genericlead gl
                                                INNER JOIN bmh_prod_na.genericleadtransformusermap gltm
                                                ON gl.id = gltm.leadid
           )
           ,union_ethnographic AS (SELECT *
                                   FROM old_survey_ethnographic
                                   UNION ALL
                                   SELECT *
                                   FROM genericsurvey_ethnographic
            )
SELECT DISTINCT ON(transformuserid)
        transformuserid
        ,CASE
            WHEN lead_ethnicity ISNULL
        THEN 9
        ELSE CASE WHEN lead_ethnicity LIKE '%Hispanic%' OR lead_ethnicity LIKE '%hispanic%'
        THEN 1
         ELSE 2 END END AS ETHNIC
         ,CASE WHEN lead_ethnicity LIKE '%Indian' OR lead_ethnicity LIKE '%indian%'
            THEN 1
            ELSE 2 END AS AIAN
         ,CASE WHEN lead_ethnicity LIKE '%Asian%' OR  lead_ethnicity LIKE '%asian%'
            THEN 1
        ELSE 2 END AS ASIAN
        ,CASE WHEN lead_ethnicity LIKE '%Black%' OR lead_ethnicity LIKE '%black%'
            THEN 1
        ELSE 2 END AS BLACK
        ,CASE WHEN lead_ethnicity LIKE '%Islander%' OR lead_ethnicity LIKE '%islander%'
            THEN 1
        ELSE 2 END AS NHOPI
        ,CASE WHEN lead_ethnicity LIKE '%White%' OR lead_ethnicity LIKE '%white%'
            THEN 1
        ELSE 2 END AS WHITE
FROM    union_ethnographic
;

-- ############################################
-- Create and populate cdc_risk table
-- ############################################

-- Drop previous version of table
DROP TABLE IF EXISTS    scratch_marcuscampbell.marcus_cdc_risk;

-- Create new table using Evan's old entries
CREATE TABLE    scratch_marcuscampbell.marcus_cdc_risk  AS
SELECT * FROM   cdc.cdc_risk;

-- Create primary key
ALTER TABLE     scratch_marcuscampbell.marcus_cdc_risk
ADD PRIMARY KEY (transformuserid);

-- Add new rows based on new information pulled from bmh_prod_na.transformuser and zoho.contacts
INSERT INTO scratch_marcuscampbell.marcus_cdc_risk (transformuserid, risk_qual)

  SELECT
      tu.id              AS transformuserid
    , max(CASE WHEN tu.qualifiedbyriskscore IS TRUE
    THEN 1
          WHEN zc."ADA Score" :: INTEGER >= 5
            THEN 1
          WHEN tu.qualifiedbyriskscore IS FALSE
            THEN 0
          ELSE NULL END) AS risk_qual
  FROM
    bmh_prod_na.transformuser tu
    LEFT JOIN zoho.contacts zc ON zc."Participant ID" :: INTEGER = tu.id

  WHERE
    tu.id NOTNULL

  GROUP BY tu.id

ON CONFLICT (transformuserid)
  DO UPDATE SET risk_qual = EXCLUDED.risk_qual
    WHERE
      (marcus_cdc_risk.risk_qual <> 1) OR (marcus_cdc_risk.risk_qual ISNULL);

-- ###########################
-- Create and populate cdc_sex
-- ###########################

-- Drop previous version of marcus_cdc_sex
DROP TABLE IF EXISTS    scratch_marcuscampbell.marcus_cdc_sex;

CREATE  TABLE   scratch_marcuscampbell.marcus_cdc_sex
        AS
        WITH sex_from_old_leads AS (SELECT    transformuser.id::INTEGER AS transformuserid
                                     ,biologicalsex::TEXT
                                     ,cohortid
                           FROM bmh_prod_na.transformuser
                                INNER JOIN bmh_prod_na.lead
                                ON transformuser.uuid = lead.uuid
                           WHERE    transformuser.id NOTNULL

             )
            ,sex_from_current_leads AS (SELECT transformuser.id AS transformuserid
                                                   ,biologicalsex::TEXT
                                                   ,cohortid
                                            FROM    bmh_prod_na.genericlead gl
                                                    INNER JOIN genericleadtransformusermap gltm
                                                    ON gl.id = gltm.leadid
                                                    INNER JOIN transformuser
                                                    ON transformuser.id = gltm.transformuserid
                                            WHERE   transformuser.id NOTNULL

            )
            ,union_sex AS (SELECT *
                              FROM  sex_from_old_leads

                              UNION ALL

                              SELECT *
                              FROM  sex_from_current_leads
            )
SELECT DISTINCT ON(transformuserid)
        transformuserid
        ,biologicalsex
FROM    union_sex
WHERE   1=1
        AND cohortid NOT IN (7, 8, 9, 10, 15, 16, 19, 222, 223, 353, 354, 355, 500, 518, 632, 646, 657, 1079, 9988)
        -- Demo cohorts can be found in: https://docs.google.com/spreadsheets/d/1Tbtw_kJ_pu0FD0TddPoPM2ARCTFi8B8oWI3ZUOsvG9Q/edit?usp=sharing
        -- Exclude CRC cohorts.
        AND cohortid NOT IN (297, 489)
;

-- For now we are making transformuserid a PRIMARY KEY in order to copy Evan's format
ALTER TABLE   scratch_marcuscampbell.marcus_cdc_sex
      ADD PRIMARY KEY (transformuserid);

-- ##############################
-- Create and populate cdc_state
-- ##############################

-- Drop previous version of marcus_cdc_state
DROP TABLE IF EXISTS    scratch_marcuscampbell.marcus_cdc_state;

CREATE  TABLE   scratch_marcuscampbell.marcus_cdc_state
        AS
        WITH state_from_old_leads AS (SELECT    transformuser.id::INTEGER AS transformuserid
                                     ,state::TEXT
                                     ,cohortid
                           FROM bmh_prod_na.transformuser
                                INNER JOIN bmh_prod_na.lead
                                ON transformuser.uuid = lead.uuid
                           WHERE    transformuser.id NOTNULL

             )
            ,state_from_current_leads AS (SELECT transformuser.id AS transformuserid
                                                   ,state::TEXT
                                                   ,cohortid
                                            FROM    bmh_prod_na.genericlead gl
                                                    INNER JOIN genericleadtransformusermap gltm
                                                    ON gl.id = gltm.leadid
                                                    INNER JOIN transformuser
                                                    ON transformuser.id = gltm.transformuserid
                                            WHERE   transformuser.id NOTNULL

            )
            ,union_state AS (SELECT *
                              FROM  state_from_old_leads

                              UNION ALL

                              SELECT *
                              FROM  state_from_current_leads
            )
SELECT DISTINCT ON(transformuserid)
        transformuserid
        ,state
FROM    union_state
WHERE   1=1
        AND cohortid NOT IN (7, 8, 9, 10, 15, 16, 19, 222, 223, 353, 354, 355, 500, 518, 632, 646, 657, 1079, 9988)
        -- Demo cohorts can be found in: https://docs.google.com/spreadsheets/d/1Tbtw_kJ_pu0FD0TddPoPM2ARCTFi8B8oWI3ZUOsvG9Q/edit?usp=sharing
        -- Exclude CRC cohorts.
        AND cohortid NOT IN (297, 489)
;

        
-- #####################################
-- Create and populate cdc_qual_blood
-- #####################################

-- Drop previous version of marcus_cdc_qual_blood
-- CASCADE dependency affects scratch_marcuscampbell.dprp_2018_bloodtest_criteria
DROP TABLE IF EXISTS    scratch_marcuscampbell.marcus_cdc_qual_blood    CASCADE;

-- Create new table using two columns in bmh_prod_na.transformuser
CREATE  TABLE   scratch_marcuscampbell.marcus_cdc_qual_blood    AS
        SELECT  transformuser.id::INTEGER       AS      transformuserid,
                CAST(qualifiedbyblood AS INTEGER)       AS      docblood,
                CAST(qualifiedbygdm AS INTEGER)         AS      gdm,
                CAST(qualifiedbydoctor AS INTEGER)      AS      doceval
        FROM    bmh_prod_na.transformuser
        WHERE   transformuser.id NOTNULL AND
                transformuser.cohortid NOT IN (7,8,9,10,15,16,19,222,223,353,354,355,500,518,632,646,657,1079,9988)
-- Exclude CRC cohorts.
                AND transformuser.cohortid NOT IN (297, 489)
;

-- Demo cohorts can be found in: https://docs.google.com/spreadsheets/d/1Tbtw_kJ_pu0FD0TddPoPM2ARCTFi8B8oWI3ZUOsvG9Q/edit?usp=sharing

-- For now we are making transformuserid a PRIMARY KEY now before we start adding more info from other tables
ALTER   TABLE   scratch_marcuscampbell.marcus_cdc_qual_blood
        ADD PRIMARY KEY (transformuserid);

-- Add additional columns to mimic cdc.cdc_qual_blood
ALTER   TABLE   scratch_marcuscampbell.marcus_cdc_qual_blood
ADD COLUMN      a1c     SMALLINT,
ADD COLUMN      fasting SMALLINT,
ADD COLUMN      oral    SMALLINT,
ADD COLUMN      "any"   SMALLINT,
ADD COLUMN      source  TEXT;

-- Update with old info from CDC
-- do a1c, fasting, oral, and source first
UPDATE  scratch_marcuscampbell.marcus_cdc_qual_blood
SET     a1c = cdc.cdc_qual_blood.a1c,
        fasting = cdc.cdc_qual_blood.fasting,
        oral = cdc.cdc_qual_blood.oral,
        source = cdc.cdc_qual_blood.source
FROM    cdc.cdc_qual_blood
WHERE   marcus_cdc_qual_blood.transformuserid = cdc_qual_blood.transformuserid;

-- now do gdm
UPDATE  scratch_marcuscampbell.marcus_cdc_qual_blood
SET     gdm = cdc.cdc_qual_blood.gdm
FROM    cdc.cdc_qual_blood
WHERE   marcus_cdc_qual_blood.transformuserid = cdc_qual_blood.transformuserid AND
        marcus_cdc_qual_blood.gdm ISNULL;

-- now do docblood
UPDATE  scratch_marcuscampbell.marcus_cdc_qual_blood
SET     docblood = cdc.cdc_qual_blood.docblood
FROM    cdc.cdc_qual_blood
WHERE   marcus_cdc_qual_blood.transformuserid = cdc_qual_blood.transformuserid AND
        marcus_cdc_qual_blood.docblood ISNULL;

-- now do doceval
UPDATE  scratch_marcuscampbell.marcus_cdc_qual_blood
SET     doceval = cdc.cdc_qual_blood.doceval
FROM    cdc.cdc_qual_blood
WHERE   marcus_cdc_qual_blood.transformuserid = cdc_qual_blood.transformuserid AND
        marcus_cdc_qual_blood.doceval ISNULL;

-- finally, update "any"
-- We are purposely excluding doceval, in order to meet the CDC's 2018 submission guidelines
UPDATE scratch_marcuscampbell.marcus_cdc_qual_blood
SET "any" =
CASE WHEN
  COALESCE(a1c :: INTEGER, 0)
  + COALESCE(fasting :: INTEGER, 0)
  + COALESCE(oral :: INTEGER, 0)
  + COALESCE(gdm :: INTEGER, 0)
  + COALESCE(docblood :: INTEGER, 0)
  >= 1
 THEN 1
 ELSE 0
 END;


