#### PREAMBLE ####

# Copyright (C) Blue Mesa Health, Inc - All Rights Reserved
# Unauthorized reproduction of this file, via any medium, is strictly prohibited
# All code contained in this file is both proprietary and confidential
# Written by Marcus Campbell marcus.campbell@bluemesahealth.com, September 2019

#### Import Packages ####

import os
import numpy as np
import pandas as pd

#### Import raw data ####

path = os.getcwd()
#raw_submitted_files_path = os.path.join(path, 'corrected_raw_submitted_DPRP_reports')
raw_submitted_files_path = os.path.join(path, 'corrected_raw_submitted_DPRP_reports')

# Create sorted list of correct DPRP submission files to parse
submitted_files = sorted([f for f in os.listdir(raw_submitted_files_path) if f.endswith('.csv')])

# Read corrected submissions into a single dataframe
submissions_df = pd.DataFrame()

for f in submitted_files:
    print('Reading file: ', "'", f,"'", '...', sep='')
    parsed_df = pd.read_csv(os.path.join(raw_submitted_files_path, f))
    submissions_df = submissions_df.append(parsed_df)
    
    if f==submitted_files[-1]:
        print('All files parsed.')

#### Investigate data ####

# Create dataframe of first chronological session for each participant
        
submissions_df.sort_values(['particip', 'date', 'sesstype'], ascending=True, inplace=True)

first_session_df = submissions_df.groupby('particip').nth(0).reset_index()

## Sanity checks ##

# Quick look at data
print(first_session_df.head())

# first_session_df should contain no duplicate participant IDs; expected result is False
assert any(first_session_df['particip'].duplicated()) == False
# Particip 24 should have first date of 2016-05-20
assert first_session_df.loc[first_session_df['particip'] == 24,'date'].values[0] == '2016-05-20'
# Particip 16855 should have first date of 2019-06-23
assert first_session_df.loc[first_session_df['particip'] == 16855,'date'].values[0] == '2019-06-23'

# Add BMI column
first_session_df['start_bmi'] = first_session_df['weight'] * 703 / (first_session_df['height'] ** 2)

# Convert date column to datetime type
first_session_df['date'] = pd.to_datetime(first_session_df['date'])
# Create starthalf column
first_session_df['start_half'] = np.where(first_session_df['date'].dt.month <= 6,
                                          first_session_df['date'].dt.year.map(str) + '-1',
                                          first_session_df['date'].dt.year.map(str) + '-2')
# Subset columns
first_session_df = first_session_df[['particip', 'start_bmi', 'start_half']]
# Join start half and starting BMI columns to submissions_df
submissions_df = pd.merge(submissions_df, first_session_df, how='inner', on='particip')

# Filter submissions_df to exclude start BMI's of < 25 for non-asians; exclude BMI's of < 23 for asians.
filtered_bmi_df = submissions_df.query('start_bmi >= 23 and asian == 1 or start_bmi >= 25')

filtered_bmi_df['date'] = pd.to_datetime(filtered_bmi_df['date'])
filtered_bmi_df['sessions_to_date'] = filtered_bmi_df.groupby('particip')['sessid'].cumcount()+1
filtered_bmi_df['total_sessions'] = filtered_bmi_df.groupby('particip')['sessid'].transform(lambda x: len(x))
filtered_bmi_df['months_to_date'] = filtered_bmi_df.groupby('particip')['date'].transform(lambda x: ((x - x.iloc[0]) / np.timedelta64(1, 'M')))
filtered_bmi_df['total_months_in_program'] = filtered_bmi_df.groupby('particip')['date'].transform(lambda x: ((x.iloc[-1] - x.iloc[0]) / np.timedelta64(1, 'M')))

# Create dataframe of participants with less than 3 sessions in their first 6 program months; used for filtering later
less_than_3sessions_first_6months_df = filtered_bmi_df.query('months_to_date <= 6')
less_than_3sessions_first_6months_df['num_sessions_in_first_6months'] = less_than_3sessions_first_6months_df.groupby('particip')['sessid'].transform(lambda x: len(x))
less_than_3sessions_first_6months_df = less_than_3sessions_first_6months_df.query('num_sessions_in_first_6months < 3').drop_duplicates('particip', keep='first')
less_than_3sessions_first_6months_particip_list = less_than_3sessions_first_6months_df['particip']
less_than_3sessions_first_6months_df = less_than_3sessions_first_6months_df.groupby('start_half')['particip'].agg('count').reset_index()

# Create dataframe of participants with less than 9 total months in the program
less_than_9months_df = filtered_bmi_df[~filtered_bmi_df['particip'].isin(less_than_3sessions_first_6months_particip_list)] # do not consider ppts 
less_than_9months_df = less_than_9months_df.query('total_months_in_program < 9').drop_duplicates('particip', keep='first')
less_than_9months_particip_list = less_than_9months_df['particip']
less_than_9months_df = less_than_9months_df.groupby('start_half')['particip'].agg('count').reset_index()

ineligible_particips_for_analysis_list = pd.concat([less_than_3sessions_first_6months_particip_list,less_than_9months_particip_list])

# Create dataframe used by the CDC for analysis
filtered_df = filtered_bmi_df[~filtered_bmi_df['particip'].isin(ineligible_particips_for_analysis_list)]

### Total submitted ppts per start half ###
ppts_per_start_half_df = submissions_df.groupby('start_half')['particip'].nunique().reset_index()

### Total ppts that are ineligible due to BMI per start half ###
bmi_excluded_df = submissions_df.query('start_bmi < 23 and asian == 1 or start_bmi < 25')
bmi_excluded_df = bmi_excluded_df.groupby('start_half')['particip'].nunique()

### Total submitted ppts that are eligible for DPRP analysis ###
dprp_eligible_ppts_per_start_half_df = filtered_df.groupby('start_half')['particip'].nunique().reset_index()

### Blood Test / GDM Eligibility ###

# test_df = filtered_df.query('risktest != 1')

last_session_df = filtered_df.groupby('particip').nth(-1).reset_index() # keep only last record for participant
blood_eligibility_df = last_session_df.query('gdm == 1 or gluctest == 1')[['start_half', 'gdm']].groupby('start_half').agg('count').reset_index()
blood_eligibility_df = blood_eligibility_df.rename(columns={'gdm': 'participants_w_blood_test'})
blood_eligibility_df['total_participants'] = last_session_df.groupby('start_half').agg('count').reset_index()[['gdm']]
blood_eligibility_df['percent_ppts_blood_test_eligible'] = blood_eligibility_df['participants_w_blood_test'] / blood_eligibility_df['total_participants']

### Weight Logging Analysis ###

weight_logging_df = filtered_df.query('weight != 999')[['start_half', 'weight']].groupby('start_half').agg('count').reset_index()
weight_logging_df = weight_logging_df.rename(columns={'weight':'sessions_with_weight'})
weight_logging_df['total_sessions'] = filtered_df[['start_half', 'weight']].groupby('start_half').agg('count').reset_index()[['weight']]
weight_logging_df['percent_sessions_with_weight'] = weight_logging_df['sessions_with_weight'] / weight_logging_df['total_sessions']

### Activity Logging Analysis ###

activity_logging_df = filtered_df.query('pa != 999 and pa != 0')[['start_half', 'pa']].groupby('start_half').agg('count').reset_index()
activity_logging_df = activity_logging_df.rename(columns={'pa':'sessions_with_activity'})
activity_logging_df['total_sessions'] = filtered_df[['start_half', 'pa']].groupby('start_half').agg('count').reset_index()[['pa']]
activity_logging_df['percent_sessions_with_activity'] = activity_logging_df['sessions_with_activity'] / activity_logging_df['total_sessions']

### Weight Loss Analysis ###

weight_loss_df = filtered_df.query('weight != 999') # remove records without weight logs
weight_loss_df = weight_loss_df.drop_duplicates(['particip', 'date', 'weight'], keep='first')
weight_loss_df = weight_loss_df.groupby('particip').nth([0,-1]).reset_index() # keep only first and last record for each participant
weight_loss_df = weight_loss_df.groupby('particip').filter(lambda x: len(x) == 2) # only keep participants with more than one record
weight_loss_df['percent_weight_loss'] = weight_loss_df.groupby('particip')['weight'].transform(lambda x: ((x.iloc[-1] - x.iloc[0]) / x.iloc[0]) * 100) # % weight loss between first and last session
weight_loss_df = weight_loss_df.groupby('start_half')['percent_weight_loss'].agg('mean').reset_index() # mean weight loss for each start half

### Session Completion Analysis ###

# At least 9 sessions in months 1-6

sessions_by_month_6 = filtered_df.query('months_to_date <= 6').groupby('particip').nth(-1).reset_index()
sessions_months_1to6_df = sessions_by_month_6.query('sessions_to_date >= 9')[['start_half', 'particip']].groupby('start_half').agg('count').reset_index()
sessions_months_1to6_df = sessions_months_1to6_df.rename(columns={'particip': 'gte_9_sessions_in_months_1to6'})
sessions_months_1to6_df['total_ppts_that_reached_6mo'] = sessions_by_month_6.groupby('start_half').agg('count').reset_index()[['particip']]
sessions_months_1to6_df['percent_ppts_with_gte_9_sessions_in_months_1to6'] = sessions_months_1to6_df['gte_9_sessions_in_months_1to6'] / sessions_months_1to6_df['total_ppts_that_reached_6mo']

# At least 3 sesssions in months 7-12

sessions_in_months_7to12 = filtered_df.query('months_to_date >= 6').groupby('particip').nth([0,-1]).reset_index()
sessions_in_months_7to12['sessions_in_mo_7to12'] = sessions_in_months_7to12.groupby('particip')['sessions_to_date'].transform(lambda x: (x.iloc[-1] - x.iloc[0]) + 1)
sessions_in_months_7to12 = sessions_in_months_7to12.drop_duplicates('particip', keep='first')
sessions_months_7to12 = sessions_in_months_7to12.query('sessions_in_mo_7to12 >= 3')[['start_half', 'particip']].groupby('start_half').agg('count').reset_index()
sessions_months_7to12 = sessions_months_7to12.rename(columns={'particip': 'gte_3_sessions_in_months_7to12'})
sessions_months_7to12['total_ppts_that_reached_7mo'] = sessions_in_months_7to12.groupby('start_half').agg('count').reset_index()[['particip']]
sessions_months_7to12['percent_ppts_with_gte_3_sessions_in_months_7to12'] = sessions_months_7to12['gte_3_sessions_in_months_7to12'] / sessions_months_7to12['total_ppts_that_reached_7mo']

# Output dataframe that CDC uses for analysis

# export_path = os.path.join(path, 'exported_data/BMH_participant_data_for_CDC_analysis.csv')
# filtered_df.to_csv(export_path, index=False)


### Analysis of physical activity patterns ###

activity_analysis_df = filtered_df.query('pa != 999 and pa != 0').groupby('sessions_to_date', as_index=False)['pa'].agg('mean')

# Analyze subpopulation used for DPRP analysis
no_activity_logging_df = filtered_df.query('pa != 999 and pa != 0')[['sessions_to_date', 'pa']].groupby('sessions_to_date').agg('count').reset_index()
no_activity_logging_df = no_activity_logging_df.rename(columns={'pa':'sessions_with_activity'})
no_activity_logging_df['total_sessions'] = filtered_df[['sessions_to_date', 'pa']].groupby('sessions_to_date').agg('count').reset_index()[['pa']]
no_activity_logging_df['percent_sessions_with_activity'] = no_activity_logging_df['sessions_with_activity'] / no_activity_logging_df['total_sessions']

# Analyze full population
new_submissions_df = submissions_df
new_submissions_df['sessions_to_date'] = new_submissions_df.groupby('particip')['sessid'].cumcount()+1

full_pop_no_activity_logging_df = new_submissions_df.query('pa != 999 and pa != 0')[['sessions_to_date', 'pa']].groupby('sessions_to_date').agg('count').reset_index()
full_pop_no_activity_logging_df = full_pop_no_activity_logging_df.rename(columns={'pa':'sessions_with_activity'})
full_pop_no_activity_logging_df['total_sessions'] = new_submissions_df[['sessions_to_date', 'pa']].groupby('sessions_to_date').agg('count').reset_index()[['pa']]
full_pop_no_activity_logging_df['percent_sessions_with_activity'] = full_pop_no_activity_logging_df['sessions_with_activity'] / full_pop_no_activity_logging_df['total_sessions']

