WITH
     mls  AS (
SELECT *
       ,CASE WHEN milestonetype = 'M1' THEN 96
             WHEN milestonetype = 'M2' THEN 83
             WHEN milestonetype = 'M3' THEN 93
             WHEN milestonetype = 'M4' THEN 118.60
             ELSE 0
        END AS milestone_value
       FROM solera.milestones_bmh_computed
       INNER JOIN solera.milestone_status
       ON milestones_bmh_computed.id = milestone_status.bmhmilestoneid
       WHERE 1=1 AND
             milestone_status.statustypeid = 5 AND
             milestonedate BETWEEN '2018-01-01'::DATE AND '2019-03-01'::DATE
     )
SELECT COUNT(bmhmilestoneid),
       SUM(milestone_value)
FROM mls
;

SELECT *
       ,CASE WHEN milestonetype = 'M1' THEN 96
             WHEN milestonetype = 'M2' THEN 83
             WHEN milestonetype = 'M3' THEN 93
             WHEN milestonetype = 'M4' THEN 118.60
             ELSE 0
        END AS milestone_value
       FROM solera.milestones_bmh_computed
       INNER JOIN solera.milestone_status
       ON milestones_bmh_computed.id = milestone_status.bmhmilestoneid
       WHERE 1=1 AND
             milestone_status.statustypeid = 5 AND
             milestonedate BETWEEN '2018-01-01'::DATE AND '2019-03-01'::DATE
;