select
       er.solera_id,
 string_agg(enrolled::text, ''  order by er.filename)
 from solera.milestone_status ms
  left join solera.milestones_bmh_computed mbc on mbc.id = ms.bmhmilestoneid
left join solera.enrollmentresponse er on er.solera_id ::text = mbc.soleraid::text
where ms.statustypeid = 13
group by er.solera_id