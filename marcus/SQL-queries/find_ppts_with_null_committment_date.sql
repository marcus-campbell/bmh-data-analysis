SELECT mbc.soleraid
      ,mbc.transformuserid
      ,mbc.milestonetype
      ,mbc.milestonedate
      ,ms.statustypeid
      ,enrep.message
FROM  solera.milestones_bmh_computed mbc
LEFT JOIN
      solera.milestone_status ms
ON    mbc.id = ms.bmhmilestoneid
INNER JOIN
      (SELECT DISTINCT ON (solera_id) *
       FROM solera.enrollmentresponse solera
      WHERE solera.message LIKE '%Committment%') enrep
ON mbc.soleraid = enrep.solera_id
WHERE mbc.milestonedate >= '2018-01-01'::DATE AND
      ms.statustypeid != 1
;
