-- TEST: Check for any rows that contain ANY null value
-- Expected output: Table should contain no rows.
SELECT  *
FROM  scratch_marcuscampbell.dprp_submission_2020_h2 dprp
WHERE NOT(dprp IS NOT NULL)
;

-- TEST: Check for any age values that are outside reasonable bounds
SELECT  *
FROM  scratch_marcuscampbell.dprp_submission_2020_h2 dprp
WHERE   age < 18 OR age > 100
;

-- TEST: Check that height values are recorded for all participants
-- Note that this also catches values of "999" which correspond to NULL entries.
SELECT  *
FROM  scratch_marcuscampbell.dprp_submission_2020_h2 dprp
WHERE   height < 50 OR height > 100
;

-- TEST: (1) No more than 26 sessions in first 6 months for any user,
--       (2) No more than 3 sessions in months 7-9 for any user
--       (3) No more than 3 sessions in months 10-12 for any user
-- Expected output: Table should contain no rows.
SELECT  transformuserid
        ,period
        ,COUNT(*)
FROM  scratch_marcuscampbell.view_cdc_2018_sessions
GROUP BY transformuserid, period
HAVING (COUNT(*) > 26 AND period = 'lte_6mo')
       OR (COUNT(*) > 3 AND period = 'btw_7mo_9mo')
       OR (COUNT(*) > 3 AND period = 'btw_10mo_12mo')
;

-- TEST: No core sessions with duplicate session IDs
-- Expected output: Table should contain no rows.
SELECT particip
      ,COUNT(*)
FROM  scratch_marcuscampbell.dprp_submission_2020_h2
GROUP BY  particip, sessid
HAVING sessid != 99
       AND COUNT(*) > 1
;

-- TEST: Check if any male participants have GDM = 1.
-- This is actually allowed, but it is so unusual that the DPRP will always send us an error response asking us to confirm this information.

SELECT *
FROM scratch_marcuscampbell.dprp_submission_2020_h2
WHERE 1=1
      AND sex = 1
      AND gdm = 1
;

-- TEST: Check for any rows that contain ANY null value
-- Expected output: Table should contain no rows.
SELECT  DISTINCT particip
FROM  scratch_marcuscampbell.dprp_submission_2020_h2 dprp
WHERE NOT(dprp IS NOT NULL)
;