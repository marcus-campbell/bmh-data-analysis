-- This file is used by the bmh-transformloopback server to generate engagement files for Solera
-- This logic is also manually copied into several locations for other operations and reporting.
-- Any updates to this query must also be reflected in:
--    Google Drive/../001 Solera Reporting/SQL Queries/engagement-history-single-bmh-participant.sql
--    Google Drive/../001 Solera Reporting/SQL Queries/engagement-history-single-solera-participant.sql
--    Looker -> dt_engagement_data


SELECT
  *
FROM (

       -- -----------------------------------------------------------------
       -- START WEIGHT is summarized onto the participant's start date, if we're reporting on that period
       -- -----------------------------------------------------------------
       SELECT
           '122'                                               AS "Partner ID"
         , s.soleraid                                          AS "Solera ID"
         , w.transformuserid                                   AS "NW Blue Mesa"
         , to_char(w.timestamp, 'YYYYMMDD')                    AS "Activity Date"
         , to_char(min(w.valuelbs :: FLOAT), 'FM999999990.00') AS "Weight lbs"
         , ''                                                  AS "Physical Activity Minutes"
         , ''                                                  AS "Articles Read"
         , ''                                                  AS "Meals Logged"
         , ''                                                  AS "Group Interactions"
         , ''                                                  AS "Group Interaction Type"
         , ''                                                  AS "Coach Interactions"
         , ''                                                  AS "Coach Interaction Type"
         , ''                                                  AS "Coach Interaction Duration"
         , ''                                                  AS "Quiz/Survey's Completed"
         , ''                                                  AS "Lessons/Education Modules Completed"
         , ''                                                  AS "App Points Obtained"
         , ''                                                  AS "Weekly Video Watched (%)"
         , ''                                                  AS "Goal Behaviors Set"
         , ''                                                  AS "Coach Name"
       FROM (
              SELECT
                  tu.id            AS transformuserid
                , wb.start_weight  AS valuelbs
                , cohort.startdate AS timestamp
              FROM
                bmh_prod_na.transformuser tu
                LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                LEFT JOIN bmh_prod_na.view_weight_bounds wb ON wb.transformuserid = tu.id
              WHERE 1=1
                AND cohort.startdate :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                AND wb.start_weight NOTNULL

            ) w
         LEFT JOIN bmh_prod_na.soleramap s ON s.transformuserid = w.transformuserid
         LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = w.transformuserid
         LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid

       GROUP BY w.transformuserid, to_char(w.timestamp, 'YYYYMMDD'), s.soleraid
       HAVING soleraid NOTNULL

       UNION

       -- -----------------------------------------------------------------
       -- WEIGHT readings after cohort start date
       -- -----------------------------------------------------------------
       SELECT
           '122'                                               AS "Partner ID"
         , S.soleraid                                          AS "Solera ID"
         , W.transformuserid                                   AS "NW Blue Mesa"
         , REPLACE(LEFT(W.timestamp, 10), '-', '')             AS "Activity Date"
         , to_char(MIN(W.valuelbs :: FLOAT), 'FM999999990.00') AS "Weight lbs"
         , ''                                                  AS "Physical Activity Minutes"
         , ''                                                  AS "Articles Read"
         , ''                                                  AS "Meals Logged"
         , ''                                                  AS "Group Interactions"
         , ''                                                  AS "Group Interaction Type"
         , ''                                                  AS "Coach Interactions"
         , ''                                                  AS "Coach Interaction Type"
         , ''                                                  AS "Coach Interaction Duration"
         , ''                                                  AS "Quiz/Survey's Completed"
         , ''                                                  AS "Lessons/Education Modules Completed"
         , ''                                                  AS "App Points Obtained"
         , ''                                                  AS "Weekly Video Watched (%)"
         , ''                                                  AS "Goal Behaviors Set"
         , ''                                                  AS "Coach Name"
       FROM (
              SELECT
                tu.id AS transformuserid
                , W.valuelbs
                , W.timestamp
              FROM
                bmh_prod_na.transformuser tu
                LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                LEFT JOIN (
                            -- If the first weight ever for a participant is during the first program week,
                            -- in would appear above query result, duplicating the "Start Weight" information in the section above.
                            -- Therefore, identify that first weight IFF it's in a participant's first program week and hide
                            -- all of the weights on that day from the reporting list below to cleanly move that first weight
                            -- back to the cohort start date
                            SELECT
                              s.transformuserid
                              , s.valuelbs
                              , s.timestamp
                            FROM (
                                   -- Get the global first weight for a participant
                                   SELECT DISTINCT ON (tu.id)
                                     tu.id AS transformuserid
                                     , W.valuelbs
                                     , W.timestamp :: DATE
                                   FROM
                                     bmh_prod_na.transformuser tu
                                     LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                                     LEFT JOIN bmh_prod_na.weight W ON W.transformuserid = tu.id
                                                                       AND W.deletedat ISNULL
                                   WHERE
                                     W.valuelbs NOTNULL AND W.deletedat ISNULL
                                   ORDER BY tu.id, timestamp ASC
                                 ) s
                              LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = transformuserid
                              LEFT JOIN bmh_prod_na.cohort ON cohort.id = tu.cohortid
                            -- And then only keep it if it happens to be in their first program week
                            WHERE
                              timestamp :: DATE BETWEEN cohort.startdate :: DATE
                              AND
                              cohort.startdate + INTERVAL '6 DAYS'

                          ) if_first_weight_in_first_period ON if_first_weight_in_first_period.transformuserid = tu.id
                LEFT JOIN bmh_prod_na.weight W ON W.transformuserid = tu.id
                                                  AND W.deletedat ISNULL
                                                  AND W.timestamp :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                                                  AND W.timestamp :: DATE > cohort.startdate :: DATE
                                                  AND W.timestamp :: DATE IS DISTINCT FROM
                                                      (if_first_weight_in_first_period.timestamp :: DATE)


              WHERE
                W.valuelbs NOTNULL AND W.deletedat ISNULL

            ) W
         LEFT JOIN bmh_prod_na.soleramap S ON S.transformuserid = W.transformuserid
         LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = W.transformuserid
         LEFT JOIN bmh_prod_na.cohort C ON C.id = tu.cohortid
       WHERE
         W.timestamp :: TIMESTAMP >= C.startdate :: TIMESTAMP
       GROUP BY W.transformuserid, REPLACE(LEFT(W.timestamp, 10), '-', ''), S.soleraid
       HAVING soleraid NOTNULL) blah
WHERE blah."NW Blue Mesa" = 10031
ORDER BY "NW Blue Mesa" ASC, "Activity Date" ASC;
