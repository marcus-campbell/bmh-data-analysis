
WITH all_engagement AS (

       -- -----------------------------------------------------------------
       -- START WEIGHT is summarized onto the participant's start date, if we're reporting on that period
       -- -----------------------------------------------------------------
       SELECT
         w.transformuserid                                   AS "NW Blue Mesa"
         , to_char(w.timestamp, 'YYYYMMDD')                    AS "Activity Date"
         , MIN(w.valuelbs :: FLOAT)                            AS "Weight lbs"
         , 0                                                   AS "Physical Activity Minutes"
         , 0                                                  AS "Articles Read"
         , 0                                                  AS "Meals Logged"
         , 0                                                  AS "Group Interactions"
         , 0                                                  AS "Coach Interactions"
         , ''                                                  AS "Coach Interaction Type"
         , 0                                                  AS "Coach Interaction Duration"
         , 0                                                  AS "Quiz/Survey's Completed"
         , 0                                                  AS "Lessons/Education Modules Completed"
       FROM (
              SELECT
                  tu.id            AS transformuserid
                , wb.start_weight  AS valuelbs
                , cohort.startdate AS timestamp
              FROM
                bmh_prod_na.transformuser tu
                LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                LEFT JOIN bmh_prod_na.view_weight_bounds wb ON wb.transformuserid = tu.id
              WHERE 1=1
                AND cohort.startdate :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                AND wb.start_weight NOTNULL
            ) w
         LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = w.transformuserid
         LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid

       GROUP BY w.transformuserid, to_char(w.timestamp, 'YYYYMMDD')

       UNION

       -- -----------------------------------------------------------------
       -- WEIGHT readings after cohort start date
       -- -----------------------------------------------------------------
       SELECT
         W.transformuserid                                   AS "NW Blue Mesa"
         , REPLACE(LEFT(W.timestamp, 10), '-', '')             AS "Activity Date"
         , MIN(W.valuelbs :: FLOAT)                            AS "Weight lbs"
         , 0                                                  AS "Physical Activity Minutes"
         , 0                                                  AS "Articles Read"
         , 0                                                  AS "Meals Logged"
         , 0                                                  AS "Group Interactions"
         , 0                                                  AS "Coach Interactions"
         , ''                                                  AS "Coach Interaction Type"
         , 0                                                  AS "Coach Interaction Duration"
         , 0                                                  AS "Quiz/Survey's Completed"
         , 0                                                  AS "Lessons/Education Modules Completed"
       FROM (
              SELECT
                tu.id AS transformuserid
                , W.valuelbs
                , W.timestamp
              FROM
                bmh_prod_na.transformuser tu
                LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                LEFT JOIN (
                            -- If the first weight ever for a participant is during the first program week,
                            -- in would appear above query result, duplicating the "Start Weight" information in the section above.
                            -- Therefore, identify that first weight IFF it's in a participant's first program week and hide
                            -- all of the weights on that day from the reporting list below to cleanly move that first weight
                            -- back to the cohort start date
                            SELECT
                              s.transformuserid
                              , s.valuelbs
                              , s.timestamp
                            FROM (
                                   -- Get the global first weight for a participant
                                   SELECT DISTINCT ON (tu.id)
                                     tu.id AS transformuserid
                                     , W.valuelbs
                                     , W.timestamp :: DATE
                                   FROM
                                     bmh_prod_na.transformuser tu
                                     LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                                     LEFT JOIN bmh_prod_na.weight W ON W.transformuserid = tu.id
                                                                       AND W.deletedat ISNULL
                                   WHERE
                                     W.valuelbs NOTNULL AND W.deletedat ISNULL
                                   ORDER BY tu.id, timestamp ASC
                                 ) s
                              LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = transformuserid
                              LEFT JOIN bmh_prod_na.cohort ON cohort.id = tu.cohortid
                            -- And then only keep it if it happens to be in their first program week
                            WHERE
                              timestamp :: DATE BETWEEN cohort.startdate :: DATE
                              AND
                              cohort.startdate + INTERVAL '6 DAYS'

                          ) if_first_weight_in_first_period ON if_first_weight_in_first_period.transformuserid = tu.id
                LEFT JOIN bmh_prod_na.weight W ON W.transformuserid = tu.id
                                                  AND W.deletedat ISNULL
                                                  AND W.timestamp :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                                                  AND W.timestamp :: DATE > cohort.startdate :: DATE
                                                  AND W.timestamp :: DATE IS DISTINCT FROM
                                                      (if_first_weight_in_first_period.timestamp :: DATE)


              WHERE
                W.valuelbs NOTNULL AND W.deletedat ISNULL

            ) W
         LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = W.transformuserid
         LEFT JOIN bmh_prod_na.cohort C ON C.id = tu.cohortid
       WHERE
         W.timestamp :: TIMESTAMP >= C.startdate :: TIMESTAMP
       GROUP BY W.transformuserid, REPLACE(LEFT(W.timestamp, 10), '-', '')


       -- -----------------------------------------------------------------
       -- MEALS
       -- -----------------------------------------------------------------
       UNION
       SELECT
         transformuserid           AS "NW Blue Mesa"
         , to_char(date, 'YYYYMMDD') AS "Activity Date"
         , 0                        AS "Weight lbs"
         , 0                        AS "Physical Activity Minutes"
         , 0                        AS "Articles Read"
         , meals :: INTEGER             AS "Meals Logged"
         , 0                        AS "Group Interactions"
         , 0                        AS "Coach Interactions"
         , ''                        AS "Coach Interaction Type"
         , 0                        AS "Coach Interaction Duration"
         , 0                        AS "Quiz/Survey's Completed"
         , 0                        AS "Lessons/Education Modules Completed"

       FROM (
              SELECT
                tu.id          AS transformuserid
                , M.date :: DATE AS date
                , COUNT(*)       AS meals
              FROM bmh_prod_na.transformuser tu
                LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                LEFT JOIN (
                            SELECT
                              transformuserid
                              , date :: DATE AS date
                              , CATEGORY
                            FROM
                              bmh_prod_na.meal
                            GROUP BY
                              transformuserid, date :: DATE, CATEGORY
                          ) M ON M.transformuserid = tu.id

              WHERE
                1=1
                AND M.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                AND M.date :: DATE >= cohort.startdate :: DATE
              --                 AND tu.id in (8240)
              GROUP BY tu.id, M.date
            ) M


       -- -----------------------------------------------------------------
       -- GROUP INTERACTIONS
       -- -----------------------------------------------------------------
       UNION
       SELECT
         transformuserid                   AS "NW Blue Mesa"
         , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
         , 0                                AS "Weight lbs"
         , 0                                AS "Physical Activity Minutes"
         , 0                                AS "Articles Read"
         , 0                                AS "Meals Logged"
         , messages :: INTEGER                  AS "Group Interactions"
         , 0                                AS "Coach Interactions"
         , ''                                AS "Coach Interaction Type"
         , 0                                AS "Coach Interaction Duration"
         , 0                                AS "Quiz/Survey's Completed"
         , 0                                AS "Lessons/Education Modules Completed"
       FROM (
              SELECT
                M.transformuserid
                , LEFT(M.date, 10) AS date
                , COUNT(*)         AS messages
              FROM bmh_prod_na.message M
                LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = M.transformuserid
                LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                LEFT JOIN bmh_prod_na.conversation C ON C.id = M.conversationid
              WHERE
                C.distinct = FALSE
                AND M.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                AND M.date :: DATE >= cohort.startdate :: DATE
              --           and m.transformuserid in (6203)
              GROUP BY
                LEFT(M.date, 10),
                M.transformuserid
            ) conversations

       -- -----------------------------------------------------------------
       -- COACH INTERACTIONS
       -- -----------------------------------------------------------------
       UNION
       SELECT
         transformuserid                   AS "NW Blue Mesa"
         , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
         , 0                                AS "Weight lbs"
         , 0                                AS "Physical Activity Minutes"
         , 0                                AS "Articles Read"
         , 0                                AS "Meals Logged"
         , 0                                AS "Group Interactions"
         , COUNT :: INTEGER                     AS "Coach Interactions"
         , TYPE :: TEXT                      AS "Coach Interaction Type"
         , (COUNT * TIME) :: FLOAT            AS "Coach Interaction Duration"
         , 0                                AS "Quiz/Survey's Completed"
         , 0                                AS "Lessons/Education Modules Completed"
       FROM
         (
           SELECT
             C.transformuserid
             , C.date :: DATE
             , C.type      AS type_str
             , CASE WHEN C.type = 'PHONE'
             THEN 1
               WHEN C.type = 'SMS'
                 THEN 2
               WHEN C.type = 'CHAT'
                 THEN 3
               WHEN C.type = 'EMAIL'
                 THEN 6
               END         AS TYPE
             , COUNT(*)    AS COUNT
             , CASE WHEN C.type = 'PHONE'
             THEN 30
               WHEN C.type = 'SMS'
                 THEN 7
               WHEN C.type = 'CHAT'
                 THEN 7
               WHEN C.type = 'EMAIL'
                 THEN 15
               END         AS TIME
           FROM
             bmh_prod_na.contact C
             LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = C.transformuserid
             LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
           WHERE
             C.response = TRUE AND C.type != 'CHAT'
             AND C.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
             AND C.date :: DATE >= cohort.startdate :: DATE
           --         and c.transformuserid in (6203)
           GROUP BY C.transformuserid, C.date :: DATE, C.type
         ) interactions


       UNION
       SELECT
         transformuserid                   AS "NW Blue Mesa"
         , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
         , 0                                AS "Weight lbs"
         , 0                                AS "Physical Activity Minutes"
         , 0                                AS "Articles Read"
         , 0                                AS "Meals Logged"
         , 0                                AS "Group Interactions"
         , messages :: INTEGER                  AS "Coach Interactions"
         , '3'                               AS "Coach Interaction Type"
         , (messages * 7) :: FLOAT            AS "Coach Interaction Duration"
         , 0                                AS "Quiz/Survey's Completed"
         , 0                                AS "Lessons/Education Modules Completed"
       FROM (
              SELECT
                M.transformuserid
                , LEFT(M.date, 10) AS date
                , COUNT(*)         AS messages
              FROM bmh_prod_na.message M
                LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = M.transformuserid
                LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                LEFT JOIN bmh_prod_na.conversation C ON C.id = M.conversationid
              WHERE
                C.distinct = TRUE
                AND M.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                AND M.date :: DATE >= cohort.startdate :: DATE
                AND C.iswithcoach = TRUE
              --             and m.transformuserid in (6203)
              GROUP BY
                LEFT(M.date, 10),
                M.transformuserid
            ) conversations

       UNION
       SELECT
         transformuserid                   AS "NW Blue Mesa"
         , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
         , 0                                AS "Weight lbs"
         , 0                                AS "Physical Activity Minutes"
         , 0                                AS "Articles Read"
         , 0                                AS "Meals Logged"
         , 0                                AS "Group Interactions"
         , 1 :: INTEGER                         AS "Coach Interactions"
         , 1 :: TEXT                         AS "Coach Interaction Type"
         , 10 :: FLOAT                       AS "Coach Interaction Duration"
         , 0                                AS "Quiz/Survey's Completed"
         , 0                                AS "Lessons/Education Modules Completed"
       FROM
         (
           SELECT
               tu.id                    AS transformuserid
             , cohort.startdate :: DATE AS date
           FROM
             bmh_prod_na.transformuser tu
             LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
           WHERE
             cohort.startdate :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
         ) firstday



       -- -----------------------------------------------------------------
       -- LESSONS
       -- -----------------------------------------------------------------

       UNION
       SELECT
         transformuserid                   AS "NW Blue Mesa"
         , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
         , 0                                AS "Weight lbs"
         , 0                                AS "Physical Activity Minutes"
         , 0                                AS "Articles Read"
         , 0                                AS "Meals Logged"
         , 0                                AS "Group Interactions"
         , 0                                AS "Coach Interactions"
         , ''                                AS "Coach Interaction Type"
         , 0                                AS "Coach Interaction Duration"
         , concat(quiz :: TEXT, '')::INTEGER          AS "Quiz/Survey's Completed"
         , 0                                AS "Lessons/Education Modules Completed"
       FROM (
              SELECT
                tu.id                                          AS transformuserid
                , to_char(quizdoneat :: TIMESTAMP, 'YYYY-MM-DD') AS date
                , COUNT(*)                                       AS quiz
              FROM
                bmh_prod_na.transformuser tu
                LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                LEFT JOIN bmh_prod_na.lessonprogress lp ON lp.transformuserid = tu.id
              WHERE
                1=1
                AND lp.quizdoneat :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                AND lp.quizdoneat :: DATE >= cohort.startdate :: DATE
              GROUP BY tu.id, to_char(quizdoneat :: TIMESTAMP, 'YYYY-MM-DD')
            ) lp
       WHERE
         quiz NOTNULL

       UNION
       SELECT
         transformuserid                   AS "NW Blue Mesa"
         , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
         , 0                                AS "Weight lbs"
         , 0                                AS "Physical Activity Minutes"
         , 0                                AS "Articles Read"
         , 0                                AS "Meals Logged"
         , 0                                AS "Group Interactions"
         , 0                                AS "Coach Interactions"
         , ''                                AS "Coach Interaction Type"
         , 0                                AS "Coach Interaction Duration"
         , 0                                AS "Quiz/Survey's Completed"
         , concat(CONTENT :: TEXT, '')::INTEGER       AS "Lessons/Education Modules Completed"
       FROM (
              SELECT

                tu.id                                             AS transformuserid
                , to_char(contentdoneat :: TIMESTAMP, 'YYYY-MM-DD') AS date
                , COUNT(*)                                          AS CONTENT
              FROM
                bmh_prod_na.transformuser tu
                LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                LEFT JOIN bmh_prod_na.lessonprogress lp ON lp.transformuserid = tu.id
              WHERE
                1=1
                AND lp.contentdoneat :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                AND lp.contentdoneat :: DATE >= cohort.startdate :: DATE
              GROUP BY tu.id, to_char(contentdoneat :: TIMESTAMP, 'YYYY-MM-DD')
            ) lp
       WHERE
         CONTENT NOTNULL

       -- -----------------------------------------------------------------
       -- PHYSICAL ACTIVITY
       -- -----------------------------------------------------------------

       UNION
       SELECT
         transformuserid                   AS "NW Blue Mesa"
         , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
         , 0                                AS "Weight lbs"
         , CASE WHEN MINUTES = 0
         THEN NULL
           ELSE MINUTES :: INTEGER END          AS "Physical Activity Minutes"
         , 0                                AS "Articles Read"
         , 0                                AS "Meals Logged"
         , 0                                AS "Group Interactions"
         , 0                                AS "Coach Interactions"
         , ''                                AS "Coach Interaction Type"
         , 0                                AS "Coach Interaction Duration"
         , 0                                AS "Quiz/Survey's Completed"
         , 0                                AS "Lessons/Education Modules Completed"
       FROM (
              SELECT
                tu.id             AS transformuserid
                , fad.date          AS date
                , fad.activeminutes AS MINUTES
              FROM
                bmh_prod_na.transformuser tu
                LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                LEFT JOIN bmh_prod_na.activityday fad ON fad.transformuserid = tu.id
              WHERE
                1=1
                AND fad.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                AND fad.date :: DATE >= cohort.startdate :: DATE
                AND (fad.activeminutes) > 0
            ) am
       WHERE
         am.minutes NOTNULL
     ),
engagement_agg AS (
SELECT  "NW Blue Mesa"                      AS  transformuserid
        ,"Activity Date"                    AS  date
        ,SUM("Weight lbs"::FLOAT)                  AS  weight
        ,SUM("Physical Activity Minutes"::FLOAT)   AS  physical_activity_minutes
        ,SUM("Meals Logged"::INTEGER)                AS  meals_logged
        ,SUM("Coach Interactions"::INTEGER)          AS  num_of_coach_interactions
        --,STRING_AGG("Coach Interaction Type", ',')      AS  coach_interaction_type
        ,SUM("Coach Interaction Duration"::FLOAT)  AS  coach_interaction_duration
        ,SUM("Quiz/Survey's Completed"::INTEGER)     AS  num_of_quizzes_or_surveys_completed
        ,SUM("Lessons/Education Modules Completed"::INTEGER) AS num_of_lessons_completed
FROM all_engagement
GROUP BY all_engagement."NW Blue Mesa", all_engagement."Activity Date"
ORDER BY all_engagement."NW Blue Mesa", all_engagement."Activity Date"
         ), restarts AS (
    SELECT DISTINCT ON(tu1.uuid)
            tu1.uuid AS uuid
    FROM    bmh_prod_na.transformuser tu1
    INNER JOIN bmh_prod_na.transformuser tu2
    ON tu1.shortname = tu2.shortname
    WHERE   1=1
            AND tu1.id != tu2.id
            AND tu1.birthdate = tu2.birthdate
            AND tu1.cohortid NOT IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 16, 19, 222, 223, 353, 354, 355, 500, 518, 632, 646, 657, 907, 918, 1079, 1132, 9988)
            AND tu2.cohortid NOT IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 16, 19, 222, 223, 353, 354, 355, 500, 518, 632, 646, 657, 907, 918, 1079, 1132, 9988)
            AND tu1.uuid NOTNULL
            AND tu2.uuid NOTNULL
)
SELECT
      engagement_agg.*
      ,transformuser.uuid
--      ,mvdpse.expanded_statusid
FROM  engagement_agg
INNER JOIN bmh_prod.transformuser
ON engagement_agg.transformuserid = transformuser.id
INNER JOIN scratch_marcuscampbell.mat_view_dt_participant_status_expanded mvdpse
ON engagement_agg.transformuserid = mvdpse.transformuserid AND engagement_agg.date::DATE = mvdpse.expanded_date
WHERE   1=1
        -- Exclude demo cohorts
        AND transformuser.cohortid NOT IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 16, 19, 222, 223, 353, 354, 355, 500, 518, 632, 646, 657, 907, 918, 1079, 1132, 9988)
        -- Exclude ppts that went through program twice
        AND uuid NOT IN ('07855f94-fb18-11e7-a046-a371beb89a63','0d55934e-e12d-11e7-8895-2339cc3b4103','56244172-819d-11e8-85f7-63b6bf00dc5b',
                        '6d3418ea-0f35-11e8-a6c1-63afaa87c6d7','926b648a-1c21-11e8-9423-9b788899a7d7','b25b7706-e000-11e7-adae-2789a50c93d7',
                        'bb3ee966-cbba-11e7-8ba1-2f7db26383ea','e3334e64-c492-11e7-a9ed-7bb30df5b0bf')
        -- Exclude ppts that started program and then deferred
        AND uuid NOT IN (SELECT * FROM restarts)
        -- Exclude ppts that dropped
        AND mvdpse.expanded_statusid NOT IN (5,6,8)
        -- Only include english speaking participants
        AND transformuser.language LIKE '%en%'
ORDER BY transformuserid, date ASC
;