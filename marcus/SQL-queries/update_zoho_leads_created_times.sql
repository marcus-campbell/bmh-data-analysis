UPDATE  zoho.leads zl
SET "Created Time" = TO_CHAR(createdat, 'YYYY-MM-DD HH24:MI:SS')
WHERE   1=1
        AND zl."Created Time" ISNULL
        AND zl.createdat > '2018-12-11'::DATE