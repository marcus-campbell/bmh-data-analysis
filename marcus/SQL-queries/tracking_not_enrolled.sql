SELECT  mbc.soleraid
        ,MIN(mbc.transformuserid) AS transformuserid
        ,STRING_AGG(enrolled::text, ''  order by er.filename) AS enrol_sequence
        ,STRING_AGG(DISTINCT milestonetype, ',')
        ,COUNT(DISTINCT mbc.milestonetype)
FROM
     solera.enrollmentresponse er
LEFT JOIN
     (SELECT DISTINCT ON (soleraid, milestonetype)
             soleraid
             ,transformuserid
             ,milestonetype
             ,milestonedate
             ,id
      FROM  solera.milestones_bmh_computed) mbc
LEFT JOIN
      solera.milestone_status ms
ON  mbc.id = ms.bmhmilestoneid
ON  er.solera_id = mbc.soleraid
WHERE mbc.milestonedate >= '2018-01-01'::DATE AND
      ms.statustypeid != 1
GROUP BY mbc.soleraid
ORDER BY enrol_sequence
;
