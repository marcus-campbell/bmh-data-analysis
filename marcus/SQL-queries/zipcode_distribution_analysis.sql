
WITH ppt_zips AS (
  SELECT DISTINCT ON (tu.id)
        tu.id            AS transformuserid
        ,all_zips.zipcode
        ,cohort.startdate::DATE AS startdate
        ,all_zips.city
  FROM (SELECT lead.uuid                         AS uuid
              ,SUBSTRING(lead.zipcode, '^\d{5}') AS zipcode
              ,INITCAP(lead.city)                AS city
        FROM bmh_prod_na.lead
        UNION
        SELECT contacts."UUID"                             AS uuid
              ,SUBSTRING(contacts."Mailing Zip", '^\d{5}') AS zipcode
              ,INITCAP(contacts."Mailing City")            AS city
        FROM zoho.contacts
        UNION
        SELECT leads."UUID"                          AS uuid
              ,SUBSTRING(leads."Zip Code", '^\d{5}') AS zipcode
              ,INITCAP(leads."City")                   AS city
        FROM zoho.leads) all_zips
         INNER JOIN bmh_prod_na.transformuser tu -- Inner join because we only want actual transform PPTs
                   ON all_zips.uuid = tu.uuid
         LEFT JOIN bmh_prod_na.cohort
                    ON tu.cohortid = cohort.id
  WHERE all_zips.uuid NOTNULL
    AND all_zips.zipcode NOTNULL
    AND cohort.id NOT IN (7, 8, 9, 10, 15, 16, 19, 222, 223, 353, 354, 355, 500, 518, 632, 646, 657, 9988)
)
SELECT ppt_zips.transformuserid
        ,ppt_zips.zipcode
        ,ppt_zips.startdate
        ,ppt_zips.city
        ,z5.county
        ,z5.state
FROM  ppt_zips
INNER JOIN (SELECT DISTINCT z5plus.zip -- Inner join used to exclude incorrectly entered ZIP codes
                           ,z5plus.county
                           ,INITCAP(scln.state_long_name) AS state
           FROM zoho.z5plus
           LEFT JOIN zoho.state_codes_to_long_names scln
           ON z5plus.state = scln.state_code) z5
ON ppt_zips.zipcode = z5.zip
ORDER BY state
;
