-- Calculate Medi-Cal M7 survivorship / procurement rate

WITH nine_percent_loss AS (

SELECT DISTINCT ON(weight.transformuserid)
        weight.valuelbs
        ,weight.transformuserid
        ,weight.date::DATE - cohort.startdate::DATE + 1 AS program_day
        ,TRUNC(DATE_PART('day', weight.date::TIMESTAMP - cohort.startdate::TIMESTAMP) / 7) + 1  AS program_week
FROM bmh_prod_na.weight
LEFT JOIN   bmh_prod_na.view_weight_bounds
ON weight.transformuserid = view_weight_bounds.transformuserid
LEFT JOIN   bmh_prod_na.transformuser
ON weight.transformuserid = transformuser.id
LEFT JOIN   bmh_prod_na.cohort
ON transformuser.cohortid = cohort.id
WHERE   1=1
        AND transformuser.cohortid NOT IN (7,8,9,10,15,16,19,222,223,353,354,355,500,518,632,646,657,9988)
        AND weight.deletedat ISNULL
        AND weight.valuelbs <= 0.91 * view_weight_bounds.start_weight
ORDER BY weight.transformuserid, weight.date ASC),
     all_ppts AS (
         SELECT id
         FROM   bmh_prod_na.transformuser
         WHERE  transformuser.cohortid NOT IN (7,8,9,10,15,16,19,222,223,353,354,355,500,518,632,646,657,9988)
     ),
        surv    AS (
SELECT program_week,
       COUNT(*)
FROM    nine_percent_loss
WHERE program_week >= 1
      AND program_week <= 104
GROUP BY   program_week
ORDER BY   program_week
    )
SELECT
surv.program_week
,surv.count / (SELECT COUNT(*)::NUMERIC FROM all_ppts)   AS m7_survivorship
FROM surv
;