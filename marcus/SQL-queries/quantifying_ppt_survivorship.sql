-- survivorship by program day

WITH all_ppts AS (
    SELECT transformuserid
           ,(program_day + 1) AS program_day
           ,((program_day + 1)::INTEGER / 7) + 1 AS program_week
    FROM scratch_marcuscampbell.dt_participant_status_expanded
    WHERE 1 = 1
      AND expanded_statusid_granular IN (30,31,32)
      AND program_day < 365
      AND cohort_startdate < '2018-04-01'
    ORDER BY transformuserid, program_day
), count_ppts AS (
    SELECT program_day
           ,COUNT(transformuserid)::NUMERIC AS ppts
    FROM    all_ppts
    GROUP BY program_day
)
SELECT program_day
       ,ppts / (SELECT MAX(ppts) FROM count_ppts) AS ppt_survivorship
FROM    count_ppts
ORDER BY program_day
;

-- survivorship by program week
REFRESH MATERIALIZED VIEW scratch_marcuscampbell.mat_view_dt_participant_status_expanded;

WITH all_ppts AS (
    SELECT transformuserid
           ,(program_day + 1) AS program_day
           ,((program_day + 1)::INTEGER / 7) AS program_week
    FROM scratch_marcuscampbell.dt_participant_status_expanded
    WHERE 1 = 1
      AND expanded_statusid_granular IN (30,31,32)
      AND program_day < 365
      AND cohort_startdate < '2020-01-01'
    ORDER BY transformuserid, program_day
), count_ppts AS (
    SELECT program_week
           ,COUNT(transformuserid)::NUMERIC AS ppts
    FROM    all_ppts
    GROUP BY program_week
)
SELECT program_week
       ,ppts / (SELECT MAX(ppts) FROM count_ppts) AS ppt_survivorship
FROM    count_ppts
WHERE   1=1
        AND program_week != 53
        AND program_week != 0
ORDER BY program_week
;

-- same calculation as above but include UUIDs for JMIR study


WITH all_ppts AS (
    SELECT transformuserid
           ,(program_day + 1) AS program_day
           ,((program_day + 1)::INTEGER / 7) AS program_week
    FROM (SELECT  dt.*
        ,tu.uuid
FROM scratch_marcuscampbell.dt_participant_status_expanded dt
LEFT JOIN bmh_prod_na.transformuser tu
ON dt.transformuserid = tu.id
INNER JOIN scratch_marcuscampbell.jmir_study_uuids jsu
ON tu.uuid = jsu.c1
) uuu
    WHERE 1 = 1
      AND expanded_statusid_granular IN (30,31,32)
      AND program_day < 365
      AND cohort_startdate < '2018-04-01'
    ORDER BY transformuserid, program_day
), count_ppts AS (
    SELECT program_week
           ,COUNT(transformuserid)::NUMERIC AS ppts
    FROM    all_ppts
    GROUP BY program_week
)
SELECT program_week
       ,ppts / (SELECT MAX(ppts) FROM count_ppts) AS ppt_survivorship
FROM    count_ppts
WHERE   1=1
        AND program_week != 53
        AND program_week != 0
ORDER BY program_week
;


SELECT  dt.*
        ,tu.uuid
FROM scratch_marcuscampbell.dt_participant_status_expanded dt
LEFT JOIN bmh_prod_na.transformuser tu
ON dt.transformuserid = tu.id
INNER JOIN scratch_marcuscampbell.jmir_study_uuids jsu
ON tu.uuid = jsu.c1
;