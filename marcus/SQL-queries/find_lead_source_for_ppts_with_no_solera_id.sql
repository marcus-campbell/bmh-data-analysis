-- Find users in view_participants_milestones with no solera ID who appear to belong to a Solera cohort,
-- and link leadsource information.

SELECT tu.id  AS transformuserid
      ,tu.uuid  AS uuid
      ,lead.leadsource
      ,lead.committeddate
FROM
  (SELECT id
          ,uuid
  FROM  bmh_prod_na.transformuser
  WHERE id IN (5,75,1009,5052,6143,6514,6667,6880,7212,7262,7941,8322,9071,10872,11703,12933,13067,13426,13649,14068,14071,14522,14610)) tu
LEFT JOIN
  (SELECT uuid
          ,leadsource
          ,committeddate
    FROM  bmh_prod_na.lead) lead
ON tu.uuid = lead.uuid;