-- Find total number of transform ppts

SELECT  COUNT(*)
FROM    bmh_prod_na.transformuser
WHERE   1=1
        AND cohortid NOT IN (1,2,3,4,5,6,7,8,9,10,15,16,19,222,223,353,354,355,500,518,632,646,657,9988) -- Exclude demo cohorts
        AND cohortid NOT IN (297, 489) -- Exclude CRC cohorts
;

-- Leads vs enrolled:

SELECT COUNT(*)
FROM    looker_scratch.lr$pfzaaah8y5ymi7cjbqabh_zoho_combined_enrollment
WHERE   1=1
        AND ind_type = 'Lead'
        AND "Lead Source" NOT IN ('MerckBRA', 'MerckGUA')
;

SELECT "Lead Source",
       COUNT(*)
FROM    looker_scratch.lr$pfzaaah8y5ymi7cjbqabh_zoho_combined_enrollment
GROUP BY "Lead Source"
;

-- Primary language

SELECT  language
        ,COUNT(*)
FROM    bmh_prod_na.transformuser
WHERE   cohortid NOT IN (15, 16, 19, 222, 223, 353, 354, 355, 500, 518, 632, 646, 657, 9988)
GROUP BY language
;

-- Find min, max, and avg age of transform ppts
SELECT  MIN(reportingage)
        ,MAX(reportingage)
        ,AVG(reportingage)
FROM    scratch_marcuscampbell.marcus_cdc_age
;

-- Biological sex data
SELECT  biologicalsex
        ,(COUNT(*)::NUMERIC / (SELECT COUNT(*)::NUMERIC FROM scratch_marcuscampbell.marcus_cdc_sex)) * 100
FROM    scratch_marcuscampbell.marcus_cdc_sex
GROUP BY biologicalsex
;

-- City data
SELECT  COUNT(DISTINCT city)
FROM    looker_scratch.lr$pfu481586823281240_zoho_combined_enrollment
WHERE   ind_type = 'Enrolled'
;

-- Education

SELECT  edu
        ,(COUNT(edu)::NUMERIC/ (SELECT COUNT(*)::NUMERIC FROM scratch_marcuscampbell.marcus_cdc_education)) * 100
FROM    scratch_marcuscampbell.marcus_cdc_education
GROUP BY edu
;

-- Ethnicity

SELECT  (COUNT(*) / (SELECT COUNT(*) FROM scratch_marcuscampbell.marcus_cdc_ethnographic)::NUMERIC) * 100
FROM    scratch_marcuscampbell.marcus_cdc_ethnographic
WHERE   nhopi= 1
;