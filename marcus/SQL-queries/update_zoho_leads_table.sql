DROP TABLE IF EXISTS    scratch_marcuscampbell.dummy_table
;

CREATE TABLE scratch_marcuscampbell.dummy_table AS
    SELECT *
    FROM    zoho.leads
;

SELECT  *
FROM    scratch_marcuscampbell.dummy_table dt
WHERE   dt."Created Time" ISNULL
ORDER BY createdat DESC
;

SELECT  dt."Created Time"
        ,dt.createdat
FROM    scratch_marcuscampbell.dummy_table dt
WHERE   dt."Created Time" NOTNULL
;

SELECT  dt."Created Time"
        ,dt.createdat
        ,TO_CHAR(dt.createdat, 'YYYY-MM-DD HH24:MI:SS') AS newcreatedat
FROM    scratch_marcuscampbell.dummy_table dt
WHERE   1=1
        AND dt."Created Time" NULL
        AND dt.createdat > '2018-12-11'::DATE
;

UPDATE  --zoho.leads dt
SET "Created Time" = ls."Created Time"
FROM scratch_marcuscampbell.leads_001 ls
WHERE   1=1
        AND dt."Lead ID" = ls."Lead ID"
        AND dt."Created Time" ISNULL
;