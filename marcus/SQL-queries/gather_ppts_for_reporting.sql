SELECT DISTINCT ON(transformuserid)
        dt.*
        ,wb.start_weight
FROM    scratch_marcuscampbell.dt_participant_status_expanded dt
INNER JOIN bmh_prod.view_weight_bounds wb
ON dt.transformuserid = wb.transformuserid
WHERE program_day < 186
ORDER BY transformuserid, program_day DESC
;


WITH    weight_by_6_months AS (
SELECT DISTINCT ON(transformuserid)
        dt.*
        ,w.valuelbs
FROM    scratch_marcuscampbell.dt_participant_status_expanded dt
INNER JOIN  bmh_prod_na.weight w
ON dt.transformuserid = w.transformuserid AND dt.expanded_date = w.date::DATE
WHERE 1=1
      AND program_day > 120
      AND program_day < 186
      AND w.deletedat ISNULL
ORDER BY transformuserid, program_day DESC),
        first_weight AS (
            SELECT sixmons.*
                   ,wb.start_weight
            FROM    weight_by_6_months sixmons
            INNER JOIN bmh_prod_na.view_weight_bounds wb
            ON sixmons.transformuserid = wb.transformuserid
        ),
     lost_weight AS (
         SELECT first_weight.transformuserid
                ,((start_weight - valuelbs) /  start_weight) * 100   AS weight_loss
         FROM   first_weight
     )
SELECT *
INTO    scratch_marcuscampbell.lost_weight3
FROM   first_weight
;

WITH ls AS(
SELECT lost_weight2.transformuserid
       ,((start_weight - valuelbs) /  start_weight) * 100   AS weight_loss
       ,CASE
            WHEN ((start_weight - valuelbs) /  start_weight) > 0.05 THEN 1
            ELSE 0
            END AS m4_met
FROM scratch_marcuscampbell.lost_weight2)
SELECT AVG(weight_loss)
FROM ls
;


WITH activity_weeks AS (SELECT DISTINCT
  dt_criteria_weekly.week_series  AS "week_series",
  dt_criteria_weekly.transformuserid as "transformuserid",
  sum(activityday.activeminutes)
 OVER (
        PARTITION BY dt_criteria_weekly.transformuserid, dt_criteria_weekly.week_series )
                AS "active_minutes"
FROM scratch_marcuscampbell.dt_criteria_weekly as dt_criteria_weekly
LEFT JOIN bmh_prod.activityday  AS activityday
ON activityday.transformuserid = dt_criteria_weekly.transformuserid
    AND dt_criteria_weekly.week_start <= activityday.date::date
    AND dt_criteria_weekly.week_end >= activityday.date::date
WHERE (dt_criteria_weekly.week_series  > 0))
SELECT *
INTO scratch_marcuscampbell.activity_weeks
FROM    activity_weeks
ORDER BY transformuserid, week_series
;

SELECT transformuserid
        ,AVG(COALESCE(active_minutes,0)) AS avg_minutes
FROM     scratch_marcuscampbell.activity_weeks
WHERE   1=1
        AND week_series <= 26
GROUP BY transformuserid
ORDER BY avg_minutes DESC
;

SELECT *
FROM scratch_marcuscampbell.activity_weeks
WHERE transformuserid = 14707
;

WITH avg_stuff AS (
    SELECT transformuserid
        ,AVG(COALESCE(active_minutes,0)) AS avg_minutes
FROM     scratch_marcuscampbell.activity_weeks
WHERE   1=1
        AND week_series IN (25,26)
GROUP BY transformuserid
ORDER BY avg_minutes DESC
)
SELECT COUNT(*)::FLOAT / (SELECT COUNT(*) FROM avg_stuff)::FLOAT
FROM avg_stuff
WHERE avg_stuff.avg_minutes >= 150
;

WITH avg_stuff AS (
    SELECT transformuserid
        ,AVG(COALESCE(active_minutes,0)) AS avg_minutes
FROM     scratch_marcuscampbell.activity_weeks
WHERE   1=1
        AND week_series <= 26
        --AND active_minutes NOTNULL
        --AND active_minutes != 0
GROUP BY transformuserid
HAVING MAX(week_series) >= 26
ORDER BY avg_minutes DESC
)
SELECT  AVG(avg_minutes)
FROM avg_stuff
;

--Average weight loss at 6 months of anyone who ever enrolled = 3.3%

--Average weight loss of ppts who are still enrolled at month 6 = 5.14%

--Percentage of PPTs who ever enrolled to reach 5% weight loss by month 6 = 30%

--Percentage of PPTs who are still enrolled at month 6 to reach 5% weight loss = 46%

--Percentage of PPTs who ever enrolled to average at least 150 minutes of physical activity a week: 12.7%

--Percentage of PPTs who are still enrolled at month 6 to average 150 minutes of physical activity a week: 7.6%

--Percentage of PPTs who are still enrolled at month 6 to average 150 minutes of weekly physical activity in their reported sessions.