CREATE TABLE scratch_marcuscampbell.dt_criteria_weekly AS
SELECT
    sm.soleraid,
    tu.id                                                                                   AS transformuserid,
    tu.fullname,
    tu.cohortid,
    week_series,
    (c.startdate + ((week_series - 1) || ' weeks') :: INTERVAL) :: DATE                     AS week_start,
    (c.startdate + ((week_series - 1) || ' weeks') :: INTERVAL + INTERVAL '6 days') :: DATE AS week_end,

    -- (A) Completing and submitting the quiz associated with each weekly lesson
    coalesce(quiz_weekly.count, 0)                                                          AS quiz_done,
    CASE WHEN coalesce(quiz_weekly.count, 0) >= 1
      THEN 1
    ELSE 0 END                                                                              AS quiz_criteria,
    quiz_weekly.dates                                                                       AS quiz_criteria_dates,
    CASE WHEN coalesce(quiz_weekly.count, 0) >= 1
      THEN quiz_weekly.dates[1]
    ELSE NULL END                                                                           AS quiz_criteria_date,

    -- (B) Engaging with the Transform Community Group social network at least once
    coalesce(group_chat_weekly.count, 0)                                                    AS group_chat_count,
    CASE WHEN coalesce(group_chat_weekly.count, 0) >= 1
      THEN 1
    ELSE 0 END                                                                              AS group_criteria,
    group_chat_weekly.dates                                                                 AS group_criteria_dates,
    CASE WHEN coalesce(group_chat_weekly.count, 0) >= 1
      THEN group_chat_weekly.dates[1]
    ELSE NULL END                                                                           AS group_criteria_date,

    -- (C) Having EITHER an individual call, participation in a group call, or an exchange of messages with the Health Coach
    coalesce(contact_weekly.count, 0)                                                       AS contact_count,
    CASE WHEN coalesce(contact_weekly.count) >= 1
      THEN 1
    ELSE 0 END                                                                              AS contact_criteria,
    contact_weekly.dates                                                                    AS contact_dates,
    CASE WHEN coalesce(contact_weekly.count) >= 1
      THEN contact_weekly.dates[1]
    ELSE NULL END                                                                           AS contact_criteria_date,

    -- (D) Tracking three or more meals.
    coalesce(meals_weekly.count, 0)                                                         AS meals_count,
    CASE WHEN coalesce(meals_weekly.count, 0) >= 3
      THEN 1
    ELSE 0 END                                                                              AS meal_criteria,
    meals_weekly.dates                                                                      AS meals_dates,
    CASE WHEN coalesce(meals_weekly.count, 0) >= 3
      THEN meals_weekly.dates[3]
    ELSE NULL END                                                                           AS meal_criteria_date,

    -- (E) Weigh in at least 1 times in one week (SOW 5 Dec 2017)
    coalesce(weights_weekly.count, 0)                                                       AS weight_count,
    CASE WHEN coalesce(weights_weekly.count, 0) >= 1
      THEN 1
    ELSE 0 END                                                                              AS weighin_criteria,
    weights_weekly.dates                                                                    AS weight_dates,
    CASE WHEN coalesce(weights_weekly.count, 0) >= 1
      THEN weights_weekly.dates[1]
    ELSE NULL END                                                                           AS weighin_criteria_date


  FROM
        generate_series(0, 52) AS week_series
    CROSS JOIN bmh_prod_na.transformuser tu

    LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid

    LEFT JOIN bmh_prod_na.soleramap sm ON sm.transformuserid = tu.id

    LEFT JOIN
    (
      SELECT
        weight_bucket.transformuserid                      AS transformuserid,
        weight_bucket.week_bucket,
        count(*)
          FILTER (WHERE weight_bucket.week_bucket NOTNULL) AS count,
        array_agg(weightdate ORDER BY weightdate)          AS dates
      FROM

        (
          SELECT
            *,
            (startdate + (weight_since.days - 1 || ' DAYS') :: INTERVAL) :: DATE AS weightdate,
            width_bucket(weight_since.days, 1, 364, 52)                          AS week_bucket
          FROM
            (
              SELECT
                weight.transformuserid,
                weight.timestamp :: DATE - c.startdate :: DATE + 1 AS days,
                c.startdate :: TIMESTAMP
              --                   ,weight.timestamp::date
              FROM bmh_prod_na.weight
                LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = weight.transformuserid
                LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid
              WHERE weight.deletedat ISNULL AND weight.transformuserid NOTNULL
              GROUP BY transformuserid, days, startdate--, timestamp
            ) weight_since
        ) weight_bucket
      -- where  tu.id = 6000
      GROUP BY weight_bucket.transformuserid, weight_bucket.week_bucket
    ) weights_weekly
      ON weights_weekly.transformuserid = tu.id AND weights_weekly.week_bucket = week_series.week_series

    LEFT JOIN (
                SELECT
                  contact_bucket.transformuserid,
                  contact_bucket.week_bucket,
                  count(*),
                  array_agg(contactdate ORDER BY contactdate) AS dates
                FROM
                  (
                    SELECT
                      *,
                      width_bucket(contacts.days, 1, 364, 52) AS week_bucket
                    FROM (
                           -- coach specified contacts, excluding 'CHAT' type
                           SELECT
                             contact.transformuserid,
                             contact.date :: DATE                           AS contactdate,
                             c.startdate :: TIMESTAMP,
                             contact.date :: TIMESTAMP - c.startdate :: TIMESTAMP,
                             contact.date :: DATE - c.startdate :: DATE + 1 AS days
                           FROM
                             bmh_prod_na.contact
                             LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = contact.transformuserid
                             LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid
                           WHERE contact.response = TRUE AND contact.type != 'CHAT'

                         UNION
                           -- artificial day #1 check-in with coach
                           SELECT
                             tu.id,
                             c.startdate :: DATE AS contactdate,
                             c.startdate :: TIMESTAMP,
                             c.startdate :: TIMESTAMP - c.startdate :: TIMESTAMP,
                             1 AS days
                           FROM
                             bmh_prod_na.transformuser tu
                             LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid

                         UNION
                           -- 'CHAT' type contact with coach, extracted from (conversation) messages
                           SELECT
                             m.transformuserid,
                             m.date :: DATE                           AS contactdate,
                             c.startdate :: TIMESTAMP,
                             m.date :: TIMESTAMP - c.startdate :: TIMESTAMP,
                             m.date :: DATE - c.startdate :: DATE + 1 AS days
                           FROM
                             bmh_prod_na.message m
                             LEFT JOIN bmh_prod_na.conversation conv ON conv.id = m.conversationid
                             LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = m.transformuserid
                             LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid
                           WHERE conv."distinct" = TRUE AND conv.iswithcoach = TRUE

                         ) contacts
                  ) contact_bucket
                GROUP BY contact_bucket.transformuserid, contact_bucket.week_bucket
              ) contact_weekly ON contact_weekly.transformuserid = tu.id AND contact_weekly.week_bucket = week_series

    LEFT JOIN
    (
      SELECT
        group_chat_bucket.transformuserid,
        group_chat_bucket.week_bucket,
        count(*) AS count,
        array_agg(chatdate ORDER BY chatdate) AS dates
      FROM (
             SELECT
               *,
               width_bucket(group_chat_days.days, 1, 364, 52) AS week_bucket
             FROM
               (
                 SELECT
                   message.transformuserid,
                   message.date :: DATE                           AS chatdate,
                   c.startdate :: TIMESTAMP,
                   message.date :: TIMESTAMP - c.startdate :: TIMESTAMP,
                   message.date :: DATE - c.startdate :: DATE + 1 AS days
                 FROM
                   bmh_prod_na.message
                   LEFT JOIN bmh_prod_na.conversation ON conversation.id = message.conversationid
                   LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = message.transformuserid
                   LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid
                 WHERE
                   conversation."distinct" = FALSE
               ) group_chat_days
           ) group_chat_bucket
      GROUP BY group_chat_bucket.transformuserid, group_chat_bucket.week_bucket
    ) group_chat_weekly ON group_chat_weekly.transformuserid = tu.id AND group_chat_weekly.week_bucket = week_series


    LEFT JOIN

    (
      SELECT
        quiz_bucket.transformuserid,
        quiz_bucket.week_bucket,
        count(*) AS count,
        array_agg(quizdate ORDER BY quizdate) AS dates
      FROM (
             SELECT
               *,
               width_bucket(quiz_days.days, 1, 364, 52) AS week_bucket
             FROM (
                    SELECT
                      lessonprogress.*,
                      C.startdate,
                      lessonprogress.date :: DATE                           AS quizdate,
                      lessonprogress.date :: DATE - C.startdate :: DATE + 1 AS DAYS,
                      1                                                     AS count
                    FROM (
                           SELECT
                             transformuserid,
                             MIN(quizdoneat) :: TIMESTAMP AS date,
                             weeknum,
                             quizstatus
                           FROM bmh_prod_na.lessonprogress
                           GROUP BY transformuserid, weeknum, quizstatus
                         ) lessonprogress
                      LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = lessonprogress.transformuserid
                      LEFT JOIN bmh_prod_na.cohort C ON C.id = tu.cohortid
                    WHERE lessonprogress.date NOTNULL
                  ) quiz_days
           ) quiz_bucket
      GROUP BY transformuserid, week_bucket
    ) quiz_weekly ON quiz_weekly.transformuserid = tu.id AND quiz_weekly.week_bucket = week_series

    LEFT JOIN
    (
      SELECT
        transformuserid,
        width_bucket(meal_bucket.days, 1, 364, 52) AS week_bucket,
        count(*)                                   AS count,
        array_agg(mealdate ORDER BY mealdate)      AS dates
      FROM (
             SELECT
               meal_date.transformuserid,
               meal_date.date :: DATE                           AS mealdate,
               c.startdate :: DATE,
               meal_date.date :: DATE - c.startdate :: DATE + 1 AS days
             FROM (
                    SELECT
                      transformuserid,
                      date,
                      category
                    FROM
                      (
                        SELECT
                          transformuserid,
                          left(date, 10) AS date,
                          category
                        FROM bmh_prod_na.meal
                      ) all_meals
                    GROUP BY transformuserid, date, category
                  ) meal_date
               LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = meal_date.transformuserid
               LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid
           ) meal_bucket
      GROUP BY transformuserid, width_bucket(meal_bucket.days, 1, 364, 52)
    ) meals_weekly ON meals_weekly.transformuserid = tu.id AND meals_weekly.week_bucket = week_series


  WHERE tu.cohortid >= 11 AND tu.cohortid NOT IN (15, 16, 19, 222, 223, 353, 354, 355, 500, 518, 632, 646, 657, 9988)
  ORDER BY tu.cohortid, tu.fullname, week_series
;