WITH
     code_7  AS (
       SELECT  *
       ,CASE
       WHEN milestonedate >= '2018-05-31' THEN
             CASE WHEN milestonetype = 'M1' THEN 96
             WHEN milestonetype = 'M2' THEN 83
             WHEN milestonetype = 'M3' THEN 93
             WHEN milestonetype = 'M4' THEN 118.60
             ELSE 0
             END
       ELSE
             CASE WHEN milestonetype = 'M1' THEN 132
             WHEN milestonetype = 'M2' THEN 83
             WHEN milestonetype = 'M3' THEN 93
             WHEN milestonetype = 'M4' THEN 103.60
             ELSE 0
             END
       END AS milestone_value
       FROM solera.milestones_bmh_computed
       INNER JOIN solera.milestone_status
       ON milestones_bmh_computed.id = milestone_status.bmhmilestoneid
       WHERE 1=1
             AND  milestone_status.statustypeid = 7
             AND  milestonedate BETWEEN '2018-01-01'::DATE AND '2019-03-15'::DATE
     ),
     success_eng_resp_weight AS  (
       SELECT DISTINCT ON(nw_partner_id, activity_date, weightlbs)
                          weightlbs
                         ,nw_partner_id
                         ,activity_date::DATE
                         ,message
       FROM solera.engagementresponse enresp
       WHERE 1=1
             AND enresp.message = 'Success'
             AND enresp.weightlbs != ''
       ORDER BY nw_partner_id, activity_date, weightlbs
     ),
     first_success_eng_resp_weight AS (
              SELECT DISTINCT ON(nw_partner_id)
                          weightlbs
                         ,nw_partner_id
                         ,activity_date::DATE
                         ,message
       FROM solera.engagementresponse enresp
       WHERE 1=1
             AND enresp.message = 'Success'
             AND enresp.weightlbs != ''
       ORDER BY nw_partner_id, activity_date ASC
     )
SELECT legit_m4s.*
FROM
  (SELECT
       code_7.transformuserid
      ,code_7.soleraid
      ,code_7.milestonedate
      ,DATE_PART('year', code_7.milestonedate) AS milestone_year
      ,code_7.milestonetype
      ,code_7.milestone_value
      ,serw.activity_date
      ,serw.message
      ,ferw.weightlbs AS first_weight
      ,serw.weightlbs
      ,(ferw.weightlbs::FLOAT - serw.weightlbs::FLOAT) / ferw.weightlbs::FLOAT AS weight_loss
FROM code_7
LEFT JOIN success_eng_resp_weight serw
ON code_7.transformuserid = serw.nw_partner_id::SMALLINT AND code_7.milestonedate::DATE = serw.activity_date::DATE
LEFT JOIN first_success_eng_resp_weight ferw
ON code_7.transformuserid = ferw.nw_partner_id::SMALLINT
WHERE 1=1
      AND serw.activity_date NOTNULL
      AND code_7.milestonetype = 'M4') legit_m4s
WHERE weight_loss <= -1 OR weight_loss >= 0.049
;