-- Select all rows which contain a null value in any column:

SELECT  *
FROM  <table> <alias>
WHERE NOT(<alias> IS NOT NULL)