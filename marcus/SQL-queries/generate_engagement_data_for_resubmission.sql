WITH all_engagement AS (
    SELECT *
    FROM (

             -- -----------------------------------------------------------------
             -- START WEIGHT is summarized onto the participant's start date, if we're reporting on that period
             -- -----------------------------------------------------------------
             SELECT '122'                                               AS "Partner ID"
                  , s.soleraid                                          AS "Solera ID"
                  , w.transformuserid                                   AS "NW Blue Mesa"
                  , to_char(w.timestamp, 'YYYYMMDD')                    AS "Activity Date"
                  , to_char(min(w.valuelbs :: FLOAT), 'FM999999990.00') AS "Weight lbs"
                  , ''                                                  AS "Physical Activity Minutes"
                  , ''                                                  AS "Articles Read"
                  , ''                                                  AS "Meals Logged"
                  , ''                                                  AS "Group Interactions"
                  , ''                                                  AS "Group Interaction Type"
                  , ''                                                  AS "Coach Interactions"
                  , ''                                                  AS "Coach Interaction Type"
                  , ''                                                  AS "Coach Interaction Duration"
                  , ''                                                  AS "Quiz/Survey's Completed"
                  , ''                                                  AS "Lessons/Education Modules Completed"
                  , ''                                                  AS "App Points Obtained"
                  , ''                                                  AS "Weekly Video Watched (%)"
                  , ''                                                  AS "Goal Behaviors Set"
                  , ''                                                  AS "Coach Name"
             FROM (
                      SELECT tu.id            AS transformuserid
                           , wb.start_weight  AS valuelbs
                           , cohort.startdate AS timestamp
                      FROM bmh_prod_na.transformuser tu
                               LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                               LEFT JOIN bmh_prod_na.view_weight_bounds wb ON wb.transformuserid = tu.id
                      WHERE 1 = 1
                        AND cohort.startdate :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                        AND wb.start_weight NOTNULL
                  ) w
                      LEFT JOIN bmh_prod_na.soleramap s ON s.transformuserid = w.transformuserid
                      LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = w.transformuserid
                      LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid

             GROUP BY w.transformuserid, to_char(w.timestamp, 'YYYYMMDD'), s.soleraid
             HAVING soleraid NOTNULL

             UNION

             -- -----------------------------------------------------------------
             -- WEIGHT readings after cohort start date
             -- -----------------------------------------------------------------
             SELECT '122'                                               AS "Partner ID"
                  , S.soleraid                                          AS "Solera ID"
                  , W.transformuserid                                   AS "NW Blue Mesa"
                  , REPLACE(LEFT(W.timestamp, 10), '-', '')             AS "Activity Date"
                  , to_char(MIN(W.valuelbs :: FLOAT), 'FM999999990.00') AS "Weight lbs"
                  , ''                                                  AS "Physical Activity Minutes"
                  , ''                                                  AS "Articles Read"
                  , ''                                                  AS "Meals Logged"
                  , ''                                                  AS "Group Interactions"
                  , ''                                                  AS "Group Interaction Type"
                  , ''                                                  AS "Coach Interactions"
                  , ''                                                  AS "Coach Interaction Type"
                  , ''                                                  AS "Coach Interaction Duration"
                  , ''                                                  AS "Quiz/Survey's Completed"
                  , ''                                                  AS "Lessons/Education Modules Completed"
                  , ''                                                  AS "App Points Obtained"
                  , ''                                                  AS "Weekly Video Watched (%)"
                  , ''                                                  AS "Goal Behaviors Set"
                  , ''                                                  AS "Coach Name"
             FROM (
                      SELECT tu.id AS transformuserid
                           , W.valuelbs
                           , W.timestamp
                      FROM bmh_prod_na.transformuser tu
                               LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                               LEFT JOIN (
                          -- If the first weight ever for a participant is during the first program week,
                          -- in would appear above query result, duplicating the "Start Weight" information in the section above.
                          -- Therefore, identify that first weight IFF it's in a participant's first program week and hide
                          -- all of the weights on that day from the reporting list below to cleanly move that first weight
                          -- back to the cohort start date
                          SELECT s.transformuserid
                               , s.valuelbs
                               , s.timestamp
                          FROM (
                                   -- Get the global first weight for a participant
                                   SELECT DISTINCT ON (tu.id) tu.id AS transformuserid
                                                            , W.valuelbs
                                                            , W.timestamp :: DATE
                                   FROM bmh_prod_na.transformuser tu
                                            LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                                            LEFT JOIN bmh_prod_na.weight W ON W.transformuserid = tu.id
                                       AND W.deletedat ISNULL
                                   WHERE W.valuelbs NOTNULL
                                     AND W.deletedat ISNULL
                                   ORDER BY tu.id, timestamp ASC
                               ) s
                                   LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = transformuserid
                                   LEFT JOIN bmh_prod_na.cohort ON cohort.id = tu.cohortid
                               -- And then only keep it if it happens to be in their first program week
                          WHERE timestamp :: DATE BETWEEN cohort.startdate :: DATE
                                    AND
                                    cohort.startdate + INTERVAL '6 DAYS'
                      ) if_first_weight_in_first_period ON if_first_weight_in_first_period.transformuserid = tu.id
                               LEFT JOIN bmh_prod_na.weight W ON W.transformuserid = tu.id
                          AND W.deletedat ISNULL
                          AND W.timestamp :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                          AND W.timestamp :: DATE > cohort.startdate :: DATE
                          AND W.timestamp :: DATE IS DISTINCT FROM
                              (if_first_weight_in_first_period.timestamp :: DATE)


                      WHERE W.valuelbs NOTNULL
                        AND W.deletedat ISNULL
                  ) W
                      LEFT JOIN bmh_prod_na.soleramap S ON S.transformuserid = W.transformuserid
                      LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = W.transformuserid
                      LEFT JOIN bmh_prod_na.cohort C ON C.id = tu.cohortid
             WHERE W.timestamp :: TIMESTAMP >= C.startdate :: TIMESTAMP
             GROUP BY W.transformuserid, REPLACE(LEFT(W.timestamp, 10), '-', ''), S.soleraid
             HAVING soleraid NOTNULL


                    -- -----------------------------------------------------------------
                    -- MEALS
                    -- -----------------------------------------------------------------
             UNION
             SELECT '122'                     AS "Partner ID"
                  , soleraid                  AS "Solera ID"
                  , transformuserid           AS "NW Blue Mesa"
                  , to_char(date, 'YYYYMMDD') AS "Activity Date"
                  , ''                        AS "Weight lbs"
                  , ''                        AS "Physical Activity Minutes"
                  , ''                        AS "Articles Read"
                  , meals :: TEXT             AS "Meals Logged"
                  , ''                        AS "Group Interactions"
                  , ''                        AS "Group Interaction Type"
                  , ''                        AS "Coach Interactions"
                  , ''                        AS "Coach Interaction Type"
                  , ''                        AS "Coach Interaction Duration"
                  , ''                        AS "Quiz/Survey's Completed"
                  , ''                        AS "Lessons/Education Modules Completed"
                  , ''                        AS "App Points Obtained"
                  , ''                        AS "Weekly Video Watched (%)"
                  , ''                        AS "Goal Behaviors Set"
                  , ''                        AS "Coach Name"

             FROM (
                      SELECT sm.soleraid
                           , tu.id          AS transformuserid
                           , M.date :: DATE AS date
                           , COUNT(*)       AS meals
                      FROM bmh_prod_na.transformuser tu
                               LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                               LEFT JOIN bmh_prod_na.soleramap sm ON sm.transformuserid = tu.id
                               LEFT JOIN (
                          SELECT transformuserid
                               , date :: DATE AS date
                               , CATEGORY
                          FROM bmh_prod_na.meal
                          GROUP BY transformuserid, date :: DATE, CATEGORY
                      ) M ON M.transformuserid = tu.id

                      WHERE sm.soleraid NOTNULL
                        AND M.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                        AND M.date :: DATE >= cohort.startdate :: DATE
                        --                 AND tu.id in (8240)
                      GROUP BY sm.soleraid, tu.id, M.date
                  ) M


                  -- -----------------------------------------------------------------
                  -- GROUP INTERACTIONS
                  -- -----------------------------------------------------------------
             UNION
             SELECT '122'                             AS "Partner ID"
                  , soleraid                          AS "Solera ID"
                  , transformuserid                   AS "NW Blue Mesa"
                  , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
                  , ''                                AS "Weight lbs"
                  , ''                                AS "Physical Activity Minutes"
                  , ''                                AS "Articles Read"
                  , ''                                AS "Meals Logged"
                  , messages :: TEXT                  AS "Group Interactions"
                  , '6'                               AS "Group Interaction Type"
                  , ''                                AS "Coach Interactions"
                  , ''                                AS "Coach Interaction Type"
                  , ''                                AS "Coach Interaction Duration"
                  , ''                                AS "Quiz/Survey's Completed"
                  , ''                                AS "Lessons/Education Modules Completed"
                  , ''                                AS "App Points Obtained"
                  , ''                                AS "Weekly Video Watched (%)"
                  , ''                                AS "Goal Behaviors Set"
                  , ''                                AS "Coach Name"
             FROM (
                      SELECT M.transformuserid
                           , sm.soleraid
                           , LEFT(M.date, 10) AS date
                           , COUNT(*)         AS messages
                      FROM bmh_prod_na.message M
                               LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = M.transformuserid
                               LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                               LEFT JOIN bmh_prod_na.conversation C ON C.id = M.conversationid
                               LEFT JOIN bmh_prod_na.soleramap sm ON sm.transformuserid = M.transformuserid
                      WHERE sm.soleraid NOTNULL
                        AND C.distinct = FALSE
                        AND M.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                        AND M.date :: DATE >= cohort.startdate :: DATE
                        --           and m.transformuserid in (6203)
                      GROUP BY LEFT(M.date, 10),
                               M.transformuserid,
                               sm.soleraid
                  ) conversations

                  -- -----------------------------------------------------------------
                  -- COACH INTERACTIONS
                  -- -----------------------------------------------------------------
             UNION
             SELECT '122'                             AS "Partner ID"
                  , soleraid                          AS "Solera ID"
                  , transformuserid                   AS "NW Blue Mesa"
                  , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
                  , ''                                AS "Weight lbs"
                  , ''                                AS "Physical Activity Minutes"
                  , ''                                AS "Articles Read"
                  , ''                                AS "Meals Logged"
                  , ''                                AS "Group Interactions"
                  , ''                                AS "Group Interaction Type"
                  , COUNT :: TEXT                     AS "Coach Interactions"
                  , TYPE :: TEXT                      AS "Coach Interaction Type"
                  , (COUNT * TIME) :: TEXT            AS "Coach Interaction Duration"
                  , ''                                AS "Quiz/Survey's Completed"
                  , ''                                AS "Lessons/Education Modules Completed"
                  , ''                                AS "App Points Obtained"
                  , ''                                AS "Weekly Video Watched (%)"
                  , ''                                AS "Goal Behaviors Set"
                  , ''                                AS "Coach Name"
             FROM (
                      SELECT sm.soleraid AS soleraid
                           , C.transformuserid
                           , C.date :: DATE
                           , C.type      AS type_str
                           , CASE
                                 WHEN C.type = 'PHONE'
                                     THEN 1
                                 WHEN C.type = 'SMS'
                                     THEN 2
                                 WHEN C.type = 'CHAT'
                                     THEN 3
                                 WHEN C.type = 'EMAIL'
                                     THEN 6
                          END            AS TYPE
                           , COUNT(*)    AS COUNT
                           , CASE
                                 WHEN C.type = 'PHONE'
                                     THEN 30
                                 WHEN C.type = 'SMS'
                                     THEN 7
                                 WHEN C.type = 'CHAT'
                                     THEN 7
                                 WHEN C.type = 'EMAIL'
                                     THEN 15
                          END            AS TIME
                      FROM bmh_prod_na.contact C
                               LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = C.transformuserid
                               LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                               LEFT JOIN bmh_prod_na.soleramap sm ON sm.transformuserid = C.transformuserid
                      WHERE C.response = TRUE
                        AND sm.soleraid NOTNULL
                        AND C.type != 'CHAT'
                        AND C.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                        AND C.date :: DATE >= cohort.startdate :: DATE
                        --         and c.transformuserid in (6203)
                      GROUP BY sm.soleraid, C.transformuserid, C.date :: DATE, C.type
                  ) interactions


             UNION
             SELECT '122'                             AS "Partner ID"
                  , soleraid                          AS "Solera ID"
                  , transformuserid                   AS "NW Blue Mesa"
                  , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
                  , ''                                AS "Weight lbs"
                  , ''                                AS "Physical Activity Minutes"
                  , ''                                AS "Articles Read"
                  , ''                                AS "Meals Logged"
                  , ''                                AS "Group Interactions"
                  , ''                                AS "Group Interaction Type"
                  , messages :: TEXT                  AS "Coach Interactions"
                  , '3'                               AS "Coach Interaction Type"
                  , (messages * 7) :: TEXT            AS "Coach Interaction Duration"
                  , ''                                AS "Quiz/Survey's Completed"
                  , ''                                AS "Lessons/Education Modules Completed"
                  , ''                                AS "App Points Obtained"
                  , ''                                AS "Weekly Video Watched (%)"
                  , ''                                AS "Goal Behaviors Set"
                  , ''                                AS "Coach Name"
             FROM (
                      SELECT M.transformuserid
                           , sm.soleraid
                           , LEFT(M.date, 10) AS date
                           , COUNT(*)         AS messages
                      FROM bmh_prod_na.message M
                               LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = M.transformuserid
                               LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                               LEFT JOIN bmh_prod_na.conversation C ON C.id = M.conversationid
                               LEFT JOIN bmh_prod_na.soleramap sm ON sm.transformuserid = M.transformuserid
                      WHERE sm.soleraid NOTNULL
                        AND C.distinct = TRUE
                        AND M.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                        AND M.date :: DATE >= cohort.startdate :: DATE
                        AND C.iswithcoach = TRUE
                        --             and m.transformuserid in (6203)
                      GROUP BY LEFT(M.date, 10),
                               M.transformuserid,
                               sm.soleraid
                  ) conversations

             UNION
             SELECT '122'                             AS "Partner ID"
                  , soleraid                          AS "Solera ID"
                  , transformuserid                   AS "NW Blue Mesa"
                  , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
                  , ''                                AS "Weight lbs"
                  , ''                                AS "Physical Activity Minutes"
                  , ''                                AS "Articles Read"
                  , ''                                AS "Meals Logged"
                  , ''                                AS "Group Interactions"
                  , ''                                AS "Group Interaction Type"
                  , 1 :: TEXT                         AS "Coach Interactions"
                  , 1 :: TEXT                         AS "Coach Interaction Type"
                  , 10 :: TEXT                        AS "Coach Interaction Duration"
                  , ''                                AS "Quiz/Survey's Completed"
                  , ''                                AS "Lessons/Education Modules Completed"
                  , ''                                AS "App Points Obtained"
                  , ''                                AS "Weekly Video Watched (%)"
                  , ''                                AS "Goal Behaviors Set"
                  , ''                                AS "Coach Name"
             FROM (
                      SELECT tu.id                    AS transformuserid
                           , sm.soleraid
                           , cohort.startdate :: DATE AS date
                      FROM bmh_prod_na.transformuser tu
                               LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                               LEFT JOIN bmh_prod_na.soleramap sm ON sm.transformuserid = tu.id
                      WHERE cohort.startdate :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                        AND sm.soleraid NOTNULL
                  ) firstday


                  -- -----------------------------------------------------------------
                  -- LESSONS
                  -- -----------------------------------------------------------------

             UNION
             SELECT '122'                             AS "Partner ID"
                  , soleraid                          AS "Solera ID"
                  , transformuserid                   AS "NW Blue Mesa"
                  , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
                  , ''                                AS "Weight lbs"
                  , ''                                AS "Physical Activity Minutes"
                  , ''                                AS "Articles Read"
                  , ''                                AS "Meals Logged"
                  , ''                                AS "Group Interactions"
                  , ''                                AS "Group Interaction Type"
                  , ''                                AS "Coach Interactions"
                  , ''                                AS "Coach Interaction Type"
                  , ''                                AS "Coach Interaction Duration"
                  , concat(quiz :: TEXT, '')          AS "Quiz/Survey's Completed"
                  , ''                                AS "Lessons/Education Modules Completed"
                  , ''                                AS "App Points Obtained"
                  , ''                                AS "Weekly Video Watched (%)"
                  , ''                                AS "Goal Behaviors Set"
                  , ''                                AS "Coach Name"
             FROM (
                      SELECT sm.soleraid                                    AS soleraid
                           , tu.id                                          AS transformuserid
                           , to_char(quizdoneat :: TIMESTAMP, 'YYYY-MM-DD') AS date
                           , COUNT(*)                                       AS quiz
                      FROM bmh_prod_na.transformuser tu
                               LEFT JOIN bmh_prod_na.soleramap sm ON sm.transformuserid = tu.id
                               LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                               LEFT JOIN bmh_prod_na.lessonprogress lp ON lp.transformuserid = tu.id
                      WHERE sm.soleraid NOTNULL
                        AND lp.quizdoneat :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                        AND lp.quizdoneat :: DATE >= cohort.startdate :: DATE
                      GROUP BY sm.soleraid, tu.id, to_char(quizdoneat :: TIMESTAMP, 'YYYY-MM-DD')
                  ) lp
             WHERE soleraid NOTNULL
               AND quiz NOTNULL

             UNION
             SELECT '122'                             AS "Partner ID"
                  , soleraid                          AS "Solera ID"
                  , transformuserid                   AS "NW Blue Mesa"
                  , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
                  , ''                                AS "Weight lbs"
                  , ''                                AS "Physical Activity Minutes"
                  , ''                                AS "Articles Read"
                  , ''                                AS "Meals Logged"
                  , ''                                AS "Group Interactions"
                  , ''                                AS "Group Interaction Type"
                  , ''                                AS "Coach Interactions"
                  , ''                                AS "Coach Interaction Type"
                  , ''                                AS "Coach Interaction Duration"
                  , ''                                AS "Quiz/Survey's Completed"
                  , concat(CONTENT :: TEXT, '')       AS "Lessons/Education Modules Completed"
                  , ''                                AS "App Points Obtained"
                  , ''                                AS "Weekly Video Watched (%)"
                  , ''                                AS "Goal Behaviors Set"
                  , ''                                AS "Coach Name"
             FROM (
                      SELECT sm.soleraid                                       AS soleraid
                           , tu.id                                             AS transformuserid
                           , to_char(contentdoneat :: TIMESTAMP, 'YYYY-MM-DD') AS date
                           , COUNT(*)                                          AS CONTENT
                      FROM bmh_prod_na.transformuser tu
                               LEFT JOIN bmh_prod_na.soleramap sm ON sm.transformuserid = tu.id
                               LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                               LEFT JOIN bmh_prod_na.lessonprogress lp ON lp.transformuserid = tu.id
                      WHERE sm.soleraid NOTNULL
                        AND lp.contentdoneat :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                        AND lp.contentdoneat :: DATE >= cohort.startdate :: DATE
                      GROUP BY sm.soleraid, tu.id, to_char(contentdoneat :: TIMESTAMP, 'YYYY-MM-DD')
                  ) lp
             WHERE soleraid NOTNULL
               AND CONTENT NOTNULL

               -- -----------------------------------------------------------------
               -- PHYSICAL ACTIVITY
               -- -----------------------------------------------------------------

             UNION
             SELECT '122'                             AS "Partner ID"
                  , soleraid                          AS "Solera ID"
                  , transformuserid                   AS "NW Blue Mesa"
                  , to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date"
                  , ''                                AS "Weight lbs"
                  , CASE
                        WHEN MINUTES = 0
                            THEN NULL
                        ELSE MINUTES :: TEXT END      AS "Physical Activity Minutes"
                  , ''                                AS "Articles Read"
                  , ''                                AS "Meals Logged"
                  , ''                                AS "Group Interactions"
                  , ''                                AS "Group Interaction Type"
                  , ''                                AS "Coach Interactions"
                  , ''                                AS "Coach Interaction Type"
                  , ''                                AS "Coach Interaction Duration"
                  , ''                                AS "Quiz/Survey's Completed"
                  , ''                                AS "Lessons/Education Modules Completed"
                  , ''                                AS "App Points Obtained"
                  , ''                                AS "Weekly Video Watched (%)"
                  , ''                                AS "Goal Behaviors Set"
                  , ''                                AS "Coach Name"
             FROM (
                      SELECT sm.soleraid                 AS soleraid
                           , tu.id                       AS transformuserid
                           , fad.date                    AS date
                           , round(fad.activeminutes, 0) AS minutes
                      FROM bmh_prod_na.transformuser tu
                               LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                               LEFT JOIN bmh_prod_na.soleramap sm ON sm.transformuserid = tu.id
                               LEFT JOIN bmh_prod_na.activityday fad ON fad.transformuserid = tu.id
                      WHERE sm.soleraid NOTNULL
                        AND fad.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                        AND fad.date :: DATE >= cohort.startdate :: DATE
                        AND (fad.activeminutes) > 0
                  ) am
             WHERE am.soleraid NOTNULL
               AND am.minutes NOTNULL
         ) T
),
tus_with_unrecog AS (
       SELECT DISTINCT
       transformuserid
       FROM solera.milestones_bmh_computed
       INNER JOIN solera.milestone_status
       ON milestones_bmh_computed.id = milestone_status.bmhmilestoneid
       WHERE 1=1 AND
             milestone_status.statustypeid NOT IN (1,3,12) AND
             milestonedate BETWEEN '2017-01-01'::DATE AND '2019-04-01'::DATE
     ),
unrecog_engagement AS (
    SELECT *
    FROM
    all_engagement
    INNER JOIN tus_with_unrecog
    ON  all_engagement."NW Blue Mesa"::INTEGER = tus_with_unrecog.transformuserid)
SELECT *
FROM    unrecog_engagement
ORDER BY unrecog_engagement."NW Blue Mesa" ASC, unrecog_engagement."Activity Date" ASC
;