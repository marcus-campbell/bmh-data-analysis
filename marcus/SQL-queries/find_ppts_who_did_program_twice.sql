-- Find UUIDs that have multiple transformuserids
-- NULL UUIDs are not necessarily cause for alarm; demos, coaches, and other BMH staff will have NULL UUIDs

SELECT  uuid,
        COUNT(transformuser)
FROM    bmh_prod_na.transformuser
GROUP BY uuid
HAVING COUNT(transformuser) > 1
;