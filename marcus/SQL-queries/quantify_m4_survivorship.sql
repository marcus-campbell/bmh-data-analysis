-- Calculate M4 survivorship / procurement rate


WITH m1  AS (SELECT *
                FROM solera.milestones_bmh_computed
                WHERE milestonetype = 'M1'
     ),
     m4  AS (SELECT *
                FROM solera.milestones_bmh_computed
                WHERE milestonetype = 'M4'
     ),
     surv AS (SELECT   TRUNC(DATE_PART('day', m4.milestonedate::TIMESTAMP - m1.milestonedate::TIMESTAMP) / 7) + 1    AS program_week
                       ,COUNT(*)
              FROM    m1
              LEFT JOIN  m4
              ON m1.transformuserid = m4.transformuserid
              WHERE 1=1
                    AND m1.milestonedate BETWEEN '2018-01-01' AND '2018-05-31' -- Update these dates to account for new M4s
              GROUP BY program_week
              ORDER BY program_week
     )
     SELECT surv.program_week
            ,surv.count / SUM(surv.count) OVER ()   AS m4_survivorship
     FROM   surv
;
