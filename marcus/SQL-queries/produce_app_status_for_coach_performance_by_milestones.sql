-- This query is used to populate the "App Status" tab of the Coach Performance by Milestones spreadsheet,
-- which is produced monthly.

WITH most_recent_tu_status AS (
        SELECT DISTINCT ON(transformuserid)
        transformuserid
        ,statusid
        FROM bmh_prod_na.participantstatus
        ORDER BY transformuserid, createdat DESC)
SELECT mrts.*
       ,bmhs.shortcode
FROM most_recent_tu_status mrts
LEFT JOIN bmh_prod_na.status bmhs
ON mrts.statusid = bmhs.id
;