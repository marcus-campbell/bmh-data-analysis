WITH
     mls  AS (
       SELECT  *
       ,CASE
       WHEN milestonedate >= '2018-05-31' THEN
             CASE WHEN milestonetype = 'M1' THEN 96
             WHEN milestonetype = 'M2' THEN 83
             WHEN milestonetype = 'M3' THEN 93
             WHEN milestonetype = 'M4' THEN 118.60
             ELSE 0
             END
       ELSE
             CASE WHEN milestonetype = 'M1' THEN 132
             WHEN milestonetype = 'M2' THEN 83
             WHEN milestonetype = 'M3' THEN 93
             WHEN milestonetype = 'M4' THEN 103.60
             ELSE 0
             END
       END AS milestone_value
       FROM solera."milestones_bmh_computed" mbc
       INNER JOIN solera."milestone_status" ms
       ON mbc.id = ms.bmhmilestoneid
       WHERE 1=1 AND
             ms.statustypeid NOT IN (1,3,12) AND
             milestonedate BETWEEN '2018-01-01'::DATE AND '2019-06-21'::DATE
       ORDER BY statustypeid
     )
SELECT
       CASE WHEN statustypeid NOTNULL THEN statustypeid::TEXT
            WHEN statustypeid ISNULL  THEN 'Total'
            END AS statustypeid
       ,COUNT(bmhmilestoneid)
       ,SUM(milestone_value)
FROM mls
GROUP BY ROLLUP (statustypeid)
;