WITH enres AS (
SELECT      solera_id
           ,filename
           ,enrolled_date
           -- Below, failed enrollment RESPONSES are coded with 'N'
           -- Successful enrollments are coded with 'Y'
           -- This CASE statement intentionally excludes PPTs for which BMH sent a 
           -- disenrollment signal.
           ,CASE
                WHEN message LIKE '%Closed%' OR
                     message LIKE '%expired%' OR
                     message LIKE '%prior%' OR
                     message LIKE 'NULL'  THEN 'N'
                ELSE 'Y'
            END AS enrol_response
FROM        solera.enrollmentresponse res)
SELECT  solera_id
       -- We use STRING_AGG to generate a sequence of all enrollment responses for each PPT
       ,STRING_AGG(enrol_response::TEXT, '' ORDER BY filename) AS enrol_response_sequence
       ,MIN(enrolled_date)  AS first_enrollment_attempt
FROM    enres
GROUP BY enres.solera_id
HAVING   STRING_AGG(enrol_response::TEXT, '' ORDER BY filename) NOT LIKE '%Y%'
ORDER BY first_enrollment_attempt
;