-- Note: mat_view_dt_participant_status_expanded is pulling from North American data only (so no Merck)

WITH    eligible_ppts AS (
            SELECT  transformuserid
            FROM    scratch_marcuscampbell.mat_view_dt_participant_status_expanded
            WHERE   1=1
                    AND cohortid NOT IN (1,2,3,4,5,6,7,8,9,10,15,16,19,222,223,353,354,355,500,518,632,646,657,9988) -- Exclude demo cohorts
                    AND cohortid NOT IN (297, 489) -- Exclude CRC cohorts
                    AND expanded_statusid IN (3,7) -- Include only active ppts
                    AND program_day = 365 -- roughly corresponds to week 16
            ORDER BY transformuserid, program_day),
        weekly_engagement AS (
            SELECT  transformuserid
                    ,week_series
                    ,meals_count
                    ,meal_criteria
                    ,weight_count
                    ,weighin_criteria
                    ,group_chat_count
                    ,group_criteria
                    ,contact_count
                    ,contact_criteria
                    ,quiz_done
                    ,quiz_criteria
                    ,CASE
                        WHEN meal_criteria + weighin_criteria + group_criteria + contact_criteria + quiz_criteria >= 2
                        THEN 1
                        ELSE 0
                        END AS weekly_criteria_met
            FROM    scratch_marcuscampbell.dt_criteria_weekly
            WHERE   1=1
                    AND cohortid NOT IN (1,2,3,4,5,6,7,8,9,10,15,16,19,222,223,353,354,355,500,518,632,646,657,9988) -- Exclude demo cohorts
                    AND cohortid NOT IN (297, 489) -- Exclude CRC cohorts
                    AND week_series BETWEEN 51 AND 52),
        engaged_subset AS (
            SELECT  weekly_engagement.*
            FROM    eligible_ppts
            INNER JOIN weekly_engagement
            ON eligible_ppts.transformuserid = weekly_engagement.transformuserid
            ORDER BY    weekly_engagement.transformuserid ASC, week_series ASC),
        engaged_sum AS (
            SELECT  SUM(weekly_criteria_met)    AS sum_of_weeks
            FROM    engaged_subset
            GROUP BY transformuserid)
SELECT  (SELECT COUNT(*)::NUMERIC FROM engaged_sum WHERE sum_of_weeks >= 1) / COUNT(*)::NUMERIC AS percent_engaged
        ,(SELECT COUNT(*)::NUMERIC FROM engaged_sum WHERE sum_of_weeks >= 1) AS numerator
        ,COUNT(*)   AS denominator
FROM    engaged_sum
;