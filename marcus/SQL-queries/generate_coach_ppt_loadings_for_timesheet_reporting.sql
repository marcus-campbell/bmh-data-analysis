-- Refresh view

REFRESH MATERIALIZED VIEW   scratch_marcuscampbell.mat_view_dt_participant_status_expanded
;

-- Current payroll period:
-- Select subset of ppts that excludes demo cohorts, and within period of interest
WITH ppt_subset AS (
    SELECT  *
    FROM    scratch_marcuscampbell.mat_view_dt_participant_status_expanded
    WHERE   1=1
            AND expanded_statusid_granular IN (30,31,32)
            AND cohortid NOT IN (7,8,9,10,15,16,19,222,223,353,354,355,500,518,632,646,657,9988)
            AND expanded_date BETWEEN '2019-06-16'::DATE AND '2019-06-30'::DATE
    ),
     ppt_status  AS (
    SELECT  transformuserid
            ,MAX(cohortid)  AS cohortid
            ,MODE() WITHIN GROUP (ORDER BY expanded_statusid_granular DESC) AS status
    FROM    ppt_subset
    GROUP BY    transformuserid
    ),
     cohort_counts  AS (
         SELECT cohortid
                ,status
                ,COUNT(status)  AS ppts
         FROM   ppt_status
         GROUP BY cohortid, status
     ),
     cohort_sheet   AS (
         SELECT cohortid
                ,COALESCE(MAX(CASE WHEN status = '30' THEN ppts END), 0) AS core
                ,COALESCE(MAX(CASE WHEN status = '31' THEN ppts END), 0) AS biweekly
                ,COALESCE(MAX(CASE WHEN status = '32' THEN ppts END), 0) AS maintenance
         FROM    cohort_counts
         GROUP BY cohortid
     ),
     coach_to_ppts  AS (
         SELECT  ccm.transformuserid AS coach_id
                 ,tu.fullname        AS coach_name
                 ,cohort_sheet.*
         FROM    cohort_sheet
         INNER JOIN bmh_prod_na.cohortcoachmapping ccm
         ON cohort_sheet.cohortid = ccm.cohortid
         INNER JOIN bmh_prod_na.transformuser tu
         ON ccm.transformuserid = tu.id
         WHERE 1=1
               AND (ccm.deletedat ISNULL AND ccm.createdat < '2019-06-16'::DATE OR ccm.deletedat::DATE > '2019-06-30'::DATE)
         ORDER BY coach_id
     ),
     final_report  AS (
         SELECT coach_name
                ,MAX(coach_id) AS coach_id
                ,SUM(core)  AS core
                ,SUM(biweekly)  AS biweekly
                ,SUM(maintenance)   AS maintenance
         FROM   coach_to_ppts
         GROUP BY coach_name
         ORDER BY coach_name ASC
     )
SELECT  *
FROM    final_report
WHERE   coach_id NOT IN (121, 123, 149, 152, 154) -- Exclude non-US and salaried coaches
;


-- Output coach timesheet logs for different payroll categories.
WITH coaches AS (SELECT CONCAT(fname, ' ', lname) AS fullname
                      , *
                 FROM tsheets."USA Health Coach - timesheet_report_2019-06-16_thru_2019-06-30"
                ),
     participant_hours AS (SELECT fullname
                                 ,SUM(hours) AS participant
                           FROM   coaches
                           WHERE  jobcode_2 = 'Participant'
                           GROUP BY fullname
                ),
     admin_hours AS (SELECT fullname
                                 ,SUM(hours) AS admin
                           FROM   coaches
                           WHERE  jobcode_2 = 'Admin'
                           GROUP BY fullname
                ),
     tech_hours AS (SELECT fullname
                                 ,SUM(hours) AS tech
                           FROM   coaches
                           WHERE  jobcode_2 = 'Tech Issues'
                           GROUP BY fullname
                ),
     project_hours AS (SELECT fullname
                                 ,SUM(hours) AS projects
                           FROM   coaches
                           WHERE  jobcode_2 = 'Projects'
                           GROUP BY fullname)
SELECT  *
FROM    project_hours
;
