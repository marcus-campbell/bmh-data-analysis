WITH cohort_subset AS (
    SELECT *
    FROM    bmh_prod_na.cohort
    WHERE   1=1
--            AND cohort.name LIKE '%Solera%' -- This may not be necessary. Instead you should use all cohorts and then filter out demos and non-USA
            AND cohort.startdate >= CURRENT_DATE - INTERVAL '1 year'
    ORDER BY cohort.startdate
    ),
     coach_mapping AS (
    SELECT cohortcoachmapping.cohortid
           ,cohortcoachmapping.transformuserid AS coach_id
       --    ,cohort_subset.startdate
    FROM    bmh_prod_na.cohortcoachmapping
            INNER JOIN  cohort_subset
            ON cohort_subset.id = cohortcoachmapping.cohortid
    WHERE   1=1
            AND cohortcoachmapping.deletedat ISNULL
            AND transformuserid NOT IN (19, 120, 168)
    ),
     coach_name_mapping AS (
    SELECT fullname
           ,id  AS coach_id
           ,cohortid
    FROM    bmh_prod_na.transformuser
    WHERE   1=1
            AND cohortid = 15
     ),
     most_recent_ppt_status AS (
     SELECT DISTINCT ON (transformuserid)
                        *
     FROM   bmh_prod_na.participantstatus
     ORDER BY transformuserid, updatedat DESC
     ),
     ppt_status AS (
     SELECT *
     FROM most_recent_ppt_status
     WHERE  statusid IN (3,6) -- keep ppts with "Active" and "NAR" status in HC dashboard
     ),
     valid_tus AS (
     SELECT transformuser.id
            ,transformuser.cohortid
     FROM   bmh_prod_na.transformuser
            INNER JOIN ppt_status
            ON transformuser.id = ppt_status.transformuserid
     ),
     cohort_loads AS (
    SELECT DISTINCT ON (valid_tus.cohortid)
           cohortid
           ,COUNT(valid_tus.id) OVER(PARTITION BY cohortid) AS ppts
           ,cohort_subset.startdate
    FROM valid_tus
         INNER JOIN cohort_subset
         ON valid_tus.cohortid = cohort_subset.id
  --  GROUP BY cohortid
     ),
     coach_loads AS (
         SELECT REPLACE(fullname, ' Coach', '') AS coach_name, -- Remove instances of ' Coach' in coach names.
                cohort_loads.startdate::DATE    AS cohort_start_date,
                NOW()::DATE - cohort_loads.startdate::DATE AS program_day,
                SUM(ppts)   AS ppts
         FROM   cohort_loads
                INNER JOIN coach_mapping
                ON cohort_loads.cohortid = coach_mapping.cohortid
                INNER JOIN coach_name_mapping
                ON coach_mapping.coach_id = coach_name_mapping.coach_id
         GROUP BY fullname, cohort_loads.startdate
         ORDER by fullname
     ),
     granular_coach_loads AS (
         SELECT *
      ,CASE WHEN program_day <= 112 THEN ppts
                ELSE 0
       END AS core
      ,CASE WHEN program_day BETWEEN 113 AND 154 THEN ppts
                ELSE 0
       END AS biweekly
      ,CASE WHEN program_day > 154 THEN ppts
                ELSE 0
       END AS maintenance
FROM    coach_loads
     )
SELECT coach_name
       ,SUM(core) AS core_ppts
       ,SUM(biweekly) AS biweekly_ppts
       ,SUM(maintenance) AS maintenance_ppts
       ,SUM(core + biweekly + maintenance) AS total_ppts
       ,SUM(core + 0.5*biweekly + 0.25*maintenance) AS core_equivalent_ppts
FROM   granular_coach_loads
GROUP BY coach_name
ORDER BY coach_name ASC
;



WITH cohort_subset AS (
    SELECT *
    FROM    bmh_prod_na.cohort
    WHERE   1=1
            AND cohort.name LIKE '%Solera%' -- This may not be necessary. Instead you should use all cohorts and then filter out demos and non-USA
            AND cohort.startdate BETWEEN '2019-01-01'::DATE AND '2020-08-31'::DATE
    ORDER BY cohort.startdate
    ),
     coach_mapping AS (
    SELECT cohortcoachmapping.cohortid
           ,cohortcoachmapping.transformuserid AS coach_id
       --    ,cohort_subset.startdate
    FROM    bmh_prod_na.cohortcoachmapping
            INNER JOIN  cohort_subset
            ON cohort_subset.id = cohortcoachmapping.cohortid
    WHERE   1=1
            AND cohortcoachmapping.deletedat ISNULL
    ),
     coach_name_mapping AS (
    SELECT fullname
           ,id  AS coach_id
           ,cohortid
    FROM    bmh_prod_na.transformuser
    WHERE   cohortid = 15
     ),
     most_recent_ppt_status AS (
     SELECT DISTINCT ON (transformuserid)
                        *
     FROM   bmh_prod_na.participantstatus
     ORDER BY transformuserid, updatedat DESC
     ),
     ppt_status AS (
     SELECT *
     FROM most_recent_ppt_status
     WHERE  statusid IN (3,6) -- keep ppts with "Active" and "NWCC" status in HC dashboard
     ),
     valid_tus AS (
     SELECT transformuser.id
            ,transformuser.cohortid
     FROM   bmh_prod_na.transformuser
            INNER JOIN ppt_status
            ON transformuser.id = ppt_status.transformuserid
     ),
     cohort_loads AS (
    SELECT DISTINCT ON (valid_tus.cohortid)
           cohortid
           ,COUNT(valid_tus.id) OVER(PARTITION BY cohortid) AS ppts
           ,cohort_subset.name AS cohort_name
           ,cohort_subset.startdate
    FROM valid_tus
         INNER JOIN cohort_subset
         ON valid_tus.cohortid = cohort_subset.id
  --  GROUP BY cohortid
     ),
     coach_loads AS (
         SELECT REPLACE(fullname, ' Coach', '') AS coach_name
                ,MAX(cohort_loads.startdate::DATE)    AS cohort_start_date
                ,MAX(cohort_name)
                ,NOW()::DATE - MAX(cohort_loads.startdate::DATE) AS program_day
                ,SUM(ppts)   AS ppts
         FROM   cohort_loads
                INNER JOIN coach_mapping
                ON cohort_loads.cohortid = coach_mapping.cohortid
                INNER JOIN coach_name_mapping
                ON coach_mapping.coach_id = coach_name_mapping.coach_id
         GROUP BY fullname, cohort_loads.cohortid
         ORDER by fullname
     ),
     granular_coach_loads AS (
         SELECT *
      ,CASE WHEN program_day <= 112 THEN ppts
                ELSE 0
       END AS core
      ,CASE WHEN program_day BETWEEN 113 AND 154 THEN ppts
                ELSE 0
       END AS biweekly
      ,CASE WHEN program_day > 154 THEN ppts
                ELSE 0
       END AS maintenance
        FROM    coach_loads
     ),
     breakdown AS (
         SELECT
              SUM(ppts)                                       AS ppts
              , SUM(core)                                       AS core
              , SUM(biweekly)                                   AS biweekly
              , SUM(maintenance)                                AS maintenance
              , SUM(core + 0.5 * biweekly + 0.25 * maintenance) AS core_equivalent_ppts
         FROM granular_coach_loads
         GROUP BY coach_name
     )
SELECT  *
        --PERCENTILE_DISC(0.5) WITHIN GROUP (ORDER BY ppts)
FROM coach_loads
;