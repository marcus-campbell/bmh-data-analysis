-- 2019-03-13
-- Laura wanted a total of all Spanish Solera PPTs we had ever enrolled, as well as a count of
-- the current number of active Spanish Solera PPTs

WITH spanish AS(
       SELECT  *
       FROM    bmh_prod_na.transformuser
       WHERE   transformuser.language = 'es')
    ,ps      AS(
       SELECT DISTINCT ON (transformuserid)
              transformuserid
             ,statusid
       FROM   bmh_prod_na.participantstatus
       ORDER BY transformuserid, createdat DESC
     )
SELECT  spanish.id
       ,ps.statusid
FROM  spanish
LEFT JOIN ps
ON spanish.id = ps.transformuserid
WHERE ps.statusid = 3 -- status id 3 corresponds to PPTs that were active during the last reporting period
;