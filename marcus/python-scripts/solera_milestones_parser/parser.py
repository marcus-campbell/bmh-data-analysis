import glob, os
import pandas as pd
import xlrd

#### Import raw data ####
path = os.getcwd()
raw_paid_files_path = os.path.join(path, 'raw_paid_files')
raw_paid_files = os.listdir(raw_paid_files_path)

print(raw_paid_files)

raw_paid_df = pd.DataFrame()
raw_pending_df = pd.DataFrame()

for f in raw_paid_files:
    print('Reading file: ', "'", f,"'", '...', sep='')
    paid_data = pd.read_excel(os.path.join(raw_paid_files_path, f), usecols=range(0, 12), sheet_name='Current')
    paid_data = paid_data.dropna(subset=['Participant Solera ID'])  # drop any rows with solera IDs; used to remove any formula rows that Solera's accountants added
    raw_paid_df = raw_paid_df.append(paid_data)

    pending_data = pd.read_excel(os.path.join(raw_paid_files_path, f), usecols=range(0, 9), sheet_name='Pending')
    pending_data = pending_data.dropna(subset=['Participant Solera ID'])
    raw_pending_df = raw_pending_df.append(pending_data)

# Cash Flow

paid_df_colnames = {'Participant Solera ID': 'solera_id',
                    'Solera Order Number': 'solera_order_id',
                    'Milestone Number': 'milestone_type',
                    'Milestone Cost': 'milestone_value',
                    'Date Partner Achieved': 'date_milestone_awarded',
                    'payment Date ': 'date_payment_received'}

paid_df = raw_paid_df.rename(columns=paid_df_colnames)  # rename columns

paid_df = paid_df[['solera_id', 'solera_order_id', 'milestone_type', 'milestone_value', 'date_milestone_awarded', 'date_payment_received']]  # subset columns

paid_df = paid_df.astype({'solera_id': int})
paid_df = paid_df.astype({'solera_id': str, 'milestone_type': str}) # coerce to string

paid_df['date_payment_received'] = pd.to_datetime(paid_df['date_payment_received']) # coerce to datetime64[ns]

paid_df['month_year'] = pd.to_datetime(paid_df['date_milestone_awarded']).dt.to_period('M')

paid_df = paid_df.drop_duplicates(subset=['solera_order_id'])  # remove duplicate solera_id-milestone pairs
paid_df = paid_df.sort_values(by=['date_payment_received'])  # sort rows

total_paid = paid_df['milestone_value'].sum()
paid_by_period = paid_df.groupby('month_year')['milestone_value'].sum()

print('Total Cash In: $', round(total_paid, ndigits=2), sep='')
print(paid_by_period)

# Revenue

pending_df_colnames = {'Participant Solera ID': 'solera_id',
                       'Solera Order Number': 'solera_order_id',
                       'Milestone Number': 'milestone_type',
                       'Payment Amount': 'milestone_value',
                       'Date Partner Achieved': 'date_milestone_awarded'}

pending_df = raw_pending_df.rename(columns=pending_df_colnames)  # rename columns

pending_df = pending_df[['solera_id', 'solera_order_id', 'milestone_type', 'milestone_value', 'date_milestone_awarded']]  # subset columns

pending_df = pending_df.astype({'solera_id': str, 'milestone_type': str}) # coerce to string

pending_df = pending_df.drop_duplicates(subset=['solera_order_id'])  # remove duplicate solera_id-milestone pairs

pending_df['month_year'] = pd.to_datetime(pending_df['date_milestone_awarded']).dt.to_period('M')

#total_pending = pending_df['milestone_value_x'].sum()  # sort rows

#pending_by_period = pending_df.groupby(['month_year_x', 'milestone_type'])['milestone_value_x'].sum()



# All recognized milestones

pending_df = pending_df.merge(paid_df, how='outer', indicator=True)
pending_df = pending_df[pending_df['_merge']=='left_only']

pending_df['status'] = 'pending'

paid_df['status'] = 'paid'

total_df = pd.concat([paid_df,pending_df],join='outer')

# TODO: Need to add filter to ensure milestones in pending_df do not occur in paid_df
#pending_not_in_paid_df = pending_df.merge(paid_df, how='outer', on=['solera_id', 'milestone_type'], indicator=True)
#pending_not_in_paid_df = pending_not_in_paid_df[pending_not_in_paid_df['_merge']=='left_only']

# All recognized within specific period

july01_july31_df = total_df[(total_df['date_milestone_awarded'] >= '2019-07-01') & (total_df['date_milestone_awarded'] <= '2019-07-31')]


july01_july31_df = july01_july31_df[['solera_id', 'milestone_type', 'solera_order_id', 'milestone_value', 'status', 'date_milestone_awarded', 'date_payment_received']]

total_revenue = july01_july31_df['milestone_value'].sum()
print('Total Revenue: $', round(total_revenue, ndigits=2), sep='')

#### Import BMH computed milestones ####

raw_bmh_milestones_path = os.path.join(path, 'raw_bmh_milestones')
raw_bmh_filename = os.listdir(raw_bmh_milestones_path)


raw_bmh_milestones_df = pd.read_csv(os.path.join(raw_bmh_milestones_path, raw_bmh_filename[0]))

bmh_milestones_df_colnames = {'transformuserid': 'BMH_id',
                              'soleraid': 'solera_id',
                              'milestonedate': 'date_BMH_achieved',
                              'milestonetype': 'milestone_type'}

bmh_milestones_df = raw_bmh_milestones_df.rename(columns=bmh_milestones_df_colnames) # rename columns
bmh_milestones_df = bmh_milestones_df[['BMH_id', 'solera_id', 'date_BMH_achieved', 'milestone_type']] # subset columns
bmh_milestones_df = bmh_milestones_df.astype({'solera_id': int})
bmh_milestones_df = bmh_milestones_df.astype({'solera_id': str, 'milestone_type': str})
bmh_milestones_df['date_BMH_achieved'] = pd.to_datetime(bmh_milestones_df['date_BMH_achieved'])
bmh_milestones_df['solera_order_id'] = bmh_milestones_df['solera_id'] + '-' + bmh_milestones_df['milestone_type']

july01_july31_df = july01_july31_df.astype({'solera_id': str, 'milestone_type': str})

output_df = pd.merge(bmh_milestones_df, july01_july31_df, how='outer', on=['solera_id', 'milestone_type'], indicator=True)
output_df2 = pd.merge(bmh_milestones_df, july01_july31_df, how='outer', indicator=True)

output_df3 = bmh_milestones_df.merge(july01_july31_df, on='solera_order_id', how='outer', indicator=True)

export_path = os.path.join(path, 'exported_data/july01_july31.csv')

july01_july31_df.to_csv(export_path, index=False)

