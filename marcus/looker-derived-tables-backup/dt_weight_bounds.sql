 SELECT wb.transformuserid,
    wb.start_weight,
    (wb.start_weight * 0.93) AS program_goal,
    COALESCE(wb.manual_goal_value, (wb.start_weight * 0.93)) AS participant_goal,
        CASE
            WHEN (wb.start_weight = (0)::numeric) THEN 'Not stepped on scale'::text
            ELSE 'Stepped on scale'::text
        END AS stepped_on_scale
   FROM ( SELECT tu.id AS transformuserid,
            COALESCE((manual_start.valuelbs)::numeric, first_weight.valuelbs) AS start_weight,
            first_weight.valuelbs AS first_value,
            (manual_start.valuelbs)::numeric AS manual_start_value,
            (manual_goal.valuelbs)::numeric AS manual_goal_value
           FROM (((bmh_prod.transformuser tu
             LEFT JOIN ( SELECT DISTINCT ON (weight.transformuserid) weight._sdc_batched_at,
                    weight._sdc_received_at,
                    weight._sdc_sequence,
                    weight._sdc_table_version,
                    weight.createdat,
                    weight.deletedat,
                    weight.deletedreason,
                    weight.id,
                    weight.source,
                    weight."timestamp",
                    weight.transformuserid,
                    weight.unit,
                    weight.updatedat,
                    weight.valuelbs
                   FROM bmh_prod.weight
                  WHERE ((weight.valuelbs IS NOT NULL) AND (weight.deletedat IS NULL))
                  ORDER BY weight.transformuserid, weight."timestamp") first_weight ON ((first_weight.transformuserid = tu.id)))
             LEFT JOIN ( SELECT DISTINCT ON (weightsummaryupdate.transformuserid) weightsummaryupdate._sdc_batched_at,
                    weightsummaryupdate._sdc_received_at,
                    weightsummaryupdate._sdc_sequence,
                    weightsummaryupdate._sdc_table_version,
                    weightsummaryupdate.createdat,
                    weightsummaryupdate.editorid,
                    weightsummaryupdate.id,
                    weightsummaryupdate.property,
                    weightsummaryupdate.transformuserid,
                    weightsummaryupdate.updatedat,
                    weightsummaryupdate.value,
                    weightsummaryupdate.valuelbs
                   FROM bmh_prod.weightsummaryupdate
                  WHERE (weightsummaryupdate.property = 'start'::text)
                  ORDER BY weightsummaryupdate.transformuserid, weightsummaryupdate.createdat DESC) manual_start ON ((manual_start.transformuserid = tu.id)))
             LEFT JOIN ( SELECT DISTINCT ON (weightsummaryupdate.transformuserid) weightsummaryupdate._sdc_batched_at,
                    weightsummaryupdate._sdc_received_at,
                    weightsummaryupdate._sdc_sequence,
                    weightsummaryupdate._sdc_table_version,
                    weightsummaryupdate.createdat,
                    weightsummaryupdate.editorid,
                    weightsummaryupdate.id,
                    weightsummaryupdate.property,
                    weightsummaryupdate.transformuserid,
                    weightsummaryupdate.updatedat,
                    weightsummaryupdate.value,
                    weightsummaryupdate.valuelbs
                   FROM bmh_prod.weightsummaryupdate
                  WHERE (weightsummaryupdate.property = 'goal'::text)
                  ORDER BY weightsummaryupdate.transformuserid, weightsummaryupdate.createdat DESC) manual_goal ON ((manual_goal.transformuserid = tu.id)))) wb
                  ;