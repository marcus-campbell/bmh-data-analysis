SELECT         

        "Partner ID" AS "Partner ID"
        --  , "Solera ID" AS "Solera ID"
        --  Adapt this query to work for non-Solera participants
        --  while still keeping backwards compatiable output w/ Solera ID if available
        --  by stripping all references to Solera ID from the data queries (to remove GROUP BY problems)
        --  and then join it back in at the top level here.
         , sm.soleraid AS "Solera ID"
         , "nw_blue_mesa" AS "nw_blue_mesa"
         , "Activity Date" AS "Activity Date"
         , "Weight lbs" AS "Weight lbs"
         , "Physical Activity Minutes" AS "Physical Activity Minutes"
         , "Articles Read" AS "Articles Read"
         , "Meals Logged" AS "Meals Logged"
         , "Group Interactions" AS "Group Interactions"
         , "Group Interaction Type" AS "Group Interaction Type"
         , "Coach Interactions" AS "Coach Interactions"
         , "Coach Interaction Type" AS "Coach Interaction Type"
         , "Coach Interaction Duration" AS "Coach Interaction Duration"
         , "Quiz/Survey's Completed" AS "Quiz/Survey's Completed"
         , "Lessons/Education Modules Completed" AS "Lessons/Education Modules Completed"
         , "App Points Obtained" AS "App Points Obtained"
         , "Weekly Video Watched (%)" AS "Weekly Video Watched (%)"
         , "Goal Behaviors Set" AS "Goal Behaviors Set"
         , "Coach Name" AS "Coach Name"



FROM (

       -- -----------------------------------------------------------------
       -- START WEIGHT is summarized onto the participant's start date, if we're reporting on that period
       -- -----------------------------------------------------------------
       SELECT
           '122'                                               AS "Partner ID"
-- -- --          , s.soleraid                                          AS "Solera ID"
         , w.transformuserid                                   AS "nw_blue_mesa"
         , to_char(w.timestamp, 'YYYYMMDD')                    AS "Activity Date"
         , to_char(min(w.valuelbs :: FLOAT), 'FM999999990.00') AS "Weight lbs"
         , ''                                                  AS "Physical Activity Minutes"
         , ''                                                  AS "Articles Read"
         , ''                                                  AS "Meals Logged"
         , ''                                                  AS "Group Interactions"
         , ''                                                  AS "Group Interaction Type"
         , ''                                                  AS "Coach Interactions"
         , ''                                                  AS "Coach Interaction Type"
         , ''                                                  AS "Coach Interaction Duration"
         , ''                                                  AS "Quiz/Survey's Completed"
         , ''                                                  AS "Lessons/Education Modules Completed"
         , ''                                                  AS "App Points Obtained"
         , ''                                                  AS "Weekly Video Watched (%)"
         , ''                                                  AS "Goal Behaviors Set"
         , ''                                                  AS "Coach Name"
       FROM (
              SELECT
                  tu.id            AS transformuserid
                , wb.start_weight  AS valuelbs
                , cohort.startdate AS timestamp
              FROM
                bmh_prod_na.transformuser tu
                LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                LEFT JOIN bmh_prod_na.view_weight_bounds wb ON wb.transformuserid = tu.id
              WHERE 1=1
                -- AND cohort.startdate :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                AND wb.start_weight NOTNULL
            ) w
--          LEFT JOIN bmh_prod_na.soleramap s ON s.transformuserid = w.transformuserid
         LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = w.transformuserid
         LEFT JOIN bmh_prod_na.cohort c ON c.id = tu.cohortid

       GROUP BY w.transformuserid, to_char(w.timestamp, 'YYYYMMDD')
       -- HAVING soleraid NOTNULL

       UNION

       -- -----------------------------------------------------------------
       -- WEIGHT readings after cohort start date
       -- -----------------------------------------------------------------
       SELECT
           '122'                                               AS "Partner ID"
-- --          , S.soleraid                                          AS "Solera ID"
         , W.transformuserid                                   AS "nw_blue_mesa"
         , REPLACE(LEFT(W.timestamp, 10), '-', '')             AS "Activity Date"
         , to_char(MIN(W.valuelbs :: FLOAT), 'FM999999990.00') AS "Weight lbs"
         , ''                                                  AS "Physical Activity Minutes"
         , ''                                                  AS "Articles Read"
         , ''                                                  AS "Meals Logged"
         , ''                                                  AS "Group Interactions"
         , ''                                                  AS "Group Interaction Type"
         , ''                                                  AS "Coach Interactions"
         , ''                                                  AS "Coach Interaction Type"
         , ''                                                  AS "Coach Interaction Duration"
         , ''                                                  AS "Quiz/Survey's Completed"
         , ''                                                  AS "Lessons/Education Modules Completed"
         , ''                                                  AS "App Points Obtained"
         , ''                                                  AS "Weekly Video Watched (%)"
         , ''                                                  AS "Goal Behaviors Set"
         , ''                                                  AS "Coach Name"
       FROM (
              SELECT
                tu.id AS transformuserid
                , W.valuelbs
                , W.timestamp
              FROM
                bmh_prod_na.transformuser tu
                LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                LEFT JOIN (
                            -- If the first weight ever for a participant is during the first program week,
                            -- in would appear above query result, duplicating the "Start Weight" information in the section above.
                            -- Therefore, identify that first weight IFF it's in a participant's first program week and hide
                            -- all of the weights on that day from the reporting list below to cleanly move that first weight
                            -- back to the cohort start date
                            SELECT
                              s.transformuserid
                              , s.valuelbs
                              , s.timestamp
                            FROM (
                                   -- Get the global first weight for a participant
                                   SELECT DISTINCT ON (tu.id)
                                     tu.id AS transformuserid
                                     , W.valuelbs
                                     , W.timestamp :: DATE
                                   FROM
                                     bmh_prod_na.transformuser tu
                                     LEFT JOIN bmh_prod_na.cohort cohort ON cohort.id = tu.cohortid
                                     LEFT JOIN bmh_prod_na.weight W ON W.transformuserid = tu.id
                                                                       AND W.deletedat ISNULL
                                   WHERE
                                     W.valuelbs NOTNULL AND W.deletedat ISNULL
                                   ORDER BY tu.id, timestamp ASC
                                 ) s
                              LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = transformuserid
                              LEFT JOIN bmh_prod_na.cohort ON cohort.id = tu.cohortid
                            -- And then only keep it if it happens to be in their first program week
                            WHERE
                              timestamp :: DATE BETWEEN cohort.startdate :: DATE
                              AND
                              cohort.startdate + INTERVAL '6 DAYS'

                          ) if_first_weight_in_first_period ON if_first_weight_in_first_period.transformuserid = tu.id
                LEFT JOIN bmh_prod_na.weight W ON W.transformuserid = tu.id
                                                  AND W.deletedat ISNULL
                                                  -- AND W.timestamp :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                                                  AND W.timestamp :: DATE > cohort.startdate :: DATE
                                                  AND W.timestamp :: DATE IS DISTINCT FROM
                                                      (if_first_weight_in_first_period.timestamp :: DATE)


              WHERE
                W.valuelbs NOTNULL AND W.deletedat ISNULL

            ) W
--          LEFT JOIN bmh_prod_na.soleramap S ON S.transformuserid = W.transformuserid
         LEFT JOIN bmh_prod_na.transformuser tu ON tu.id = W.transformuserid
         LEFT JOIN bmh_prod_na.cohort C ON C.id = tu.cohortid
       WHERE
         W.timestamp :: TIMESTAMP >= C.startdate :: TIMESTAMP
       GROUP BY W.transformuserid, REPLACE(LEFT(W.timestamp, 10), '-', '')
       -- HAVING soleraid NOTNULL

  -- -----------------------------------------------------------------
  -- MEALS
  -- -----------------------------------------------------------------
  UNION
  SELECT
    '122'                     AS "Partner ID",
--     soleraid                  AS "Solera ID",
    transformuserid           AS "nw_blue_mesa",
    to_char(date, 'YYYYMMDD') AS "Activity Date",
    ''                        AS "Weight lbs",
    ''                        AS "Physical Activity Minutes",
    ''                        AS "Articles Read",
    meals :: TEXT             AS "Meals Logged",
    ''                        AS "Group Interactions",
    ''                        AS "Group Interaction Type",
    ''                        AS "Coach Interactions",
    ''                        AS "Coach Interaction Type",
    ''                        AS "Coach Interaction Duration",
    ''                        AS "Quiz/Survey's Completed",
    ''                        AS "Lessons/Education Modules Completed",
    ''                        AS "App Points Obtained",
    ''                        AS "Weekly Video Watched (%)",
    ''                        AS "Goal Behaviors Set",
    ''                        AS "Coach Name"

  FROM (
         SELECT
--            sm.soleraid,
           tu.id          AS transformuserid,
           m.date :: DATE AS date,
           count(*)       AS meals
         FROM bmh_prod.transformuser tu
           LEFT JOIN bmh_prod.cohort cohort ON cohort.id = tu.cohortid
--            LEFT JOIN bmh_prod.soleramap sm ON sm.transformuserid = tu.id
           LEFT JOIN (
                       SELECT
                         transformuserid,
                         date :: DATE as date,
                         category
                       FROM
                         bmh_prod.meal
                       GROUP BY
                         transformuserid, date :: DATE, category
                     ) m ON m.transformuserid = tu.id

         WHERE 1=1
--         -- sm.soleraid NOTNULL
--               AND m.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
               AND m.date :: DATE >= cohort.startdate :: DATE
         --                 AND tu.id in (8240)
         GROUP BY tu.id, m.date
       ) m


  -- -----------------------------------------------------------------
  -- GROUP INTERACTIONS
  -- -----------------------------------------------------------------
  UNION
  SELECT
    '122'                             AS "Partner ID",
--     soleraid                          AS "Solera ID",
    transformuserid                   AS "nw_blue_mesa",
    to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date",
    ''                                AS "Weight lbs",
    ''                                AS "Physical Activity Minutes",
    ''                                AS "Articles Read",
    ''                                AS "Meals Logged",
    messages :: TEXT                  AS "Group Interactions",
    '6'                               AS "Group Interaction Type",
    ''                                AS "Coach Interactions",
    ''                                AS "Coach Interaction Type",
    ''                                AS "Coach Interaction Duration",
    ''                                AS "Quiz/Survey's Completed",
    ''                                AS "Lessons/Education Modules Completed",
    ''                                AS "App Points Obtained",
    ''                                AS "Weekly Video Watched (%)",
    ''                                AS "Goal Behaviors Set",
    ''                                AS "Coach Name"
  FROM (
         SELECT
           m.transformuserid,
--            sm.soleraid,
           left(m.date, 10) AS date,
           count(*)         AS messages
         FROM bmh_prod.message m
           LEFT JOIN bmh_prod.transformuser tu ON tu.id = m.transformuserid
           LEFT JOIN bmh_prod.cohort cohort ON cohort.id = tu.cohortid
           LEFT JOIN bmh_prod.conversation c ON c.id = m.conversationid
--            LEFT JOIN bmh_prod.soleramap sm ON sm.transformuserid = m.transformuserid
         WHERE
--         -- sm.soleraid NOTNULL AND
        c.distinct = FALSE
--               AND m.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
               AND m.date :: DATE >= cohort.startdate :: DATE
         --           and m.transformuserid in (6203)
         GROUP BY
           left(m.date, 10),
           m.transformuserid
       ) conversations

  -- -----------------------------------------------------------------
  -- COACH INTERACTIONS
  -- -----------------------------------------------------------------
  UNION
  SELECT
    '122'                             AS "Partner ID",
--     soleraid                          AS "Solera ID",
    transformuserid                   AS "nw_blue_mesa",
    to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date",
    ''                                AS "Weight lbs",
    ''                                AS "Physical Activity Minutes",
    ''                                AS "Articles Read",
    ''                                AS "Meals Logged",
    ''                                AS "Group Interactions",
    ''                                AS "Group Interaction Type",
    count :: TEXT                     AS "Coach Interactions",
    type :: TEXT                      AS "Coach Interaction Type",
    (count * time) :: TEXT            AS "Coach Interaction Duration",
    ''                                AS "Quiz/Survey's Completed",
    ''                                AS "Lessons/Education Modules Completed",
    ''                                AS "App Points Obtained",
    ''                                AS "Weekly Video Watched (%)",
    ''                                AS "Goal Behaviors Set",
    ''                                AS "Coach Name"
  FROM
    (
      SELECT
--         sm.soleraid AS soleraid,
        c.transformuserid,
        c.date :: DATE,
        c.type      AS type_str,
        CASE WHEN c.type = 'PHONE'
          THEN 1
        WHEN c.type = 'SMS'
          THEN 2
        WHEN c.type = 'CHAT'
          THEN 3
        WHEN c.type = 'EMAIL'
          THEN 6
        END         AS type,
        count(*)    AS count,
        CASE WHEN c.type = 'PHONE'
          THEN 30
        WHEN c.type = 'SMS'
          THEN 7
        WHEN c.type = 'CHAT'
          THEN 7
        WHEN c.type = 'EMAIL'
          THEN 15
        END         AS time
      FROM
        bmh_prod.contact c
        LEFT JOIN bmh_prod.transformuser tu ON tu.id = c.transformuserid
        LEFT JOIN bmh_prod.cohort cohort ON cohort.id = tu.cohortid
--         LEFT JOIN bmh_prod.soleramap sm ON sm.transformuserid = C.transformuserid
      WHERE c.response = TRUE
--       -- AND sm.soleraid NOTNULL
      AND c.type != 'CHAT'
--            AND c.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
            AND c.date :: DATE >= cohort.startdate :: DATE
      --         and c.transformuserid in (6203)
      GROUP BY c.transformuserid, c.date :: DATE, c.type
    ) interactions


  UNION
  SELECT
    '122'                             AS "Partner ID",
--     soleraid                          AS "Solera ID",
    transformuserid                   AS "nw_blue_mesa",
    to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date",
    ''                                AS "Weight lbs",
    ''                                AS "Physical Activity Minutes",
    ''                                AS "Articles Read",
    ''                                AS "Meals Logged",
    ''                                AS "Group Interactions",
    ''                                AS "Group Interaction Type",
    messages :: TEXT                  AS "Coach Interactions",
    '3'                               AS "Coach Interaction Type",
    (messages * 7) :: TEXT            AS "Coach Interaction Duration",
    ''                                AS "Quiz/Survey's Completed",
    ''                                AS "Lessons/Education Modules Completed",
    ''                                AS "App Points Obtained",
    ''                                AS "Weekly Video Watched (%)",
    ''                                AS "Goal Behaviors Set",
    ''                                AS "Coach Name"
  FROM (
         SELECT
           m.transformuserid,
--            sm.soleraid,
           left(m.date, 10) AS date,
           count(*)         AS messages
         FROM bmh_prod.message m
           LEFT JOIN bmh_prod.transformuser tu ON tu.id = m.transformuserid
           LEFT JOIN bmh_prod.cohort cohort ON cohort.id = tu.cohortid
           LEFT JOIN bmh_prod.conversation c ON c.id = m.conversationid
--            LEFT JOIN bmh_prod.soleramap sm ON sm.transformuserid = m.transformuserid
         WHERE
--         -- sm.soleraid NOTNULL AND
            c.distinct = TRUE
--               AND m.date :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
               AND m.date :: DATE >= cohort.startdate :: DATE
               AND c.iswithcoach = TRUE
         --             and m.transformuserid in (6203)
         GROUP BY
           left(m.date, 10),
           m.transformuserid
       ) conversations

  UNION
  SELECT
    '122'                             AS "Partner ID",
--     soleraid                          AS "Solera ID",
    transformuserid                   AS "nw_blue_mesa",
    to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date",
    ''                                AS "Weight lbs",
    ''                                AS "Physical Activity Minutes",
    ''                                AS "Articles Read",
    ''                                AS "Meals Logged",
    ''                                AS "Group Interactions",
    ''                                AS "Group Interaction Type",
    1 :: TEXT                         AS "Coach Interactions",
    1 :: TEXT                         AS "Coach Interaction Type",
    10 :: TEXT                        AS "Coach Interaction Duration",
    ''                                AS "Quiz/Survey's Completed",
    ''                                AS "Lessons/Education Modules Completed",
    ''                                AS "App Points Obtained",
    ''                                AS "Weekly Video Watched (%)",
    ''                                AS "Goal Behaviors Set",
    ''                                AS "Coach Name"
  FROM
    (
      SELECT
        tu.id                    AS transformuserid,
--         sm.soleraid,
        cohort.startdate :: DATE AS date
      FROM
        bmh_prod.transformuser tu
        LEFT JOIN bmh_prod.cohort cohort ON cohort.id = tu.cohortid
--         LEFT JOIN bmh_prod.soleramap sm ON sm.transformuserid = tu.id
      -- WHERE 1=1 AND
--        cohort.startdate :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE AND
--          -- sm.soleraid NOTNULL
    ) firstday



  -- -----------------------------------------------------------------
  -- LESSONS
  -- -----------------------------------------------------------------

  UNION
  SELECT
    '122'                             AS "Partner ID",
--     soleraid                          AS "Solera ID",
    transformuserid                   AS "nw_blue_mesa",
    to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date",
    ''                                AS "Weight lbs",
    ''                                AS "Physical Activity Minutes",
    ''                                AS "Articles Read",
    ''                                AS "Meals Logged",
    ''                                AS "Group Interactions",
    ''                                AS "Group Interaction Type",
    ''                                AS "Coach Interactions",
    ''                                AS "Coach Interaction Type",
    ''                                AS "Coach Interaction Duration",
    concat(quiz :: TEXT, '')          AS "Quiz/Survey's Completed",
    ''                                AS "Lessons/Education Modules Completed",
    ''                                AS "App Points Obtained",
    ''                                AS "Weekly Video Watched (%)",
    ''                                AS "Goal Behaviors Set",
    ''                                AS "Coach Name"
  FROM (
         SELECT
--            sm.soleraid                                    AS soleraid,
           tu.id                                          AS transformuserid,
           to_char(quizdoneat :: TIMESTAMP, 'YYYY-MM-DD') AS date,
           count(*)                                       AS quiz
         FROM
           bmh_prod.transformuser tu
--            LEFT JOIN bmh_prod.soleramap sm ON sm.transformuserid = tu.id
           LEFT JOIN bmh_prod.cohort cohort ON cohort.id = tu.cohortid
           LEFT JOIN bmh_prod.lessonprogress lp ON lp.transformuserid = tu.id
         WHERE 1 = 1
--         -- sm.soleraid NOTNULL
--               AND lp.quizdoneat :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
               AND lp.quizdoneat :: DATE >= cohort.startdate :: DATE
         GROUP BY tu.id, to_char(quizdoneat :: TIMESTAMP, 'YYYY-MM-DD')
       ) lp
  WHERE
  -- soleraid NOTNULL AND
  quiz NOTNULL

  UNION
  SELECT
    '122'                             AS "Partner ID",
--     soleraid                          AS "Solera ID",
    transformuserid                   AS "nw_blue_mesa",
    to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date",
    ''                                AS "Weight lbs",
    ''                                AS "Physical Activity Minutes",
    ''                                AS "Articles Read",
    ''                                AS "Meals Logged",
    ''                                AS "Group Interactions",
    ''                                AS "Group Interaction Type",
    ''                                AS "Coach Interactions",
    ''                                AS "Coach Interaction Type",
    ''                                AS "Coach Interaction Duration",
    ''                                AS "Quiz/Survey's Completed",
    concat(content :: TEXT, '')       AS "Lessons/Education Modules Completed",
    ''                                AS "App Points Obtained",
    ''                                AS "Weekly Video Watched (%)",
    ''                                AS "Goal Behaviors Set",
    ''                                AS "Coach Name"
  FROM (
         SELECT
--            sm.soleraid                                       AS soleraid,
           tu.id                                             AS transformuserid,
           to_char(contentdoneat :: TIMESTAMP, 'YYYY-MM-DD') AS date,
           count(*)                                          AS content
         FROM
           bmh_prod.transformuser tu
--            LEFT JOIN bmh_prod.soleramap sm ON sm.transformuserid = tu.id
           LEFT JOIN bmh_prod.cohort cohort ON cohort.id = tu.cohortid
           LEFT JOIN bmh_prod.lessonprogress lp ON lp.transformuserid = tu.id
         WHERE 1=1
--         -- AND sm.soleraid NOTNULL
--               AND lp.contentdoneat :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
                AND lp.contentdoneat :: DATE >= cohort.startdate :: DATE
         GROUP BY tu.id, to_char(contentdoneat :: TIMESTAMP, 'YYYY-MM-DD')
       ) lp
  WHERE
  -- soleraid NOTNULL AND
  content NOTNULL

  -- -----------------------------------------------------------------
  -- PHYSICAL ACTIVITY
  -- -----------------------------------------------------------------

  UNION
  SELECT
    '122'                             AS "Partner ID",
--     soleraid                          AS "Solera ID",
    transformuserid                   AS "nw_blue_mesa",
    to_char(date :: DATE, 'YYYYMMDD') AS "Activity Date",
    ''                                AS "Weight lbs",
    CASE WHEN minutes = 0
      THEN NULL
    ELSE minutes :: TEXT END          AS "Physical Activity Minutes",
    ''                                AS "Articles Read",
    ''                                AS "Meals Logged",
    ''                                AS "Group Interactions",
    ''                                AS "Group Interaction Type",
    ''                                AS "Coach Interactions",
    ''                                AS "Coach Interaction Type",
    ''                                AS "Coach Interaction Duration",
    ''                                AS "Quiz/Survey's Completed",
    ''                                AS "Lessons/Education Modules Completed",
    ''                                AS "App Points Obtained",
    ''                                AS "Weekly Video Watched (%)",
    ''                                AS "Goal Behaviors Set",
    ''                                AS "Coach Name"
  FROM (
         SELECT
--            sm.soleraid       AS soleraid,
           tu.id             AS transformuserid,
           date          AS date,
           ROUND(fad.activeminutes, 0) AS minutes
         FROM
           bmh_prod.transformuser tu
           LEFT JOIN bmh_prod.cohort cohort ON cohort.id = tu.cohortid
--            LEFT JOIN bmh_prod.soleramap sm ON sm.transformuserid = tu.id
           LEFT JOIN bmh_prod.activityday fad ON fad.transformuserid = tu.id
--  --        WHERE sm.soleraid NOTNULL
--               AND fad.date__st :: DATE BETWEEN :PERIOD_START :: DATE AND :PERIOD_END :: DATE
               AND fad.date :: DATE >= cohort.startdate :: DATE
               AND (fad.activeminutes) > 0
       ) am
  WHERE
--   -- am.soleraid NOTNULL AND
  am.minutes NOTNULL
) T

LEFT JOIN bmh_prod.soleramap sm ON sm.transformuserid = T."nw_blue_mesa"

      ;