      SELECT
        row_number() over() as pk,
        to_date(contacts."Created Time", 'YYYY-MM-DD'::text) AS ind_createdate,
        contacts."Lead Source",
        contacts."Solera Payer ID" AS solera_payer_id,
        contacts."Status",
        contacts."Mailing State" AS state,
        contacts."Mailing City" AS city,
        contacts."Mailing Zip" AS "Zip Code",
        COALESCE(contacts."Language", ''::text) AS "Language",
        contacts."Biological Sex",
        age((to_date(contacts."Created Time", 'YYYY-MM-DD'::text))::timestamp with time zone, to_timestamp(contacts."Date of Birth", 'YYYY-MM-DD'::text)) AS "AgeAtCreate",
        to_date(contacts."Committed Date", 'YYYY-MM-DD'::text) as commited_date,
        contacts."Solera ID",
        contacts."CDC Score",
        contacts."ADA Score",
        contacts."Participant ID"::integer,
        to_date(contacts."Cohort Start Date", 'YYYY-MM-DD'::text) as cohort_start_date,
        contacts."Disenrollment Reason",
        to_date(contacts."Disenrollment Date", 'YYYY-MM-DD'::text) as disenrollment_date,
        contacts.extract_date AS zoho_extract_date,
        'Enrolled'::text AS ind_type,
        1::integer as num_leads
       FROM (zoho.contacts
         JOIN reference.accounts ON (((accounts.accountname)::text = contacts."Lead Source")))
      WHERE (to_date(contacts."Created Time", 'YYYY-MM-DD'::text) > '2016-01-01'::date)
    UNION ALL
     SELECT
        (row_number() over()) * -1 as pk,
        to_date(leads."Created Time", 'YYYY-MM-DD'::text) AS ind_createdate,
        leads."Lead Source",
        leads."Solera Payer ID" AS solera_payer_id,
        mls.lead_status_summary AS "Status",
        leads."State" AS state,
        leads."City" AS city,
        leads."Zip Code",
        COALESCE(leads."Language", ''::text) AS "Language",
        leads."Biological Sex",
        age((to_date(leads."Created Time", 'YYYY-MM-DD'::text))::timestamp with time zone, to_timestamp(leads."Birthdate", 'YYYY-MM-DD'::text)) AS "AgeAtCreate",
        to_date(leads."Committed Date", 'YYYY-MM-DD'::text) as commited_date,
        leads."Solera ID",
        leads."CDC Score",
        leads."ADA Score",
        NULL::integer AS "Participant ID",
        NULL::date AS  cohort_start_date,
        NULL::text AS "Disenrollment Reason",
        NULL::date AS disenrollment_date,
        leads.extract_date AS zoho_extract_date,
        'Lead'::text AS ind_type,
        1::integer as num_leads
       FROM ((zoho.leads
         JOIN reference.accounts ON (((accounts.accountname)::text = leads."Lead Source")))
         LEFT JOIN reference.mapping_lead_status mls ON (((mls.first_character)::text = "left"(leads."Lead Status", 1))))
      WHERE (to_date(leads."Created Time", 'YYYY-MM-DD'::text) > '2016-01-01'::date)
            ;