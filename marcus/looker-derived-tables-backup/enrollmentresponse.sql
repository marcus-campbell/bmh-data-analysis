SELECT
CAST(se.nw_partner_id as INT) as transformuserid,
enrolled,
createdat,
id,
CAST(enrolled_date as DATE),
CAST(start_date as DATE),
solera_id,
partner_id,
filename,
message
FROM solera.enrollmentresponse as se
WHERE se.enrolled = 'Y' AND se.message = 'Success'
 ;