with daily_adherence as (
        SELECT user_id, reporting_date, case when count(distinct weight.createdat) >= 1 then 1 else 0 end as weight_complete,
        case when count(distinct lp.contentdoneat) >= 1 then 1 else 0 end as lesson_complete
        FROM ${date_table.SQL_TABLE_NAME} AS users
        LEFT JOIN bmh_prod_sa.weight as weight on users.user_id = weight.transformuserid and date_trunc('day',weight.createdat) = users.reporting_date
        LEFT JOIN bmh_prod_sa.lessonprogress as lp on users.user_id = lp.transformuserid and date_trunc('day',cast(lp.contentdoneat as timestamp)) = users.reporting_date
        GROUP BY 1,2
      )
      select user_id, reporting_date, weight_complete, lesson_complete,
      sum(weight_complete) over(partition by user_id order by reporting_date asc rows between 6 preceding and current row) as  weight_measurements_in_rolling_week,
      sum(lesson_complete) over(partition by user_id order by reporting_date asc rows between 6 preceding and current row) as  lessons_complete_in_rolling_week
      from daily_adherence
      order by 1,2
       ;