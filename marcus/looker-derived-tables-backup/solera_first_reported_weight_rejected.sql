SELECT DISTINCT ON (transformuserid) *

FROM (SELECT nw_partner_id::BIGINT AS transformuserid, activity_date::DATE, weightlbs, message AS solera_message,

        split_part(message, ' - ', 1) AS solera_message_category

      FROM solera.engagementresponse

      WHERE weightlbs NOTNULL AND

        weightlbs IS DISTINCT FROM '' AND

        nw_partner_id NOTNULL AND

        nw_partner_id IS DISTINCT FROM '') AS er

WHERE solera_message_category IN ('Invalid Rule', 'Invalid Weight', 'NULL Value') AND

  WEIGHTLBS::NUMERIC > 90.0

ORDER BY transformuserid DESC, activity_date ASC

;