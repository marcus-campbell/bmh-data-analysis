SELECT
    participants.soleraid,
    participants.transformuserid,
    --     participants.fullname,
    participants.cohortid   AS cohort_id,
    participants.cohortname AS cohort_name,
    coach.fullname          AS coach_name,
    participants.startdate,
    --   participants.milestone_2_engaged_weeks,
    --   participants.milestone_3_engaged_weeks,
    CASE WHEN milestone_2_engaged_weeks >= 2
      THEN 'YES'
    ELSE 'NO' END           AS milestone_2_met,
    -- second meaningful engagement date
    CASE WHEN milestone_2_engaged_weeks >= 2
      THEN milestone_2_engagement_dates[2]
    ELSE NULL END           AS milestone_2_date,
    (participants.startdate + INTERVAL '4 WEEKS') :: DATE
                            AS milestone_2_end_date,
    CASE WHEN milestone_3_engaged_weeks >= 2
      THEN 'YES'
    ELSE 'NO' END           AS milestone_3_met,
    -- second meaningful engagement date
    CASE WHEN milestone_3_engaged_weeks >= 2
      THEN milestone_3_engagement_dates[2]
    ELSE NULL END           AS milestone_3_date,
    (participants.startdate + INTERVAL '9 WEEKS') :: DATE
                            AS milestone_3_end_date,

    lastweight.pct_lost     AS last_weight_reading_loss_pct,
    lastweight.date         AS last_weight_reading_date,
    CASE WHEN m4.pct_lost >= 5.0
      THEN 'YES'
    ELSE 'NO' END           AS milestone_4_met,
    m4.pct_lost             AS m4_weight_loss,
    m4.date                 AS m4_weight_date,
    (participants.startdate + INTERVAL '52 WEEKS') :: DATE
                            AS milestone_4_end_date,
    minweight.pct_lost      AS most_pct_lost,
    minweight.date          AS most_pct_date

  FROM
    (
      SELECT
        engagement_weekly.soleraid,
        engagement_weekly.transformuserid,
        engagement_weekly.fullname,
        engagement_weekly.cohortid,
        c.name                                                                 AS cohortname,
        c.startdate :: DATE,
        count(*)                                                               AS week_records,
        count(*)
          FILTER (WHERE week_series BETWEEN 1 AND 4 AND engagement_score >= 2) AS milestone_2_engaged_weeks,
        count(*)
          FILTER (WHERE week_series BETWEEN 5 AND 9 AND engagement_score >= 2) AS milestone_3_engaged_weeks,
        -- second criteria date - the one that fulfills meaningful engagement conditions
        array_agg(criteria_dates[2] ORDER BY criteria_dates[2])
          FILTER (WHERE week_series BETWEEN 1 AND 4 AND engagement_score >= 2) AS milestone_2_engagement_dates,
        array_agg(criteria_dates[2] ORDER BY criteria_dates[2])
          FILTER (WHERE week_series BETWEEN 5 AND 9 AND engagement_score >= 2) AS milestone_3_engagement_dates
      FROM (
              SELECT
               view_criteria_weekly_tmp.soleraid,
               view_criteria_weekly_tmp.transformuserid,
               view_criteria_weekly_tmp.fullname,
               view_criteria_weekly_tmp.cohortid,
               view_criteria_weekly_tmp.week_series,
               view_criteria_weekly_tmp.quiz_criteria + view_criteria_weekly_tmp.group_criteria + view_criteria_weekly_tmp.contact_criteria +
               view_criteria_weekly_tmp.meal_criteria + view_criteria_weekly_tmp.weighin_criteria AS engagement_score,
               -- sorted list of all successful criteria dates (filtering out null dates)
               array(
                 SELECT unnest(
                   array_remove(
                       ARRAY[
                         view_criteria_weekly_tmp.quiz_criteria_date, view_criteria_weekly_tmp.group_criteria_date,
                         view_criteria_weekly_tmp.contact_criteria_date, view_criteria_weekly_tmp.meal_criteria_date,
                         view_criteria_weekly_tmp.weighin_criteria_date
                       ], NULL)
                 ) AS x
                 ORDER BY x
               )                                                                                  AS criteria_dates
               FROM  ${dt_criteria_weekly.SQL_TABLE_NAME} view_criteria_weekly_tmp
--              FROM view_criteria_weekly_tmp
              WHERE view_criteria_weekly_tmp.week_series BETWEEN 1 AND 9
           ) engagement_weekly
        LEFT JOIN bmh_prod.cohort c ON c.id = engagement_weekly.cohortid
      GROUP BY engagement_weekly.soleraid, engagement_weekly.transformuserid,
        engagement_weekly.cohortid, engagement_weekly.fullname, c.name, c.startdate
      ORDER BY cohortid, fullname
    ) participants
    LEFT JOIN (
                SELECT
                  ccm.cohortid,
                  ccm.transformuserid
                FROM
                  (
                    SELECT
                      min(id) AS id,
                      cohortid
                    FROM bmh_prod.cohortcoachmapping
                    WHERE deletedat ISNULL
                    GROUP BY cohortid
                  ) primary_coach_mapping
                  LEFT JOIN bmh_prod.cohortcoachmapping ccm ON ccm.id = primary_coach_mapping.id
              ) cohortcoachmapping ON cohortcoachmapping.cohortid = participants.cohortid
    LEFT JOIN bmh_prod.transformuser coach ON coach.id = cohortcoachmapping.transformuserid


    -------------------------------------------------------------------------------------------
    --   M4
    -------------------------------------------------------------------------------------------
    LEFT JOIN (
                SELECT
                  m4weight.transformuserid,
                  m4weight.date            AS date,
                  round(m4weight.valuelbs, 2) AS VALUE,
                  CASE WHEN m4weight.start_weight > 0
                    THEN
                      round(((m4weight.start_weight - m4weight.valuelbs) / m4weight.start_weight) * 100, 2)
                  ELSE NULL END
                                           AS pct_lost
                FROM (
                     SELECT
                       lost5pct.transformuserid
                       , lost5pct.date
                       , lost5pct.valuelbs AS valuelbs
                       , lost5pct.start_weight
                     FROM (
                            SELECT DISTINCT ON (bw.transformuserid)
                              bw.transformuserid
                              , wb.start_weight           AS start_weight
                              , cohort.startdate
                              , bw.timestamp::date AS date
                            , bw.valuelbs
                            FROM bmh_prod.weight bw
                              LEFT JOIN ${dt_weight_bounds.SQL_TABLE_NAME} wb ON wb.transformuserid = bw.transformuserid
                              LEFT JOIN bmh_prod.transformuser tu ON tu.id = bw.transformuserid
                              LEFT JOIN bmh_prod.cohort ON cohort.id = tu.cohortid
                            WHERE
                              valuelbs <= (start_weight * 0.95)
                              AND bw.timestamp :: TIMESTAMP > (cohort.startdate + INTERVAL '9 WEEKS')
                              AND bw.timestamp :: TIMESTAMP < (cohort.startdate + INTERVAL '52 WEEKS')
                              AND bw.deletedat ISNULL
                            ORDER BY bw.transformuserid, bw.timestamp ASC
                          ) lost5pct
                   ) m4weight
              ) m4 ON m4.transformuserid = participants.transformuserid


    -------------------------------------------------------------------------------------------
    --   MIN WEIGHT
    -------------------------------------------------------------------------------------------
    LEFT JOIN (
                SELECT
                  minweight.transformuserid
                  , minweight.timestamp::date as date
                  , round(minweight.valuelbs, 2) AS valuelbs
                  , CASE WHEN wb.start_weight > 0
                  THEN round(((wb.start_weight - minweight.valuelbs) / wb.start_weight) * 100, 2)
                    ELSE NULL END
                                                 AS pct_lost
                FROM
                  (
                    SELECT DISTINCT ON (w.transformuserid)
                    *
                    FROM bmh_prod.weight w
                    WHERE w.deletedat ISNULL
                    ORDER BY w.transformuserid, w.valuelbs asc
                  ) minweight
                LEFT JOIN ${dt_weight_bounds.SQL_TABLE_NAME} wb on wb.transformuserid = minweight.transformuserid
              ) minweight ON minweight.transformuserid = participants.transformuserid


    -------------------------------------------------------------------------------------------
    --   MOST RECENT WEIGHT
    -------------------------------------------------------------------------------------------

    LEFT JOIN (
          SELECT
            lastvalue.transformuserid
            , lastvalue.date
            , lastvalue.valuelbs
            , CASE WHEN lastvalue.start_weight > 0
            THEN
              round(((lastvalue.start_weight - lastvalue.valuelbs) / lastvalue.start_weight) * 100, 2)
              ELSE NULL END AS pct_lost
          FROM
            (
              SELECT
                lastweight.transformuserid
                , lastweight.timestamp::date    AS date
                , lastweight.valuelbs AS valuelbs
                , wb.start_weight
              FROM (
                     SELECT DISTINCT ON (bw.transformuserid)
                       bw.transformuserid
                       , bw.id
                       , bw.valuelbs
                       , bw.timestamp
                     FROM bmh_prod.weight bw
                     WHERE
                       bw.deletedat ISNULL
                     ORDER BY bw.transformuserid, bw.timestamp DESC
                   ) lastweight
                LEFT JOIN ${dt_weight_bounds.SQL_TABLE_NAME} wb ON wb.transformuserid = lastweight.transformuserid
            ) lastvalue
              ) lastweight ON lastweight.transformuserid = participants.transformuserid

      ;