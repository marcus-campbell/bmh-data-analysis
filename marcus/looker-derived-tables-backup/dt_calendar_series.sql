      WITH dateseries AS (
          SELECT
            date :: DATE
            , EXTRACT(MONTH FROM date)                        AS month
            , EXTRACT(YEAR FROM date)                         AS year
            , EXTRACT(DOW FROM date)                          AS day_of_week
            -- ISO weeks start on Mondays
            , EXTRACT(WEEK FROM date)                         AS week_iso
            , EXTRACT(ISOYEAR FROM date)                      AS week_year_iso
            -- BMH weeks start on Sundays, so bring back tomorrow's (Monday start) week
            , EXTRACT(WEEK FROM (date + INTERVAL '1 day'))    AS week
            , EXTRACT(ISOYEAR FROM (date + INTERVAL '1 day')) AS week_year
          FROM
                generate_series('2016-01-01' :: DATE, now(), '1 day' :: INTERVAL) AS date
      )
      SELECT
        *
        , format('%s_%s', week_year, TO_CHAR(week, '00'))         AS week_name
        , format('%s_%s', week_year_iso, TO_CHAR(week_iso, '00')) AS week_name_iso
      FROM
        dateseries
      ORDER BY date ASC
      ;