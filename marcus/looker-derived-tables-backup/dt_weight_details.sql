WITH
        weight_to_id_week_legacy AS (
        -- Written by Mark, Looker implementation contract dev
          SELECT DISTINCT
            dt_criteria_weekly.transformuserid
            , dt_criteria_weekly.week_series                                                      AS "week_series"
            , min(weight.valuelbs)
              OVER (
                PARTITION BY dt_criteria_weekly.transformuserid, dt_criteria_weekly.week_series ) AS "min_weekly_weight"

          FROM ${dt_criteria_weekly.SQL_TABLE_NAME} AS dt_criteria_weekly

                LEFT JOIN bmh_prod.weight AS weight ON dt_criteria_weekly.transformuserid = weight.transformuserid
                                                       AND (DATE(dt_criteria_weekly.week_start )) <= weight.date::date
                                                       AND (DATE(dt_criteria_weekly.week_end )) >= weight.date::date
          WHERE weight.deletedreason IS NULL  -- 20190211 THis is wrong.  deletedreason is metadata and not indicitive.  Must use deletedat.
      ),
      weight_to_id_week as (
      -- Written 20190211 by EvanWillms to be more correct
            select DISTINCT ON (
            dt_criteria_weekly.transformuserid,
            dt_criteria_weekly.week_series
            )

            dt_criteria_weekly.transformuserid  as "transformuserid",
            dt_criteria_weekly.week_series      AS "week_series",
            weight.valuelbs                     as "min_weekly_weight"

                      FROM ${dt_criteria_weekly.SQL_TABLE_NAME} AS dt_criteria_weekly
                      LEFT JOIN bmh_prod.weight AS weight ON dt_criteria_weekly.transformuserid = weight.transformuserid
                                                                   AND (DATE(dt_criteria_weekly.week_start )) <= weight.date::date
                                                                   AND (DATE(dt_criteria_weekly.week_end )) >= weight.date::date
                                                                   AND weight.deletedat ISNULL

                    ORDER BY dt_criteria_weekly.transformuserid asc, dt_criteria_weekly.week_series asc, weight.valuelbs asc


      )

    SELECT
        row_number()
        OVER ()                                              AS PK
      , weight_to_id_week.transformuserid
      , weight_to_id_week.week_series
      , weight_to_id_week.min_weekly_weight
      , dt_weight_bounds.start_weight :: NUMERIC                       AS "start_weight"
      --              last_weight,
      , min(weight_to_id_week.min_weekly_weight)
        OVER (
          PARTITION BY weight_to_id_week.transformuserid
          ORDER BY weight_to_id_week.week_series
          ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) ::NUMERIC AS min_weight_tocurrent_week

      , split_part(
            max(
                weight_to_id_week.week_series || '|' || weight_to_id_week.min_weekly_weight
            )
            OVER (
              PARTITION BY weight_to_id_week.transformuserid
              ORDER BY weight_to_id_week.week_series
              ),
            '|', 2) :: NUMERIC                                         AS last_weight
    -- This stupid SQL trick brought to you by: https://discourse.looker.com/t/only-measuring-the-latest-values/6719
    FROM weight_to_id_week

      LEFT JOIN ${dt_weight_bounds.SQL_TABLE_NAME} AS dt_weight_bounds
        ON weight_to_id_week.transformuserid = dt_weight_bounds.transformuserid

       ;