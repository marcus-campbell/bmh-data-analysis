    SELECT *
        FROM
        (SELECT
         tu.id as transformuserid,
         week,
         c.startdate,

     CASE
    WHEN
    c.startdate is NULL THEN NULL
    WHEN
     c.startdate :: TIMESTAMP + (week || 'weeks')::INTERVAL
    < now() - ('1 week'):: INTERVAL THEN 'PAST'
    WHEN c.startdate :: TIMESTAMP + (week || 'weeks')::INTERVAL
    > now()::TIMESTAMP THEN 'FUTURE'
    ELSE 'PRESENT' END AS period
    FROM bmh_prod.transformuser tu
        JOIN bmh_prod.cohort c ON c.id = tu.cohortid
        CROSS JOIN generate_series(0,56) as week) a
    ;