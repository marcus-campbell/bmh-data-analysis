SELECT DISTINCT ON (transformuserid) *

FROM (SELECT nw_partner_id::BIGINT AS transformuserid, activity_date::DATE,
        coachinteractions, message AS solera_message,
        split_part(message, ' - ', 1) AS solera_message_category
      FROM solera.engagementresponse

      WHERE coachinteractions NOTNULL AND
        coachinteractions IS DISTINCT FROM '' AND
        nw_partner_id NOTNULL AND
        nw_partner_id IS DISTINCT FROM '') AS er

WHERE solera_message_category IS DISTINCT FROM 'Success'

ORDER BY transformuserid DESC, activity_date ASC

    ;