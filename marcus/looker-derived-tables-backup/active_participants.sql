WITH
        active_week_ids as (
         (SELECT  DISTINCT ON(tu.id) tu.id as transformuserid, c.startdate, ps.createdat, ps.statusid
        FROM bmh_prod.transformuser tu
             LEFT JOIN bmh_prod.cohort c ON c.id = tu.cohortid
             LEFT JOIN (
              SELECT
                  ps.id,ps.transformuserid,ps.createdat, ps.statusid
                FROM bmh_prod.participantstatus ps
              UNION
              SELECT -1 as id, tu.id as transformuserid,c.startdate as createdat, 3 as status_id from bmh_prod.cohort c
              JOIN transformuser tu ON tu.cohortid = c.id) ps ON ps.transformuserid = tu.id

          WHERE c.startdate :: TIMESTAMP IS NOT NULL
          AND
            (ps.createdat is NULL
            OR
            ps.createdat <
            (c.startdate :: TIMESTAMP + INTERVAL {% parameter number_of_results %}):: TIMESTAMP)

            ORDER BY tu.id, ps.createdat DESC)



  )
SELECT * from active_week_ids;