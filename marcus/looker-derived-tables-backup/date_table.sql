SELECT users.id as user_id, generate_series as reporting_date
      FROM generate_series('2017-01-01 00:00'::timestamp,now(), '1 day')
      CROSS JOIN bmh_prod_sa.transformuser as users
      GROUP BY 1,2
      ;