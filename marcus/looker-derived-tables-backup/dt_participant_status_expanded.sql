SELECT
  *
  , CASE WHEN DATE_PART('day', expanded_date - cohort_startdate) < 0 then NULL
    ELSE DATE_PART('day', expanded_date - cohort_startdate)  end as program_day
  , CASE
    WHEN (expanded_statusid = 3 OR expanded_statusid = 6) AND expanded_date >= cohort_startdate + INTERVAL '1 year'
      THEN 33
    WHEN (expanded_statusid = 3 OR expanded_statusid = 6) AND expanded_date >= cohort_startdate + INTERVAL '22 weeks'
      THEN 32
    WHEN (expanded_statusid = 3 OR expanded_statusid = 6) AND expanded_date >= cohort_startdate + INTERVAL '16 weeks'
      THEN 31
    WHEN (expanded_statusid = 3 OR expanded_statusid = 6)
      THEN 30
    ELSE expanded_statusid
    END AS expanded_statusid_granular
FROM (
       SELECT
           concat(transformuserid, '-', expanded_date) AS id
         , s.*
         , first_value(statusid)
           OVER (
             PARTITION BY transformuserid, value_partition
             ORDER BY expanded_date
             )                                         AS expanded_statusid
       FROM (
              SELECT DISTINCT ON (tu.id, expanded_date)
                  tu.id                             AS transformuserid
                , c.startdate                       AS cohort_startdate
                , expanded_date :: DATE
                , ps.id                             AS ps_id
                , ps.statusid
                , sum(CASE WHEN ps.id IS NULL
                THEN 0
                      ELSE 1 END)
                  OVER (
                    PARTITION BY tu.id
                    ORDER BY tu.id, expanded_date ) AS value_partition
              FROM
                bmh_prod.transformuser tu
                LEFT JOIN bmh_prod.cohort c ON c.id = tu.cohortid
                CROSS JOIN generate_series('2016-01-01' :: DATE, now(), '1 day' :: INTERVAL) AS expanded_date
                LEFT JOIN (
                            SELECT
                              ps.id
                              , ps.transformuserid
                              , ps.statusid
                              , ps.createdat
                            FROM
                              bmh_prod.participantstatus ps

                            UNION

                            SELECT
                                -1          AS id
                              , tu.id       AS transformuserid
                              , 3           AS statusid
                              , c.startdate AS createdat
                            FROM bmh_prod.transformuser tu
                              LEFT JOIN bmh_prod.cohort c ON c.id = tu.cohortid
                            WHERE
                              c.startdate NOTNULL

                          ) ps
                  ON ps.transformuserid = tu.id AND ps.createdat :: DATE = expanded_date :: DATE

              WHERE
                1 = 1
                -- AND tu.id IN (12443, 10682)
              ORDER BY tu.id, expanded_date, ps.createdat DESC
            ) s
     ) s2
WHERE
  1 = 1
  AND expanded_statusid NOTNULL
      ;