with
      cdc_status as
      (
        SELECT tu.id                                            AS transformuserid,
               tu.fullname,
               tu.status,
               tu.cohortid,
               c.name                                           AS cohortname,
               c.startdate,
               coach.fullname                                   AS coachname,
               date_part('year' :: text, (c.startdate) :: date) AS startyear,
               CASE
                 WHEN (c.version = ANY (ARRAY[(2)::bigint, (3)::bigint])) THEN 'en' :: text
                 WHEN (c.version = 4) THEN 'es' :: text
                 ELSE NULL :: text
                   END                                          AS cohort_lang
        FROM (((bmh_prod.transformuser tu
            LEFT JOIN cohort c ON ((c.id = tu.cohortid)))
            LEFT JOIN (SELECT ccm_1.transformuserid, ccm_1.cohortid
                       FROM ((SELECT min(cohortcoachmapping.id) AS id, cohortcoachmapping.cohortid
                              FROM bmh_prod.cohortcoachmapping
                              WHERE (cohortcoachmapping.deletedat IS NULL)
                              GROUP BY cohortcoachmapping.cohortid) primary_coachmapping
                           LEFT JOIN cohortcoachmapping ccm_1 ON ((ccm_1.id = primary_coachmapping.id)))) ccm ON ((
          ccm.cohortid = c.id)))
            LEFT JOIN bmh_prod.transformuser coach ON ((coach.id = ccm.transformuserid)))
        WHERE ((tu.cohortid <> ALL
                (ARRAY[(1)::bigint, (2)::bigint, (3)::bigint, (4)::bigint, (5)::bigint, (6)::bigint, (7)::bigint,
                       (8)::bigint, (9)::bigint, (10)::bigint, (15)::bigint, (16)::bigint, (222)::bigint, (354)::bigint,
                       (355)::bigint, (233)::bigint, (353)::bigint, (222)::bigint, (518)::bigint])) AND
               (NOT (tu.id IN (SELECT DISTINCT participantstatus.transformuserid
                               FROM bmh_prod.participantstatus
                               WHERE (participantstatus.statusid = 6)))) AND
               (tu.id <> ALL (ARRAY[(6090)::bigint, (6137)::bigint, (6089)::bigint, (6084)::bigint])))
      ),

      cdc_lesson_weeks AS
      (  SELECT
          lp.transformuserid
          , lp.contentdoneat :: date                                                   AS date
          , date_part('week' :: TEXT, lp.contentdoneat :: TIMESTAMP WITHOUT TIME ZONE) AS yearweek
          , date_part('year' :: TEXT, lp.contentdoneat :: TIMESTAMP WITHOUT TIME ZONE) AS year
          , lp.weeknum                                                                 AS lesson_number
        FROM bmh_prod.lessonprogress lp
        WHERE
          lp.contentdoneat IS NOT NULL
      ),

      cdc_checkin_weeks AS
      (  SELECT
          contact.transformuserid
          , min(contact.date) :: DATE                                              AS date
          , date_part('week' :: TEXT, contact.date :: TIMESTAMP WITHOUT TIME ZONE) AS yearweek
          , date_part('year' :: TEXT, contact.date :: TIMESTAMP WITHOUT TIME ZONE) AS year
        FROM bmh_prod.contact
        WHERE
          contact.response = TRUE
        GROUP BY contact.transformuserid, (date_part('week' :: TEXT, contact.date :: TIMESTAMP WITHOUT TIME ZONE)),
          (date_part('year' :: TEXT, contact.date :: TIMESTAMP WITHOUT TIME ZONE))
      ),

      cdc_first_session AS
      (  SELECT
          all_weeks.transformuserid
          , min(all_weeks.date) AS date
        FROM (SELECT
                  cdc_lesson_weeks.transformuserid
                , cdc_lesson_weeks.date
              FROM cdc_lesson_weeks
              UNION
              SELECT
                  cdc_checkin_weeks.transformuserid
                , cdc_checkin_weeks.date
              FROM cdc_checkin_weeks) all_weeks
          LEFT JOIN cdc_status cstat ON cstat.transformuserid = all_weeks.transformuserid
        WHERE
          all_weeks.date > cstat.startdate
        GROUP BY all_weeks.transformuserid
      ),

      cdc_lesson_sessions AS
      (  SELECT
          lw.transformuserid
          , lw.date
          , lw.yearweek
          , lw.year
          , lw.lesson_number
          , 'core' :: TEXT AS type
        FROM cdc_lesson_weeks lw
          LEFT JOIN cdc_first_session fs ON fs.transformuserid = lw.transformuserid
        WHERE
          lw.lesson_number <= 20 AND lw.date <= (fs.date + '6 mons' :: INTERVAL)
        UNION
        -- Maintinace lessons are valid anytime after they're available
        SELECT
          lw.transformuserid
          , lw.date
          , lw.yearweek
          , lw.year
          , lw.lesson_number
          , 'maintenance' :: TEXT AS type
        FROM cdc_lesson_weeks lw
          LEFT JOIN cdc_first_session fs ON fs.transformuserid = lw.transformuserid
        WHERE
          lw.lesson_number > 20
      )

      SELECT
          row_number()
          OVER () AS id
          , transformuserid
          , source
          , date
          , yearweek
          , year
          , type
          , lesson_number
          , cdc_type
          , cdc_session_id
        FROM (
               SELECT
                 *
                 , CASE
                   WHEN week_count > 1 AND type = 'core'
                     THEN 'MU'
                   WHEN type = 'core'
                     THEN 'C'
                   ELSE 'CM'
                   END               AS cdc_type
                 , CASE
                   WHEN type = 'core'
                     THEN session_count
                   ELSE 99 END       AS cdc_session_id
                 , row_number()
                   OVER (
                     PARTITION BY transformuserid, period
                     ORDER BY date ) AS period_number
               FROM
                 (
                   SELECT
                     s.*
                     , row_number()
                       OVER (
                         PARTITION BY s.transformuserid, year, yearweek
                         ORDER BY s.transformuserid, s.date DESC ) AS week_count
                     , row_number()
                       OVER (
                         PARTITION BY s.transformuserid
                         ORDER BY s.transformuserid, s.date )      AS session_count
                     , CASE WHEN s.date < (fs.date + INTERVAL '6 months')
                     THEN 'lte_6mo'
                       WHEN s.date BETWEEN (fs.date + INTERVAL '6 months') AND (fs.date + INTERVAL '9 months')
                         THEN 'btw_7mo_9mo'
                       WHEN s.date BETWEEN (fs.date + INTERVAL '9 months') AND (fs.date + INTERVAL '12 months')
                         THEN 'btw_10mo_12mo'
                       WHEN s.date > (fs.date + INTERVAL '12 months')
                         THEN 'gte_12mo'
                       END                                         AS period
                   FROM
                     (

                       SELECT
                           ls.transformuserid :: INTEGER AS transformuserid
                         , ls.source :: TEXT             AS source
                         , ls.date
                         , ls.yearweek :: INTEGER        AS yearweek
                         , ls.year :: INTEGER            AS year
                         , ls.type
                         , ls.lesson_number
                       FROM (
                              SELECT
                                cdc_lesson_sessions.transformuserid
                                , 'lesson'                                                                                                        AS source
                                , cdc_lesson_sessions.date
                                , cdc_lesson_sessions.yearweek
                                , cdc_lesson_sessions.year
                                , cdc_lesson_sessions.type
                                , cdc_lesson_sessions.lesson_number
                                , -- Only 2 lessons per week count, but they can be the same day
                                  row_number()
                                  OVER (
                                    PARTITION BY cdc_lesson_sessions.transformuserid, cdc_lesson_sessions.yearweek, cdc_lesson_sessions.year ) AS in_week
                              FROM cdc_lesson_sessions) ls
                       WHERE
                         ls.in_week <= 2
                       UNION
                       -- checkin_sessions
                       SELECT
                           checkin_weeks.transformuserid :: INTEGER AS transformuserid
                         , 'checkin' :: TEXT                        AS source
                         , min(checkin_weeks.date)                  AS date
                         , checkin_weeks.yearweek :: INTEGER        AS yearweek
                         , checkin_weeks.year :: INTEGER            AS year
                         , CASE WHEN min(checkin_weeks.date) > (fs.date + INTERVAL '6 months')
                         THEN 'maintenance'
                           ELSE 'core' END                          AS type
                         , NULL                                     AS lesson_number
                       FROM (
                              -- checkin_only_weeks: Weeks with a checkin but not a lesson
                              SELECT
                                checkin_weeks_1.transformuserid
                                , checkin_weeks_1.yearweek
                                , checkin_weeks_1.year
                              FROM cdc_checkin_weeks checkin_weeks_1
                              EXCEPT
                              SELECT
                                cdc_lesson_sessions.transformuserid
                                , cdc_lesson_sessions.yearweek
                                , cdc_lesson_sessions.year
                              FROM cdc_lesson_sessions) checkin_only_weeks
                         LEFT JOIN cdc_checkin_weeks checkin_weeks ON checkin_weeks.transformuserid =
                                                                           checkin_only_weeks.transformuserid AND
                                                                           checkin_weeks.yearweek =
                                                                           checkin_only_weeks.yearweek AND
                                                                           checkin_weeks.year = checkin_only_weeks.year
                         LEFT JOIN cdc_first_session fs ON fs.transformuserid = checkin_only_weeks.transformuserid

                       GROUP BY checkin_weeks.transformuserid, checkin_weeks.yearweek, checkin_weeks.year, fs.date
                     ) s
                     LEFT JOIN cdc_first_session fs ON fs.transformuserid = s.transformuserid
                   ORDER BY transformuserid, s.date
                 ) os

             ) oss
        WHERE
          (period = 'lte_6mo' AND period_number <= 20)
          OR (period = 'btw_7mo_9mo' AND period_number <= 3)
          OR (period = 'btw_10mo_12mo' AND period_number <= 3)
       ;